#!/usr/bin/python3

import sys
import re
import time
import os
import json
import argparse
import subprocess
import traceback
import multiprocessing
from pathlib import Path

import build
import scadutils
from stl import STL

def translate(pt, pos):
    px, py, pz = pt
    vx, vy, vz = pos
    return (px + vx, py + vy, pz + vz)

def main():
    p = argparse.ArgumentParser(description='Build a SCAD project')
    p.add_argument(
        '-f', '--file', dest='makefile',
        type=Path, default=Path('makefile.py'),
        help='Python script containing project definitions (default: makefile.py)')

    p.add_argument(
        '-c', '--cache-path',
        type=Path,
        help='Path to STL cache')

    p.add_argument(
        '-p', '--prog',
        help='Path to OpenSCAD')

    p.add_argument(
        '-g', '--generate', default=None, const='Makefile', nargs='?',
        help='Generate a Makefile instead of building')

    p.add_argument(
        '-j', '--parallel', type=int, default=multiprocessing.cpu_count(),
        help='Number of processes to run in parallel')

    p.add_argument(
        '-d', '--description', default=None, const='descriptions.json', nargs='?',
        help='Generate a JSON file with STL descriptions')

    p.add_argument('--nocache', action='store_true', help='Do not cache results')

    p.add_argument('-C', '--customizer', action='store_true', help='Update customizer files')

    p.add_argument('-r', '--rebuild', action='store_true', help='Rebuild even if cache matches')

    p.add_argument('-n', '--nobuild', action='store_true', help='Do not actually run build')

    args = p.parse_args()

    sys.path.insert(0, str(args.makefile.parent))

    py_text = args.makefile.read_text('utf8').replace('\r\n', '\n').removeprefix('\ufeff')

    mg = scadutils.MakeGen()

    globs = {}
    globs['__file__'] = str(args.makefile)
    globs['makegen'] = mg
    globs['source'] = mg.source
    globs['mergepart'] = mg.mergepart

    os.chdir(args.makefile.parent)

    exec(py_text, globs)

    if args.customizer:
        mg.update_customizer()

    dp = scadutils.DependencyFinder()
    for source in mg.sources:
        if source._automain:
            source.gen_automain(dp)

    if args.description:
        json_data = {}
        for source in mg.sources:
            for part in source.parts:
                for variant in part.get_children():
                    if variant.desc:
                        fn = variant.get_filename(source.name)
                        json_data[fn] = variant.desc

        for part in mg.mergeparts:
            if part.desc:
                json_data[part.out_stl] = part.desc

        path = Path(args.description)
        with path.open('w') as fp:
            json.dump(json_data, fp, indent=4)

    if args.generate:
        mg.generate_makefile()
        return

    if args.nobuild:
        return

    mb = build.MultiBuilder(args.prog, args.cache_path, args.rebuild, not args.nocache, args.parallel)
    for source in mg.sources:
        if source.parts:
            for part in source.parts:
                for variant in part.get_children():
                    if not variant.output_enable:
                        continue

                    filename = variant.get_filename(source.name)
                    variant.path = Path(filename)
                    if filename is not None:
                        mb.add(source.path, variant.path, variant.gen_args())

        else:
            mb.add(source.path, source.path.with_suffix('.stl'), [])

    mb.run()

    for mp in mg.mergeparts:
        print(f'Merging {mp.out_stl}')
        merged_tridata = []
        for part, pos in mp.sources:
            part = getattr(part, 'default_variant', part)
            if part.stldata is None:
                part.stldata = STL(part.path)

            if pos is None:
                merged_tridata.extend(part.stldata.tridata)
            else:
                for norm, v1, v2, v3, attr in part.stldata.tridata:
                    merged_tridata.append((
                        norm,
                        translate(v1, pos),
                        translate(v2, pos),
                        translate(v3, pos),
                        attr
                    ))

        out_stl = STL(name='stlmerge')
        out_stl.tridata = merged_tridata
        out_stl.write_to_file(mp.out_stl)

if __name__ == '__main__':
    main()
