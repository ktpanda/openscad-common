#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
from pathlib import Path

from stl import STL
import numpy as np

RX_XFORM = re.compile(r'^([trs])([xyz])\s*([-+]?\d*\.?\d*)$')

class NumpySTL(STL):
    pointarray = None

    def gen_pointarray(self):
        self.path = path

        pts = self.pointarray = np.zeros((len(tridata) * 4, 4))
        for i, (norm, v1, v2, v3, attr) in enumerate(tridata):
            j = i * 4
            pts[j + 0] = [norm[0], norm[1], norm[2], 0]
            pts[j + 1] = [v1[0], v1[1], v1[2], 1]
            pts[j + 2] = [v2[0], v2[1], v2[2], 1]
            pts[j + 3] = [v3[0], v3[1], v3[2], 1]

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('input', nargs='*', help='Input STL files')
    p.add_argument('-o', '--output', type=Path, help='')
    #p.add_argument('-v', '--verbose', action='store_true', help='')
    #p.add_argument(help='')
    args = p.parse_args()

    inputs = []

    identmat = np.identity(4)

    stls = {}

    point_arrays = []

    out_stl = STL()
    out_stl.name = 'stlmerge.py'

    for spec in args.input:
        txmat = np.copy(identmat)
        path, _, xform = spec.partition(':')

        try:
            stl = stls[path]
        except KeyError:
            stl = stls[path] = NumpySTL(path)
            stl.gen_pointarray()

        for part in xform.split(','):
            part = part.strip()
            if not part:
                continue
            cmat = np.copy(identmat)
            if m := RX_XFORM.match(part):
                xftype = m.group(1)
                xfaxis = m.group(2)
                val = float(m.group(3))
                if xftype == 't':
                    cmat[3]['xyz'.find(xfaxis)] = val
                elif xftype == 'r':
                    sv = np.sin(val * np.pi / 180)
                    cv = np.cos(val * np.pi / 180)
                    if xfaxis == 'z':
                        cmat[0][0] = cv
                        cmat[0][1] = sv
                        cmat[1][1] = cv
                        cmat[1][0] = -sv
                    elif xfaxis == 'y':
                        cmat[0][0] = cv
                        cmat[0][2] = -sv
                        cmat[2][2] = cv
                        cmat[2][0] = sv
                    elif xfaxis == 'x':
                        cmat[1][1] = cv
                        cmat[1][2] = sv
                        cmat[2][2] = cv
                        cmat[2][1] = -sv
                elif xftype == 's':
                    idx = 'xyz'.find(xfaxis)
                    cmat[idx][idx] = val
            txmat = np.dot(txmat, cmat)

        outpts = np.dot(stl.pointarray, txmat)

        if np.linalg.det(txmat) < 0:
            # flip the winding of all triangles by swapping the second and third points
            for j in range(0, outpts.shape[0], 4):
                pt2 = np.copy(outpts[j + 2])
                outpts[j + 2] = outpts[j + 3]
                outpts[j + 3] = pt2

        point_arrays.append(outpts)


    allpts = np.concatenate(point_arrays, axis=0, dtype=float)

    tridata = out_stl.tridata


    for j in range(0, allpts.shape[0], 4):
        norm = tuple(allpts[j + 0][0:3])
        v1 = tuple(allpts[j + 1][0:3])
        v2 = tuple(allpts[j + 2][0:3])
        v3 = tuple(allpts[j + 3][0:3])
        tridata.append((norm, v1, v2, v3, 0))

    if args.output:
        out_stl.write_to_file(args.output)

if __name__ == '__main__':
    main()
