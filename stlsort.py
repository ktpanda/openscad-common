#!/usr/bin/python3
import sys
import argparse
import traceback
import struct
from pathlib import Path
from stlmod import runmain

def process(stl, args):
    stl.sort

def main():
    p = argparse.ArgumentParser(description='Sort the triangles in a binary STL file so that output is consistent')
    p.add_argument('files', nargs='*', type=Path, help='STL files to process')
    p.add_argument('-o', '--output', type=Path, help='Output path. If not specified, each input STL file is overwritten')
    args = p.parse_args()

    runmain(p, args, process)

if __name__ == '__main__':
    main()
