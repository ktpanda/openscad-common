#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
from pathlib import Path

import gcode_filter as gcf

RX_EXTRUDER_COLOR = re.compile(r'(;\s*)(extruder|filament)(_colou?r\s*=\s*)(.*)')
RX_OPT = re.compile(r'(;\s*)([^=]+\s*)(=\s*)(.*)')
RX_TOOL_CHANGE = re.compile(r'T(\d+)')

def is_option_per_tool(key):
    if key.endswith('_gcode'):
        return False

    if key.endswith('_matrix'):
        return False

    if key.endswith('_cumulative') or key.endswith('_cummulative'):
        return False

    if key.startswith('machine_'):
        return False

    if key in {'bed_shape', 'thumbnails', 'objects_info'}:
        return False

    if key in {'wipe', 'cooling', 'deretract_speed', 'nozzle_diameter', 'extrusion_multiplier',
               'max_layer_height', 'min_layer_height', 'min_print_speed', 'nozzle_high_flow'}:
        return True

    if 'temperature' in key or 'fan_' in key:
        return True

    if key.startswith('filament') or key.startswith('retract') or key.startswith('extruder'):
        return True

    if key.startswith('slowdown') or key.startswith('fan') or key.startswith('wiping'):
        return True

    if key.startswith('travel'):
        return True

    return None

class Extruder:
    def __init__(self, idxin, idxout):
        self.idxin = idxin
        self.idxout = idxout
        self.color = ''
        self.used = False

class ExtruderSet:
    def __init__(self):
        self.exmap_in = {}
        self.exmap_out = {}
        self.num_extruders = None

    def get_extruder(self, idxin):
        try:
            return self.exmap_in[idxin]
        except KeyError:
            ex = Extruder(idxin, idxin)
            self.exmap_in[idxin] = ex
            return ex

    def remap(self, idxin, idxout):
        ex = self.get_extruder(idxin)
        ex.idxout = idxout

    def remap_done(self):
        maxn = -1
        outm = self.exmap_out
        for ex in self.exmap_in.values():
            if ex.idxin > maxn:
                maxn = ex.idxin
            if ex.idxout > maxn:
                maxn = ex.idxout
            self.exmap_out[ex.idxout] = ex
        self.num_extruders = maxn + 1

    def shuffle_option(self, text, sep, is_filament=False):
        if sep in text:
            # split on both the separator and separator + space. If the resulting lists
            # are the same length, then use the separator with the space.
            sep_spc = sep + ' '
            itext = text.split(sep)
            itext2 = text.split(sep_spc)
            if len(itext2) == len(itext):
                sep = sep_spc
                itext = itext2

            otext = []
            for i in range(self.num_extruders):
                try:
                    ex = self.exmap_out[i]
                    v = itext[ex.idxin]
                    if is_filament and re.match(r'^[0\.]+$', v.strip()) and ex.used:
                        v = '999.00'
                    otext.append(v)
                except (KeyError, IndexError):
                    otext.append('')
            return sep.join(otext)
        return text

def parsetool(txt, lbl):
    try:
        v = int(txt)
        if v <= 0:
            raise ValueError()
    except ValueError as e:
        raise ValueError(f'Invalid {lbl} tool: {txt!r}') from None

    return v - 1

def filter(args, lines):
    exts = ExtruderSet()

    for spec in args.map:

        if '=' in spec:
            swap = False
            tin, tout = spec.split('=')
        elif '/' in spec:
            tin, tout = spec.split('/')
            swap = True
        else:
            raise ValueError('Invalid spec: {spec!r}')


        tin = parsetool(tin.strip(), 'source')
        tout = parsetool(tout.strip(), 'dest')
        exts.remap(tin, tout)
        if swap:
            exts.remap(tout, tin)


    for line in lines:
        if m := RX_EXTRUDER_COLOR.match(line):
            type = m.group(2)
            for i, clr in enumerate(m.group(4).split(';')):
                print(f'input {type} color {i} = {clr}')
                ex = exts.get_extruder(i)
                if clr and (type == 'extruder' or not ex.color):
                    ex.color = clr

    exts.remap_done()

    extruder_colors_out = []
    for ex in exts.exmap_in.values():
        print(f'Extruder: idxin={ex.idxin} idxout={ex.idxout} clr={ex.color}')
        append = ex.idxout - len(extruder_colors_out) + 1
        for j in range(append):
            extruder_colors_out.append('')

        extruder_colors_out[ex.idxout] = ex.color

    unknown_opts = {}
    for idx, inline in enumerate(lines):
        if m := RX_TOOL_CHANGE.match(inline):
            tool = int(m.group(1))
            ex = exts.get_extruder(tool)
            lines[idx] = f'T{ex.idxout}' + inline[m.end():]
            ex.used = True
            print(f'replace toolchange line {idx}: {inline!r} -> {lines[idx]!r}')

    for idx, inline in enumerate(lines):
        if m := RX_EXTRUDER_COLOR.match(inline):
            lines[idx] = m.group(1) + m.group(2) + m.group(3) + ';'.join(extruder_colors_out)
        elif m := RX_OPT.match(inline):
            prefix = m.group(1)
            key_part = m.group(2)
            mid = m.group(3)
            val = m.group(4)
            key = key_part.strip()

            sep = None
            if ';' in val:
                sep = ';'
            elif ',' in val:
                sep = ','

            if sep is not None:
                per_tool = is_option_per_tool(key)
                if per_tool is None:
                    unknown_opts[key] = val

                if per_tool:
                    sep = ','
                    if ';' in val:
                        sep = ';'
                    outline = prefix + key_part + mid + exts.shuffle_option(m.group(4), sep, key.startswith('filament'))
                    if inline != outline:
                        lines[idx] = outline
                        print(f'replace option line {idx}:')
                        print(f'   {inline}')
                        print(f'   {outline}')



    for key, val in unknown_opts.items():
        print(f'??? {key} = {val}')

    return lines

def add_arguments(p):
    p.add_argument('map', nargs='*')

if __name__ == '__main__':
    p = gcf.init_arguments('Remap tools')
    add_arguments(p)
    gcf.run(p.parse_args(), filter)
