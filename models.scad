include <consts.scad>
use <funcs.scad>
use <shapes.scad>
use <sevenseg.scad>

module print_bed_model_i3(center=true) {
    size = [250, 210];

    let(y2 = size.y + 12.8)
    translate(center ? -size / 2 : [0, 0]) {
        color("#405040")
        lxt(-1.6, -eps) difference() {
            squarex(x1 = -1, x2 = size.x + 1, y1 = -17.3, y2 = y2, r = 5);

            let(x1 = 141.3 / 2)
            translate([size.x/2, y2])
            polyx(rfx([
                [x1, -4.6],
                [x1 + 2.8, 0],
                [x1 + 5, 0],
                [x1 + 5, 2],
            ]));
        }

        let(lw = .6)
        color("#f8f8f8")
        lxt(-eps, 0) {
            tx(closed_range(lw/2, size.x - lw/2, base = 0, stepn=50))
            squarex(xs = lw, y1 = 0, y2 = size.y);

            ty(closed_range(lw/2, size.y - lw/2, base = size.y/2, stepn=50))
            squarex(ys = lw, x1 = 0, x2 = size.x);
        }
    }
}
