include <consts.scad>
use <funcs.scad>

// Define a planet gear based on the number of teeth on the planet gear,

// pg_ring_gear_teeth = pg_planet_carrier_teeth + pg_planet_gear_teeth;

// pg_sun_gear_teeth + pg_planet_gear_teeth = pg_planet_carrier_teeth;

function pg_define(planet=undef, sun=undef, ring=undef, carrier=undef, num_planets=4,
scale=1, height=15, gear_size=1.1, twist=30, fit_clearance=0.06) = let(
    teeth = (
        (!is_undef(carrier) && !is_undef(planet)) ? [ planet, carrier ] :
        (!is_undef(carrier) && !is_undef(sun)) ? [ carrier - sun, carrier ] :
        (!is_undef(carrier) && !is_undef(ring)) ? [ ring - carrier, carrier ] :
        (!is_undef(planet) && !is_undef(sun)) ? [ planet, planet + sun ] :
        (!is_undef(planet) && !is_undef(ring)) ? [ planet, ring - planet ] :
        (!is_undef(sun) && !is_undef(ring)) ? let(ct=(sun + ring) / 2) [ ring - ct, ct ] :
        assert(false, "pg_define: not enough parameters")
    ),
    planet_teeth = teeth[0],
    carrier_teeth = teeth[1],
    sun_teeth = carrier_teeth - planet_teeth,
    ring_teeth = carrier_teeth + planet_teeth,
    divis_check = carrier_teeth * 2 / num_planets,
    check1 = assert(carrier_teeth == floor(carrier_teeth), "sun + planet must be even"),
    check2 = assert(divis_check == floor(divis_check), str("number of planets must divide ", carrier_teeth * 2))
) [
    sun_teeth,
    planet_teeth,
    ring_teeth,
    num_planets,
    scale,
    height,
    fit_clearance,
    gear_size,
    twist
];

function pg_sun_teeth(def) = def[0];
function pg_planet_teeth(def) = def[1];
function pg_ring_teeth(def) = def[2];
function pg_num_planets(def) = def[3];
function pg_scale(def) = def[4];
function pg_height(def) = def[5];
function pg_fit_clearance(def) = def[6];
function pg_gear_size(def) = def[7];
function pg_twist(def) = def[8];

function pg_carrier_teeth(def) = pg_sun_teeth(def) + pg_planet_teeth(def);
function pg_ratio_fixed_ring(def) = pg_carrier_teeth(def) * 2 / pg_sun_teeth(def);

function pg_sun_gear_size(def) = pg_sun_teeth(def) * pg_scale(def);
function pg_planet_gear_size(def) = pg_planet_teeth(def) * pg_scale(def);
function pg_carrier_gear_size(def) = (pg_sun_teeth(def) + pg_planet_teeth(def)) * pg_scale(def);
function pg_ring_gear_size(def) = pg_ring_teeth(def) * pg_scale(def);


function triwave(t) = let(v = 2*(t - floor(t))) 2*(v < 1 ? v : 2 - v) - 1;

function gearpts(s, gs, n, phase=0) = [
    for (t = [ 0 : 360 / (2 * n * ceil($fn/n/2)) : 360 ])
    polarpt(s  + gs * triwave(t*n/360), t + phase)
];


module ring_gear(is, igs, in, os, ogs, on, iphase=0, ophase=0) {
    opts = is_undef(ogs) ? [
        [-os, -os],
        [-os, os],
        [os, os],
        [os, -os],
    ] :gearpts(os, ogs, on, ophase);

    ipts = gearpts(is, igs, in, iphase);
    points = [ each opts, each ipts ];
    paths = [
        [ for(i=[0 : 1 : len(opts)-1]) i ],
        [ for(i=[0 : 1 : len(ipts)-1]) i+len(opts) ]
    ];
    polygon(points, paths);
}

module pg_planet_transform(def, ang) {
    rz(ang)
    tx(pg_carrier_gear_size(def))
    rz(-ang)
    children();
}

module _pg_gear(def, nteeth, twist, slices, fcs=1) {
    h = pg_height(def);

    rfz(h/2)
    linear_extrude(h/2, twist=twist, convexity = 3, slices=slices)
    polygon(gearpts(
        nteeth * pg_scale(def) + fcs * pg_fit_clearance(def),
        pg_gear_size(def) * pg_scale(def),
        nteeth
    ));
}

module pg_sun_gear(def, slices=10) {
    nteeth = pg_sun_teeth(def);
    _pg_gear(def, nteeth, pg_twist(def) * pg_planet_teeth(def) / nteeth, slices, -1);
}

module pg_planet_gear(def, slices=10) {
    nteeth = pg_planet_teeth(def);
    rz(180 + 180/pg_planet_teeth(def))
    _pg_gear(def, nteeth, -pg_twist(def), slices, -1);
}

module pg_ring_gear_inner(def, slices=10) {
    nteeth = pg_ring_teeth(def);
    rz(180 + 180/pg_ring_teeth(def))
    _pg_gear(def, nteeth, -pg_twist(def) * pg_planet_teeth(def) / nteeth, slices, 1);
}
