#!/usr/bin/python3
import sys
import argparse
import traceback
import struct
from pathlib import Path
from stl import STL

def main():
    p = argparse.ArgumentParser(description='Transform the triangles in an STL file')
    p.add_argument('files', nargs='*', type=Path, help='STL files to process')
    p.add_argument('-s', '--sort', action='store_true', help='Sort the triangles so that output is consistent')
    p.add_argument('-c', '--project-cylinder', action='store_true', help='Project the points onto a cylinder: x -> theta, z-> rho, y -> z')
    p.add_argument('-o', '--output', type=Path, help='Output path. If not specified, each input STL file is overwritten')
    args = p.parse_args()

    runmain(p, args, process_stl)

def process_stl(stl, args):
    if args.project_cylinder:
        stl.project_cylinder()

    if args.sort:
        stl.sort()

def runmain(p, args, process):
    if not args.files:
        p.print_help()
        return

    if args.output and len(args.files) > 1:
        print('--output can only be used with a single input file')
        return

    for stlf in args.files:
        try:
            stl = STL(stlf)
            process(stl, args)
            stl.write_to_file(args.output or stlf)
        except Exception as e:
            print(f'Failed to process {stlf}:')
            traceback.print_exc()

if __name__ == '__main__':
    main()
