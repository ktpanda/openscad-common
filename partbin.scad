include <consts.scad>
use <funcs.scad>
use <shapes.scad>

module hexnut_hole(d=hexhole_width, h=hexhole_height, xw=0, xl=0) {
    linear_extrude(h)
    hexagon(d, xw, xl);
}

module hexnut_plug_base(d, hole_dia, xh, xl, vfc, hfc=0) {
    tz(-xh)
    hexnut_hole(d+.6 - 2*hfc, xh+vfc);
    for (ang = [0, 2]) rz(60*ang+30)
    cubex(xc=0, yc=0, ys=hole_dia*.4 - 2*hfc, xs=d+2*xl - 2*hfc, z1=-xh, z2=vfc);
}

module hexnut_cutout(d=nut_diameter_m2, h=2.4, hole_dia=screw_diameter_m2, screw_l=6, xh=2, xl=1.8) {
    hexnut_plug_base(d, hole_dia, xh, xl, eps);
    tz(-xh-h)
    hexnut_hole(d=d, h=h+eps);
    cylx(-screw_l, 0, d=hole_dia);
}

module hexnut_plug(d=nut_diameter_m2, h=2.4, hole_dia=screw_diameter_m2, xh=2, xl=1.8) {
    difference() {
        hexnut_plug_base(d, hole_dia, xh, xl, 0, 0.1);
        cylx(-xh-eps, eps, d=hole_dia);
    }
}

module countersink_m2(
    head_z, tail_z=0, head_extra=0, dia=screw_diameter_m2, head_dia=screw_head_diameter_m2,
    head_length=screw_head_length_m2, nut_length=0, expand=1.1
) {
    tz(tail_z)
    if (head_z < tail_z) {
        sz(-1) _countersink_m2_5(tail_z - head_z, head_extra, dia, head_dia, head_length, nut_length, expand);
    } else {
        _countersink_m2_5(head_z - tail_z, head_extra, dia, head_dia, head_length, nut_length, expand);
    }
}

module _countersink_m2_5(length, head_extra, dia, head_dia, head_length, nut_length, expand) {
    d1 = dia;
    d2 = head_dia * expand;

    z4 = length;
    z3 = length - head_extra;
    z2 = z3 - head_length * expand;
    z1 = 0;

    render(4) {
        lathe(d=uniq([
            [z1, d1],
            [z2, d1],
            [z3, d2],
            [z4, d2],
        ]));

        if (nut_length > 0)
        lxt(0, nut_length) hexagon(nut_diameter_m2);
    }
}

function taper_points_3d(pts, cone_z1=0, cone_z2=1, cone_s1=1, cone_s2=1) =
(cone_s1 == 1 && cone_s2 == 1) ? pts : [
    for (pt = pts)
    vec3(pt * lerp2(cone_s1, cone_s2, cone_z1, cone_z2, pt.z), pt.z)
];

function taper_points_xy(pts, cone_z1=0, cone_z2=1, cone_s1=1, cone_s2=1) =
(cone_s1 == 1 && cone_s2 == 1) ? pts : [
    for (pt = pts)
    [pt.x * lerp2(cone_s1, cone_s2, cone_z1, cone_z2, pt.y), pt.y]
];

function taper_lathe(pts, cone_z1=0, cone_z2=1, cone_s1=1, cone_s2=1) =
(cone_s1 == 1 && cone_s2 == 1) ? pts : [
    for (pt = pts)
    [pt.x, pt.y * lerp2(cone_s1, cone_s2, cone_z1, cone_z2, pt.x)]
];

module _screw_thread(defn, count=10, cone_z1=0, cone_z2=1, cone_s1=1, cone_s2=1) {
    pitch = defn[0];
    major_rad = defn[1]/2;
    minor_rad = defn[2]/2;

    //echo(H, (pitch_dia - minor_dia) / H);

    outer_points = flatten([
        each [
            for (i = [0 : count - 1])
            [
                [minor_rad, (i - .5) * pitch],
                [major_rad, i * pitch]
            ]

        ],
        [[minor_rad, (count - .5) * pitch]]
    ]);

    slices = [
        each [
            for (ang = half_open_range_a(0, 360, stepn=360, stepd=$fn))
            let(c = cos(ang), s = sin(ang))
            [for (pt = outer_points) [pt.x * c, pt.x * s, pt.y + (ang * pitch / 360)]]
        ],
        // End cap faces. Wonky ends will be cut off by CSG.
        /*[
        ]*/
    ];

    points = taper_points_3d([
        each flatten(slices),
        [0, 0, -pitch/2],
        [0, 0, ((count + .5) * pitch)]
    ], cone_z1, cone_z2, cone_s1, cone_s2);

    endcap_base = len(slices) * len(slices[0]);


    // Modified from closed_extrusion_faces - shifts the end up and excludes the back face

    faces = let(np = len(outer_points), ns = len(slices)) [
        // `i` goes from 0 to np - 2 to exclude the back face
        each [for (base = [0 : np : np * (ns - 2)], i = [0 : np - 2], side = [0, 1])
            _extrusion_side_face(np, base, base + np, i, side)
        ],
        each let(base1 = np * (ns - 1)) [
            // `i` goes to np - 4 to exclude the back face and the top thread
            // base2 is 2 instead of 0 to shift the closing faces up
            for (i = [0 : np-4], side = [0, 1])
            _extrusion_side_face(np, base1, 2, i, side)
        ],

        // Bottom face
        each [
            for (base = [0 : np : np * (ns - 2)])
            [endcap_base, base, base + np]
        ],
        [endcap_base, np * (ns - 1), 2],
        [endcap_base, 2, 1],
        [endcap_base, 1, 0],

        // Top face

        [endcap_base + 1, endcap_base - 2, endcap_base - 1],
        [endcap_base + 1, endcap_base - 3, endcap_base - 2],
        [endcap_base + 1, endcap_base - np * (ns - 1) - 1, endcap_base - 3],

        each [
            for (basex = [0 : np : np * (ns - 2)])
            let(base = endcap_base - basex - np - 1)
            [endcap_base + 1, base + np, base]
        ],
    ];

    polyhedron(points, faces, convexity=(2 * count));
}

module _screw_thread_double(defn, count=10, cone_z1=0, cone_z2=1, cone_s1=1, cone_s2=1) {
    pitch = defn[0];
    major_rad = defn[1]/2;
    minor_rad = defn[2]/2;

    //echo(H, (pitch_dia - minor_dia) / H);

    outer_points = flatten([
        each [
            for (i = [0 : count - 1])
            [
                [minor_rad, (i - .5) * pitch],
                [major_rad, i * pitch]
            ]

        ],
        [[minor_rad, (count - .5) * pitch]]
    ]);

    slices = [
        each [
            for (ang = half_open_range_a(0, 360, stepn=360, stepd=$fn))
            let(c = cos(ang), s = sin(ang))
            [for (pt = outer_points) [pt.x * c, pt.x * s, pt.y + (ang * pitch / 180)]]
        ],
    ];

    points = taper_points_3d([
        each flatten(slices),
        [0, 0, -pitch/2],
        [0, 0, ((count + 1.5) * pitch)]
    ], cone_z1, cone_z2, cone_s1, cone_s2);

    endcap_base = len(slices) * len(slices[0]);


    // Modified from closed_extrusion_faces - shifts the end up and excludes the back face

    faces = let(np = len(outer_points), ns = len(slices)) [
        // `i` goes from 0 to np - 2 to exclude the back face
        each [for (base = [0 : np : np * (ns - 2)], i = [0 : np - 2], side = [0, 1])
            _extrusion_side_face(np, base, base + np, i, side)
        ],

        each let(base1 = np * (ns - 1)) [
            // `i` goes to np - 4 to exclude the back face and the top thread
            // base2 is 2 instead of 0 to shift the closing faces up
            for (i = [0 : np-6], side = [0, 1])
            _extrusion_side_face(np, base1, 4, i, side)
        ],

        // Bottom face
        for (base = [0 : np : np * (ns - 2)])
        [endcap_base, base, base + np],

        // Connect the final cone, then close it up
        [endcap_base, np * (ns - 1), 4],
        [4, 3, 2],
        [2, 1, 0],
        [endcap_base, 4, 2],
        [endcap_base, 2, 0],

        // Top face

        [endcap_base - 3, endcap_base - 2, endcap_base - 1],
        [endcap_base - 5, endcap_base - 4, endcap_base - 3],

        [endcap_base + 1, endcap_base - 3, endcap_base - 1],
        [endcap_base + 1, endcap_base - 5, endcap_base - 3],
        [endcap_base + 1, endcap_base - np * (ns - 1) - 1, endcap_base - 5],

        for (basex = [0 : np : np * (ns - 2)])
        let(base = endcap_base - basex - np - 1)
        [endcap_base + 1, base + np, base],
    ];

    polyhedron(points, faces, convexity=(2 * count));
}

function _taper_pts(r1, r2, p) = [
    [0, 0],
    [0, -p],
    [r2 + (r2-r1), -p],
    [r2 + (r2-r1), (r2-r1) * 2],
    [r1, 0],
];

module screw_thread(defn,
    z1, z2, zc, zs,
    ltaper = 0, utaper = 0,
    lext = 0, uext = 0,
    zbase = undef,
    double = false,
    cone_z1=undef, cone_z2=undef, cone_s1=1, cone_s2=1
) {
    zspan = span(z1, z2, zc, zs, "screw_thread (z)");
    h = zspan[1] - zspan[0];

    zbase = !is_undef(zbase) ? zbase : zspan[0];

    pitch = defn[0];
    maj_r = defn[1] / 2;
    min_r = defn[2] / 2;

    clip_z1 = zspan[0];
    clip_z2 = zspan[1];

    cone_z1 = coalesce(cone_z1, zspan[0]);
    cone_z2 = coalesce(cone_z2, zspan[1]);

    maxscale = max(cone_s1, cone_s2);

    thread_z1 = zbase + pitch * (floor((clip_z1 - zbase) / pitch) - (double ? 2 : 1));

    count = ceil((clip_z2 - thread_z1) / pitch + .1) + (double ? 4 : 2);

    //tx(100) lxtd(.1) polyx(taper_points_xy(negy(_taper_pts(min_r, maj_r, pitch*3)), cone_z1 - clip_z2, cone_z2 - clip_z2, cone_s1, cone_s2));

    render(count) {
        difference() {
            intersection() {
                tz(thread_z1)
                if (double) {
                    _screw_thread_double(defn, count=count, cone_z1 - thread_z1, cone_z2 - thread_z1, cone_s1, cone_s2);
                } else {
                    _screw_thread(defn, count=count, cone_z1 - thread_z1, cone_z2 - thread_z1, cone_s1, cone_s2);
                }

                // cut off the top and bottom
                let(sz = maxscale * 2 * maj_r + 1)
                cubex(xc=0, xs=sz, yc=0, ys=sz, z1=clip_z1, z2=clip_z2);
            }

            // taper inward
            if (ltaper == 1) {
                tz(clip_z1)
                rotate_extrude() polygon(taper_points_xy(_taper_pts(min_r, maj_r, pitch*3), cone_z1 - clip_z1, cone_z2 - clip_z1, cone_s1, cone_s2));
            }
            if (utaper == 1) {
                tz(clip_z2)
                rotate_extrude() polygon(taper_points_xy(negy(_taper_pts(min_r, maj_r, pitch*3)), cone_z1 - clip_z2, cone_z2 - clip_z2, cone_s1, cone_s2));
            }
        }

        // taper outward
        if (ltaper == -1) {
            lathe(r=taper_lathe([
                if (lext > 0) [clip_z1 - lext, maj_r],
                [clip_z1, maj_r],
                [clip_z1 + maj_r - min_r, min_r],
            ], cone_z1, cone_z2, cone_s1, cone_s2 ));
        } else if (lext > 0) {
            cylx(clip_z1 - lext, clip_z1,
                r1=min_r * lerp2(cone_s1, cone_s2, cone_z1, cone_z2, clip_z1 - lext),
                r2=min_r * lerp2(cone_s1, cone_s2, cone_z1, cone_z2, clip_z1),
            );
        }

        if (utaper == -1) {
            lathe(r=taper_lathe([
                if (uext > 0) [clip_z2 + uext, maj_r],
                [clip_z2, maj_r],
                [clip_z2 - maj_r + min_r, min_r],
            ], cone_z1, cone_z2, cone_s1, cone_s2));
        } else if (uext > 0) {
            cylx(clip_z2, clip_z2 + uext,
                r1=min_r * lerp2(cone_s1, cone_s2, cone_z1, cone_z2, clip_z2),
                r2=min_r * lerp2(cone_s1, cone_s2, cone_z1, cone_z2, clip_z2 + uext),
            );
        }
    }

}

module screw_taper(h, defn, taper_top=0, taper_bot=0) {
    screw_thread(defn, 0, h, ltaper=taper_bot, utaper=taper_top);
}

module skirt(offset, extent_a, extent_b) {
    thick = $preview ? 2 : .02;

    render(2)
    difference() {
        cubespan(
            [-extent_a.x - offset, extent_b.x + offset],
            [-extent_a.y - offset, extent_b.y + offset],
            [-extent_a.z, -extent_a.z + .02]
        );
        cubespan(
            [-extent_a.x - offset + thick, extent_b.x + offset - thick],
            [-extent_a.y - offset + thick, extent_b.y + offset - thick],
            [-extent_a.z - 10, -extent_a.z + 10]
        );
    }
}

module bearing_cutout(h, od, ofs=2, fca=0, angshift=0) {
    let(rad = od/2, angofs = 30 + fca)
    linear_extrude(h, convexity=4) union() {
        circle(r=rad - ofs);
        difference() {
            circle(r = rad);
            for (ang = [0, 120, -120])
            let(pt=[rad, 0]) rz(ang + angshift) polygon([
                rotpt(pt/2, -angofs),
                rotpt(pt/2, +angofs),
                rotpt(pt*2, +angofs),
                rotpt(pt*2, -angofs),
            ]);
        }
    }
}

module pcb_model_old(traces=false, source="dispenser", thick=1.6, trace_thick=0.3, convexity=10, pcb_color="#00bb00", trace_color="gold") {
    color(pcb_color)
    linear_extrude(thick)
    render(convexity)
    difference() {
        import(str(source, "-pcb-board.svg"));
        import(str(source, "-pcb-holes.svg"), convexity=10);
    }

    if (traces) {
        color(trace_color, 0.5)
        tz(thick + eps)
        linear_extrude(trace_thick)
        render(convexity)
        difference() {
            import(str(source, "-pcb-trace.svg"), convexity=10);
            import(str(source, "-pcb-holes.svg"), convexity=10);

        }
    }
}

module thumbscrew_outline(baser = 7.4, rofs = .6, lobes = 6) {
    polyx([
        for (ang = [0 : 360/$fn/4 : 360])
        rotpt([baser + cos(ang * lobes) * rofs, 0], ang)
    ]);
}

module thumbscrew(height = 7, head_dia = 5.5, head_len = 3, screw_dia = screw_diameter_m3, baser = 7.4, rofs = 0.6, lobes = 6, hex=false) {
    difference() {
        lxt(0, height) difference() {
            if (hex) {
                hexagon(baser * 2);
            } else {
                thumbscrew_outline(baser, rofs, lobes);
            }
            squarex(xs=1, ys=head_dia + 4);
        }

        cylx(-4, height + 1, d=screw_dia);

        tz(height) {
            rfx() intersection() {
                cubex(
                    x2 = 20, ys = 20,
                    zs = 20
                );

                tx(-.2) {
                    cylx(-head_len, eps, d=head_dia);
                }
            }
        }
    }
}
