#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
import zipfile
from pathlib import Path

RX_VALUE = re.compile(r'value="([^\"]+\.stl)"')

def do_replace(repl, text):
    for sfrom, sto in repl:
        text = text.replace(sfrom, sto)
    return text

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('input', metavar='3MF', help='3mf source file')
    p.add_argument('-o', '--output', metavar='3MF', help='3mf output file')
    p.add_argument('-i', '--inplace', action='store_true', help='Write changes back to input file')
    p.add_argument('-r', '--replace', nargs=2, metavar=('FROM', 'TO'), action='append', help='Replacement text')
    #p.add_argument('-v', '--verbose', action='store_true', help='')
    #p.add_argument(help='')
    args = p.parse_args()

    def value_sub(m):
        stlf = do_replace(args.replace, m.group(1))
        return f'value="{stlf}"'

    if args.inplace:
        if args.output:
            print('Argument error: --output specified with --inplace', file=sys.stderr)
            return

        args.output = args.input

    elif not args.output:
        args.output = do_replace(args.replace, args.input)
        if args.output == args.input:
            print(f'{args.input!r} does not match replacement patterns, specify --output', file=sys.stderr)
            return

    xml_name = 'Metadata/Slic3r_PE_model.config'

    tmpfn = f'.tmp{os.getpid()}'
    try:
        modified = False
        print(f'{args.input} -> {args.output}')
        with zipfile.ZipFile(args.input, 'r') as inz:
            with zipfile.ZipFile(tmpfn, 'w', compression=zipfile.ZIP_DEFLATED, compresslevel=9) as outz:
                for inf in inz.infolist():
                    data = inz.read(inf)
                    if inf.filename == xml_name:
                        modified = True
                        text = data.decode('utf8')
                        text = RX_VALUE.sub(value_sub, text)
                        data = text.encode('utf8')
                    outz.writestr(inf, data)
        if modified:
            os.replace(tmpfn, args.output)
            tmpfn = None
        else:
            print(f'No {xml_name} found in {args.input}')
    finally:
        if tmpfn:
            try:
                os.unlink(tmpfn)
            except OSError:
                pass


if __name__ == '__main__':
    main()
