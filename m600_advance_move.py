#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
from pathlib import Path
from decimal import Decimal as D, DecimalException

import gcode_filter as gcf

def find_next_non_print_move(lines, idx, lx, ly):
    end = min(idx + 500, len(lines))
    while idx < end:
        line = lines[idx]
        if re.match(r'^G[0-3] ', line):
            v = gcf.split_params(line[3:])
            if ('X' in v or 'Y' in v) and ('Z' not in v) and ('E' not in v):
                return idx, v.get('X', lx), v.get('Y', ly)

        idx += 1
    return None, None, None

def find_next_extrusion(lines, idx):
    end = min(idx + 500, len(lines))
    while idx < end:
        line = lines[idx]
        if re.match(r'^G[0-3] ', line):
            v = gcf.split_params(line[3:])
            if D(v.get('E', 0)) > 0:
                return idx
        idx += 1

    return None

def filter(args, lines):
    skip_moves_until = -1

    outlines = []
    for idx, inline in enumerate(lines):
        if inline.startswith('M600'):
            cx, cy, cz, cspeed = gcf.find_current_params(lines, idx - 1)

            if args.remove_extra:
                nidx = find_next_extrusion(lines, idx + 1)
                nx, ny, nz, nspeed = gcf.find_current_params(lines, nidx - 1)
                skip_moves_until = nidx
            else:
                nidx, nx, ny = find_next_non_print_move(lines, idx + 1, cx, cy)
                nz = cz

            if nidx is not None and cz is not None and cspeed is not None:
                outlines.append(f'; color change - advance move ({nidx})')

                liftz = max(args.minz, cz + args.lift)

                outlines.append(f'G1 Z{liftz} F{args.speed}')
                if args.pos:
                    mx, my = args.pos
                    outlines.append(f'G1 X{mx} Y{my}')
                    if args.purge_line:
                        outlines.append(f'G1 Z15')
                else:
                    outlines.append(f'G1 X{nx} Y{ny}')

                outlines.append(inline)


                if args.pos:
                    if args.purge_line:
                        if mx < 30:
                            xd = 1
                        else:
                            xd = -1
                        outlines.append(f'G0 E7 X{mx + xd * 15} Z0.2 F500')
                        outlines.append(f'G0 X{mx + xd * 25} E4 F500')
                        outlines.append(f'G0 X{mx + xd * 35} E4 F650')
                        outlines.append(f'G0 X{mx + xd * 45} E4 F800')
                        outlines.append(f'G0 X{mx + xd * (45 + 3)} Z0.05 F8000')
                        outlines.append(f'G0 X{mx + xd * (45 + 3 * 2)} Z0.2 F8000')
                        outlines.append(f'G0 Z{liftz} F{args.speed}')

                    outlines.append(f'G1 X{nx} Y{ny} F{args.speed}')

                if args.purge:
                    outlines.append(f'G1 E{args.purge} F600')

                if args.pause:
                    outlines.append(f'G4 P{args.pause}')

                outlines.append(f'G1 Z{nz} F{args.speed}')
                outlines.append(f'G1 F{cspeed}')
                outlines.append(f'; color change - advance move end')
            else:
                outlines.append(f'; color change - no advance move found')
                outlines.append(inline)
        else:
            if idx < skip_moves_until:
                if inline.startswith('G'):
                    outlines.append(f'; skip move {inline}')
                    v = gcf.split_params(inline)
                    gtype = v.pop('G', '0')
                    v.pop('X', None)
                    v.pop('Y', None)
                    v.pop('Z', None)
                    if v:
                        outlines.append(f'G{gtype}' + ''.join(f' {k}{v}' for k, v in v.items()))
                    continue

            outlines.append(inline)

    return outlines

def add_arguments(p):
    p.add_argument(
        '-p', '--pos', nargs=2, metavar=('X', 'Y'), type=D, default=None,
        help='Position to move extruder to before color change')

    p.add_argument(
        '-l', '--lift', type=D, metavar='MM', default=D('20'),
        help='Amount to lift prior to color change')

    p.add_argument(
        '-Z', '--minz', type=D, metavar='MM', default=D('15'),
        help='Minimum Z position to lift to')

    p.add_argument(
        '-r', '--purge', type=D, metavar='MM', default=None,
        help='Purge prior to moving back down')

    p.add_argument(
        '-z', '--pause', type=int, metavar='MS', default=None,
        help='Pause after moving over return position')

    p.add_argument(
        '-s', '--speed', default='18000',
        help='Speed for travel moves')

    p.add_argument(
        '-R', '--remove-extra', action='store_true',
        help='Remove extra moves before the next extrusion after the M600')

    p.add_argument(
        '--purge-line', action='store_true',
        help='Draw a purge line on the bed prior to moving back (requires --pos)')

if __name__ == '__main__':
    p = gcf.init_arguments('Move extruder to next print position before M600')
    add_arguments(p)
    gcf.run(p.parse_args(), filter)
