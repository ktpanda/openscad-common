

module outer_hull(z1, z2) {
    cubex(
        x1 = -outer_extent_a.x,
        x2 = outer_extent_b.x,
        y1 = -outer_extent_a.y,
        y2 = outer_extent_b.y,
        z1 = z1,
        z2 = z2,
        r=outer_radius
    );
}

module inner_hull(z1, z2) {
    cubex(
        x1 = -inner_margin.x,
        x2 = pri_width + inner_margin.x,
        y1 = -inner_margin.y,
        y2 = pri_length + inner_margin.y,
        z1 = z1 - eps,
        z2 = z2 + eps,
        r = inner_radius
    );
}

module lid_transform(lid_open, yofs=1) {
    for(lid_pos=lid_open) {
        let(zofs=box_height + lid_thick,
        yofs=lerp(-pri_length/2, outer_extent_a.y + 5, yofs)) {
            tz(sin(lid_pos*180) * 20)
            tz(-(zofs + outer_extent_a.z) * lid_pos)
            tz(zofs)
            ty(-yofs)
            rx(lid_pos * 180)
            ty(yofs)
            tz(-zofs) // so top of lid is zero
            children();
        }
    }
}

module boxskirt(offset) {
    skirt(offset, outer_extent_a, outer_extent_b);
}
