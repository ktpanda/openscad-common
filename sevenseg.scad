include <consts.scad>
use <funcs.scad>
use <shapes.scad>

// preview_num = -1; // [-100 : 0.01 : 100]

// compensate for rounding errors
function numdig(v) = (
    let(intv = floor(abs(v)))
    intv == 0 ? 1 :
    (let(l10v=floor(.5 + log(intv))) l10v + (intv >= 10 ^ l10v ? 1 : 0))
);

// parameter default functions
function _sevenseg_v(v=undef) = !is_undef(v) ? v : !is_undef($sevenseg_v) ? $sevenseg_v : 0;
function _sevenseg_sx(sx=undef) = !is_undef(sx) ? sx : !is_undef($sevenseg_sx) ? $sevenseg_sx : 5;
function _sevenseg_sy(sy=undef) = !is_undef(sy) ? sy : !is_undef($sevenseg_sy) ? $sevenseg_sy : 5;
function _sevenseg_spc(spc=undef) = !is_undef(spc) ? spc : !is_undef($sevenseg_spc) ? $sevenseg_spc : 1.5;
function _sevenseg_t(t=undef) = !is_undef(t) ? t : !is_undef($sevenseg_t) ? $sevenseg_t : 1;
function _sevenseg_r(r=undef) = !is_undef(r) ? r : !is_undef($sevenseg_r) ? $sevenseg_r : 0;
function _sevenseg_dec(dec=undef) = !is_undef(dec) ? dec : !is_undef($sevenseg_dec) ? $sevenseg_dec : 0;
function _sevenseg_dec_spc(dec_spc=undef) = !is_undef(dec_spc) ? dec_spc : !is_undef($sevenseg_dec_spc) ? $sevenseg_dec_spc : 0.6;
function _sevenseg_pad(pad=undef) = !is_undef(pad) ? pad : !is_undef($sevenseg_pad) ? $sevenseg_pad : 0;
function _sevenseg_pos_sign(pos_sign=undef) = !is_undef(pos_sign) ? pos_sign : !is_undef($sevenseg_pos_sign) ? $sevenseg_pos_sign : false;

function numsize(
    v=undef, sx=undef, sy=undef, spc=undef, t=undef, r=undef,
    dec=undef, dec_spc=undef, pad=undef, pos_sign=false
) = (
    let(
        v = _sevenseg_v(v),
        sx = _sevenseg_sx(sx),
        sy = _sevenseg_sy(sy),
        spc = _sevenseg_spc(spc),
        t = _sevenseg_t(t),
        r = _sevenseg_r(r),
        dec = _sevenseg_dec(dec),
        dec_spc = _sevenseg_dec_spc(dec_spc),
        pad = _sevenseg_pad(pad),
        pos_sign = _sevenseg_pos_sign(pos_sign),

        rspc = sx * spc,
        nd = max(pad, numdig(v)),
        int_spc = ((pos_sign || v < 0) ? rspc : 0) + rspc * nd,
    )
    int_spc + (dec > 0 ? dec_spc : 0) + dec * rspc
);


seg_T = [[0, 2], [1, 2]];
seg_M = [[0, 1], [1, 1]];
seg_B = [[0, 0], [1, 0]];

seg_L = [[0, 0], [0, 2]];
seg_R = [[1, 0], [1, 2]];
seg_C = [[.5, 0], [.5, 2]];

seg_LT = [[0, 1], [0, 2]];
seg_LB = [[0, 0], [0, 1]];

seg_RT = [[1, 1], [1, 2]];
seg_RB = [[1, 0], [1, 1]];

sign_neg = [ seg_M ];
sign_pos = [ seg_M, [[.5, .5], [.5, 1.5]] ];
sign_point = [ [[0, 0], [0, 0]] ];

digits = [
    /* 0 */ [ seg_T, seg_B, seg_L, seg_R ],
    /* 1 */ [ seg_C ],
    /* 2 */ [ seg_T, seg_M, seg_B, seg_RT, seg_LB ],
    /* 3 */ [ seg_T, seg_B, seg_M, seg_R ],
    /* 4 */ [ seg_LT, seg_R, seg_M ],
    /* 5 */ [ seg_T, seg_M, seg_B, seg_LT, seg_RB ],
    /* 6 */ [ seg_T, seg_B, seg_L, seg_RB, seg_M ],
    /* 7 */ [ seg_T, seg_R ],

    /* 8 */ [ seg_T, seg_B, seg_L, seg_R, seg_M ],
    /* 9 */ [ seg_T, seg_R, seg_LT, seg_M ],
];

module _sevenseg_digit(segs, sx=1, sy=1, t=1, r=0) {
    for (seg = segs) {
        squarex(
            x1 = seg[0].x * sx - t/2, x2=seg[1].x * sx + t/2,
            y1 = seg[0].y * sy - t/2, y2=seg[1].y * sy + t/2,
            r1=r, r2=r, r3=r, r4=r
        );
    }
}

module sevenseg_digit(d, sx=1, sy=1, t=1, r=0) {
    _sevenseg_digit(digits[d], sx=sx, sy=sy, t=t, r=r);
}

module sevenseg_num(
    v=undef, sx=undef, sy=undef, spc=undef, t=undef, r=undef,
    dec=undef, dec_spc=undef, pad=undef, pos_sign=false
) {
    let(
        v = _sevenseg_v(v),
        sx = _sevenseg_sx(sx),
        sy = _sevenseg_sy(sy),
        spc = _sevenseg_spc(spc),
        t = _sevenseg_t(t),
        r = _sevenseg_r(r),
        dec = _sevenseg_dec(dec),
        dec_spc = _sevenseg_dec_spc(dec_spc),
        pad = _sevenseg_pad(pad),
        pos_sign = _sevenseg_pos_sign(pos_sign),

        rspc = sx * spc,
        nd = max(pad, numdig(v)),

        absv = abs(v),

        sign_spc = (pos_sign || v < 0) ? rspc : 0,
        int_spc = sign_spc + rspc * (nd),
        total_spc = int_spc + (dec > 0 ? dec_spc : 0) + dec * rspc,
    ) {
        if (sign_spc) {
            _sevenseg_digit(v < 0 ? sign_neg : sign_pos, sx=sx, sy=sy, t=t, r=r);
        }
        tx(sign_spc)
        for (dig = [0 : nd - 1]) {
            tx(rspc * dig)
            _sevenseg_digit(digits[floor(absv / 10 ^ (nd - dig - 1)) % 10], sx=sx, sy=sy, t=t, r=r);
        }

        if (dec > 0) {
            tx(int_spc) {
                tx(dec_spc/2)
                _sevenseg_digit(sign_point, sx=sx, sy=sy, t=t, r=r);
                tx(dec_spc * sx)
                for (dig = [0 : dec - 1]) {
                    tx(rspc * dig)
                    _sevenseg_digit(digits[floor(10^(dig - 2) + absv * 10 ^ (dig + 1)) % 10], sx=sx, sy=sy, t=t, r=r);
                }
            }

        }
    }
}

module sevenseg_def(
    v=undef, sx=undef, sy=undef, spc=undef, t=undef, r=undef,
    dec=undef, dec_spc=undef, pad=undef, pos_sign=false
) {
    $sevenseg_v = _sevenseg_v(v);
    $sevenseg_sx = _sevenseg_sx(sx);
    $sevenseg_sy = _sevenseg_sy(sy);
    $sevenseg_spc = _sevenseg_spc(spc);
    $sevenseg_t = _sevenseg_t(t);
    $sevenseg_r = _sevenseg_r(r);
    $sevenseg_dec = _sevenseg_dec(dec);
    $sevenseg_dec_spc = _sevenseg_dec_spc(dec_spc);
    $sevenseg_pad = _sevenseg_pad(pad);
    $sevenseg_pos_sign = _sevenseg_pos_sign(pos_sign);

    $sevenseg_size = [
        numsize(),
        2 * $sevenseg_sy
    ];

    children();
}
