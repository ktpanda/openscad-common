import sys
import os
import math
from io import StringIO

from collections import namedtuple

#EPSILON=1e-10

class vtx(namedtuple('_vtx', ('x', 'y', 'z'))):
    def __new__(cls, x=0, y=0, z=0):
        if isinstance(x, tuple):
            return super().__new__(cls, *x)
        else:
            return super().__new__(cls, x, y, z)

    def __add__(self,o):
        return vtx(self.x + o.x, self.y + o.y, self.z + o.z)

    def __sub__(self,o):
        return vtx(self.x - o.x, self.y - o.y, self.z - o.z)

    def __mul__(self,o):
        if isinstance(o, vtx):
            return self.dot(o)

        return vtx(self.x * o, self.y * o, self.z * o)

    def __rmul__(self, o):
        return vtx(self.x * o, self.y * o, self.z * o)

    def __div__(self,s):
        return vtx(self.x / s, self.y / s, self.z / s)

    def __rdiv__(self,s):
        raise ArithmeticError("cannot divide by vect")

    def __neg__(self):
        return vtx(-self.x, -self.y, -self.z)

    def __pos__(self):
        return vtx(self.x, self.y, self.z)

    def len(self):
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

    __abs__ = len

    #def __eq__(self,o):
    #    return abs(self.x - o.x)<EPSILON and abs(self.y - o.y)<EPSILON and abs(self.z - o.z)<EPSILON

    #def __ne__(self,o):
    #    return not self.__eq__(o)


    def dist(self, o):
        return abs(self - o)

    def len2(self):
        return self.x * self.x + self.y * self.y + self.z * self.z

    def dot(self,o):
        return self.x * o.x + self.y * o.y + self.z * o.z

    def __xor__(self,o):
        x = self.y * o.z - o.y*self.z
        y = o.x * self.z - self.x * o.z
        z = self.x * o.y - o.x * self.y
        return vtx(x, y, z)

    def normal(self):
        l = 1.0 / self.len()
        return vtx(self.x * l, self.y * l, self.z * l)

    # so we can do x, y, z = vtx
    def __len__(self):
        return 3

    def __iter__(self):
        yield self.x
        yield self.y
        yield self.z

    def __str__(self):
        return f"<{self.x:.4f}, {self.y:.4f}, {self.z:.4f}>"

    def __repr__(self):
        return str(self)
        return f"vtx({self.x:.4f}, {self.y:.4f}, {self.z:.4f})"

def generate_poly_params(faces):
    out_vertices = []
    out_faces = []
    vertex_index = {}
    for face in faces:
        curface = []
        for pt in face:
            try:
                idx = vertex_index[pt]
            except KeyError:
                idx = vertex_index[pt] = len(out_vertices)
                out_vertices.append(pt)
            curface.append(idx)
        out_faces.append(curface)
    return out_vertices, out_faces

def emit_scad_sequence(f, seq):
    f.write('[')
    itr = iter(seq)
    try:
        v = next(itr)
    except StopIteration:
        pass
    else:
        emit_scad_val(f, v)
        for v in itr:
            f.write(',')
            emit_scad_val(f, v)
    f.write(']')

def emit_scad_val(f, val):
    if isinstance(val, str):
        val = val.replace('\\', r'\\')
        val = val.replace('"', r'\"')
        val = val.replace('\r', r'\r')
        val = val.replace('\n', r'\n')
        f.write(f'"{val}"')
    elif isinstance(val, (int, float)):
        f.write(repr(val))
    elif hasattr(val, '__len__'):
        emit_scad_sequence(f, val)

def emit_polyhedron(f, modname, faces, convexity=10):
    vertices, faces = generate_poly_params(faces)
    f.write(f'module {modname}() {{\n')
    f.write(f'    polyhedron(\n        points=')
    emit_scad_val(f, vertices)
    f.write(f',\n        faces=')
    emit_scad_val(f, faces)
    f.write(f',\n    convexity={convexity}\n    );\n')

    f.write('}\n')
