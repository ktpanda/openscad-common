#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
from pathlib import Path

RX_PRINT_OBJ = re.compile(r'; printing object (\S+)')

def normalize_obj(txt):
    txt = txt.lower()
    if txt.endswith('.stl'):
        txt = txt[:-4]
    return txt

def do_replace_gcode(inf, outf, colors, args):
    last_color = None

    for line in inf:
        if m := RX_PRINT_OBJ.match(line):
            obj = m.group(1)
            outf.write(line)
            clr = colors.get(normalize_obj(obj))
            if clr is not None and clr != last_color:
                if last_color is not None:
                    if args.pos:
                        x, y = args.pos
                        outf.write(f'G0 X{x} Y{y} F10000\n')
                    outf.write('M600\n')
                outf.write(f';COLOR_CHANGE,T0,#{clr}\n')
                last_color = clr
        else:
            outf.write(line)

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument(
        '-g', '--gcode', type=Path,
        help='GCODE file to insert color changes into')
    p.add_argument(
        '-o', '--obj', nargs=2, metavar=('OBJ', 'COLOR'), action='append',
        help='Objects and the corresponding hex color (e.g. FF0000)')
    p.add_argument(
        '-p', '--pos', nargs=2, type=float, default=None,
        help='Position to move extruder to before color change')

    args = p.parse_args()

    colors = {}
    for obj, color in args.obj:
        colors[normalize_obj(obj)] = color.upper()

    if args.gcode:
        outfn = args.gcode.with_suffix(args.gcode.suffix + '.tmp')

        try:
            with args.gcode.open('r', encoding='utf8') as inf:
                with outfn.open('w', encoding='utf8') as outf:
                    do_replace_gcode(inf, outf, colors, args)

            os.replace(outfn, args.gcode)
            outfn = None
        finally:
            if outfn:
                try:
                    outfn.unlink()
                except OSError:
                    pass
    else:
        do_replace_gcode(sys.stdin, sys.stdout, colors, args)

if __name__ == '__main__':
    main()
