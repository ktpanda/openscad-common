'''
stl
---

Contains functions and utilities for reading and writing STL files
'''

import sys
import re
import argparse
import traceback
import struct
import math
from pathlib import Path
from io import BytesIO

from collections import namedtuple

STL_HEADER = struct.Struct('<80sI')
STL_TRI = struct.Struct('<12fh')

class Point(namedtuple('_Point', ['x', 'y', 'z'])):
    def __repr__(self):
        return f'[{self.x:.4f}, {self.y:.4f}, {self.z:.4f}]'

    def __add__(self, o):
        return Point(self.x + o.x, self.y + o.y, self.z + o.z)

    __radd__ = __add__

    def __sub__(self, o):
        return Point(self.x - o.x, self.y - o.y, self.z - o.z)

    def __rsub__(self, o):
        return Point(o.x - self.x, o.y - self.y, o.z - self.z)

    def __mul__(self, o):
        return Point(self.x * o, self.y * o, self.z * o)

    __rmul__ = __mul__

    def __neg__(self):
        return Point(-self.x, -self.y, -self.z)

    def dot(self, o):
        return self.x * o.x + self.y * o.y + self.z * o.z

    def midpoint(self, o):
        return Point((self.x + o.x) / 2, (self.y + o.y) / 2, (self.z + o.z) / 2)

    def square_length(self):
        dx = self.x
        dy = self.y
        dz = self.z
        return dx*dx + dy*dy + dz*dz

    def length(self):
        return math.sqrt(self.square_length())

    def square_dist(self, o):
        dx = o.x - self.x
        dy = o.y - self.y
        dz = o.z - self.z
        return dx*dx + dy*dy + dz*dz

    def dist(self, o):
        return math.sqrt(self.square_dist(o))

class STL:
    def __init__(self, path=None, name=''):
        self.path = path
        self.name = name
        self.tridata = []
        if path is not None:
            self.read_from_file(path)

    def read_from(self, fp):
        hdr = fp.read(84)
        asciihdr = hdr.decode('ascii', 'replace')

        # For a binary STL to be mistaken as ASCII, it would have to have:
        # 1. A title starting with 'solid ' and 80 characters long (so there is no terminating NUL)
        # 2. More than 2**24 triangles (so the MSB of the triangle count is not zero)

        is_ascii = re.match(r'\s*solid\s+', asciihdr) and b'\0' not in hdr

        self.tridata = tridata = []

        if is_ascii:
            buf = asciihdr
            while True:
                data = fp.read(8192)
                if not data:
                    break
                buf += data.decode('ascii', 'replace')
                lines = buf.split('\n')
                buf = lines.pop()

                for line in lines:
                    pass
        else:
            name, tricount = STL_HEADER.unpack(hdr)

            self.name = name.split(b'\0', 1)[0].decode('utf8')
            for i in range(tricount):
                data = fp.read(50)
                tri_data = STL_TRI.unpack(data)

                norm = tri_data[0:3]
                v1 = Point(*tri_data[3:6])
                v2 = Point(*tri_data[6:9])
                v3 = Point(*tri_data[9:12])
                attr = tri_data[12]
                #if attr != 0:
                #    raise ValueError(f'Unexpected non-zero triangle attribute at {fp.tell()-2}: {attr}')

                tridata.append((norm, v1, v2, v3, attr))

    def read_from_file(self, path):
        with open(path, 'rb') as fp:
            self.read_from(fp)

    def write_to(self, fp):
        name = self.name.encode('utf8')[:80].ljust(80, b'\0')
        fp.write(name)
        fp.write(struct.pack('<I', len(self.tridata)))
        data = [None] * 13
        ex = data.extend
        ap = data.append
        for norm, v1, v2, v3, attr in self.tridata:
            data[0:3] = norm
            data[3:6] = v1
            data[6:9] = v2
            data[9:12] = v3
            data[12] = attr
            fp.write(STL_TRI.pack(*data))

    def write_to_file(self, path):
        with open(path, 'wb') as fp:
            self.write_to(fp)

    def frombytes(self, data):
        self.read_from(BytesIO(data))

    def tobytes(self):
        bio = BytesIO()
        self.write_to(bio)
        return bio.getvalue()

    def sort(self):
        tridata = self.tridata
        for i, (norm, v1, v2, v3, attr) in enumerate(tridata):
            # Can't just sort the points because that might change the winding
            minv = min(v1, v2, v3)
            if v2 == minv:
                tridata[i] = norm, v2, v3, v1, attr
            elif v3 == minv:
                tridata[i] = norm, v3, v1, v2, attr

        tridata.sort(key=lambda tri: (tri[1], tri[2], tri[3], tri[0]))

    def transform_points(self, func, threshold=(1.0/256.0), division_limit=5):
        tf = Transformer(func, threshold, division_limit)
        tridata = self.tridata
        faces = []
        for i, (norm, v1, v2, v3, attr) in enumerate(tridata):
            p1 = tf.point_index(v1)
            p2 = tf.point_index(v2)
            p3 = tf.point_index(v3)
            faces.append((p1, p2, p3))


        tf.subdivide_faces(faces)
        del tridata[:]

        xf_points = tf.xf_points

        # openscad doesn't care about normal when importing
        norm = (0.0, 0.0, 0.0)

        for p1, p2, p3 in tf.output_faces:

            v1 = xf_points[p1]
            v2 = xf_points[p2]
            v3 = xf_points[p3]
            tridata.append((norm, v1, v2, v3, 0))

    def project_cylinder(self):
        # x -> theta
        # y -> rho
        # z -> z
        self.transform_points(lambda pt: Point(pt.y * math.cos(pt.x), pt.y * math.sin(pt.x), pt.z))

class Edge:
    __slots__ = ['p1', 'p2', 'mpt']

    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
        self.mpt = None


class Transformer:
    def __init__(self, func, threshold=(1.0/256.0), division_limit=5):
        self.func = func
        self.orig_points = []
        self.xf_points = []
        self.point_map = {}
        self.edge_map = {}

        self.threshold = threshold
        self.sqr_threshold = threshold * threshold
        self.division_limit = division_limit
        self.output_faces = []

    def point_index(self, pt, xpt=None):
        pm = self.point_map
        ri = pm.get(pt)
        if ri is None:
            pl = self.orig_points
            ri = pm[pt] = len(pl)
            pl.append(pt)
            if xpt is None:
                xpt = self.func(pt)
            self.xf_points.append(xpt)
        return ri

    def get_edge(self, p1, p2):
        rv = self.edge_map.get((p1, p2))
        if rv is None:
            rv = Edge(p1, p2)
            self.edge_map[(p1, p2)] = rv
            self.edge_map[(p2, p1)] = Edge(p2, p1)
        return rv

    def subdivide_edge(self, edge):
        # edge.mpt is None only if we have not checked if we need to subdivide.
        # It will be set to False if it doesn't need to be subdivided.
        if edge.mpt is None:
            op1 = self.orig_points[edge.p1]
            op2 = self.orig_points[edge.p2]
            xp1 = self.xf_points[edge.p1]
            xp2 = self.xf_points[edge.p2]

            # If the transformed points are closer than our threshold, don't subdivide.
            if xp1.square_dist(xp2) < self.sqr_threshold:
                edge.mpt = False
                return

            # Calculate the midpoint of the original points
            ompt = op1.midpoint(op2)

            # Transform the midpoint
            xform_of_midpoint = self.func(ompt)

            # Calculate the normal vector from xp1 to xp2
            norm = (xp2 - xp1) * (1.0 / xp1.dist(xp2))

            # Project the transformed midpoint onto the plane perpendicular to the edge
            # which contains xp1. The distance from this point to xp1 is the same as the
            # distance of the transformed midpoint to the transformed line.
            #
            # We skip the step of adding xp1, because we would just have to subtract it
            # again to get the distance.
            xfmid_rel_to_xp1 = xform_of_midpoint - xp1
            projected = xfmid_rel_to_xp1 - norm * norm.dot(xfmid_rel_to_xp1)

            # If the transformation of the original midpoint is close to the midpoint of the
            # transformed edge, don't subdivide.
            if projected.square_length() < self.sqr_threshold:
                edge.mpt = False
                return

            edge.mpt = self.point_index(ompt, xform_of_midpoint)

            # Subdivide the reverse edge the same way
            reverse_edge = self.edge_map[(edge.p2, edge.p1)]
            reverse_edge.mpt = edge.mpt

    def subdivide_faces(self, faces):
        limit = self.division_limit
        for p1, p2, p3 in faces:
            self._subdivide_face(p1, p2, p3, limit)

    def _subdivide_face(self, p1, p2, p3, limit):
        if limit == 0:
            self.output_faces.append((p1, p2, p3))
            return

        limit -= 1

        e1 = self.get_edge(p1, p2)
        e2 = self.get_edge(p2, p3)
        e3 = self.get_edge(p3, p1)

        self.subdivide_edge(e1)
        self.subdivide_edge(e2)
        self.subdivide_edge(e3)

        if not e1.mpt:
            if e2.mpt:
                # If edge 1 is not subdivided, but edge 2 is, rotate so that edge 2
                # becomes edge 1
                e1, e2, e3 = e2, e3, e1
            elif e3.mpt:
                # If edge 1 and edge 2 are not subdivided, but edge 3 is, rotate so that
                # edge 3 becomes edge 1
                e1, e2, e3 = e3, e1, e2
            else:
                # No edges are subdivided.
                self.output_faces.append((p1, p2, p3))
                return

        # At this point, everything has been rotated so that edge 1 is subdivided.
        # Now there are only 4 cases to consider:
        # * No other edges are subdivided
        # * Edge 2 is subdivided
        # * Edge 3 is subdivided
        # * All edges are subdivided

        p1 = e1.p1
        p2 = e2.p1
        p3 = e3.p1

        if e2.mpt:
            if e3.mpt:
                # All edges are subdivided
                self._subdivide_face(e1.mpt, e2.mpt, e3.mpt, limit)
                self._subdivide_face(e1.mpt, e3.mpt, p1, limit)
                self._subdivide_face(e2.mpt, e1.mpt, p2, limit)
                self._subdivide_face(e3.mpt, e2.mpt, p3, limit)
            else:
                self._subdivide_face(e1.mpt, p2, e2.mpt, limit)
                self._subdivide_face(e1.mpt, e2.mpt, p3, limit)
                self._subdivide_face(e1.mpt, p3, p1,  limit)
        else:
            if e3.mpt:
                self._subdivide_face(e1.mpt, p2, p3, limit)
                self._subdivide_face(e1.mpt, p3, e3.mpt, limit)
                self._subdivide_face(e1.mpt, e3.mpt, p1,  limit)
            else:
                self._subdivide_face(e1.mpt, p2, p3, limit)
                self._subdivide_face(e1.mpt, p3, p1, limit)
