#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
from pathlib import Path

RX_EXTRUDER_COLOR = re.compile(r';\s*extruder_colou?r\s*=\s*(.*)')
RX_OPT = re.compile(r'(;\s*)([^=]+\s*)(=\s*)(.*)')
RX_TOOL_CHANGE = re.compile(r'T(\d+)')

def normalize_obj(txt):
    txt = txt.lower()
    if txt.endswith('.stl'):
        txt = txt[:-4]
    return txt

def do_replace_gcode(inf, outf, args):
    last_tool = 0
    extruder_colors = []

    for line in inf:
        if m := RX_EXTRUDER_COLOR.match(line):
            colors = m.group(1).split(';')
            extruder_colors = [v.strip().strip('#') for v in colors]
            #print(f'{extruder_colors}', file=sys.stderr)

        if m := RX_OPT.match(line):
            print(f'{line!r} {m}', file=sys.stderr)
            prefix = m.group(1)
            key_part = m.group(2)
            mid = m.group(3)
            val = m.group(4)
            key = key_part.strip()
            if (key in {'wipe', 'cooling', 'deretract_speed'} or 'temperature' in key or 'fan_' in key or key.startswith('filament') or
                key.startswith('retract') or key.startswith('extruder') or key.startswith('slowdown') or key.startswith('fan') or key.startswith('wiping')):
                if ';' in val:
                    val = val.split(';')[0]
                elif ',' in val:
                    val = val.split(',')[0]

                if not val:
                    val = '""'
                outf.write(f'{prefix}{key_part}{mid}{val}\n')
                continue

        if m := RX_TOOL_CHANGE.match(line):
            tool = int(m.group(1))
            outf.write(f';original gcode: {line}')
            if tool != last_tool:
                #print(f'{tool, extruder_colors}', file=sys.stderr)
                try:
                    clr = extruder_colors[tool]
                    outf.write(f';COLOR_CHANGE,T0,#{clr}\n')
                except IndexError:
                    pass
                outf.write(f'M117 Tool {tool}\n')
                if args.pos:
                    x, y = args.pos
                    outf.write(f'G0 X{x} Y{y} F10000\n')
                outf.write('M600\n')
                outf.write(f'M117 Change ok\n')
                last_tool = tool
        else:
            outf.write(line)

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument(
        '-g', '--gcode', type=Path,
        help='GCODE file to insert color changes into')
    p.add_argument(
        '-p', '--pos', nargs=2, type=float, default=None,
        help='Position to move extruder to before color change')

    args = p.parse_args()

    if args.gcode:
        outfn = args.gcode.with_suffix(args.gcode.suffix + '.tmp')

        try:
            with args.gcode.open('r', encoding='utf8') as inf:
                with outfn.open('w', encoding='utf8') as outf:
                    do_replace_gcode(inf, outf, args)

            os.replace(outfn, args.gcode)
            outfn = None
        finally:
            if outfn:
                try:
                    outfn.unlink()
                except OSError:
                    pass
    else:
        do_replace_gcode(sys.stdin, sys.stdout, args)

if __name__ == '__main__':
    main()
