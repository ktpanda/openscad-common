$component_bend = 0;
$pin_length = 2;

$preview_slice_max = 500;
$preview_slice_vc = undef;
$preview_slice_vs = undef;
$preview_slice_axis = undef;
$preview_sliced_color = false;

eps = 0.01;
eps2 = 2 * eps;

// M2.5
screw_diameter_m2 = 2.8;
screw_head_diameter_m2 = 4.8;
screw_head_length_m2 = 1.6;
nut_diameter_m2 = 5.2;  // 4.90 + .3 margin
mount_nut_width_m2 = nut_diameter_m2;


// M3
screw_diameter_m3 = 3.2;
nut_diameter_m3 = 5.85;  // 5.45 + .4 margin
mount_nut_width_m3 = nut_diameter_m3;

hexhole_height = 3;
hexhole_width = 5.8;
hexhole_width_tight = 5.5;
