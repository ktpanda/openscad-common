include <consts.scad>
use <funcs.scad>

module cubept(p1, p2) {
    cubespan([p1.x, p2.x], [p1.y, p2.y], [p1.z, p2.z]);
}

module cubex(
    x1=undef, x2=undef, xc=undef, xs=undef,
    y1=undef, y2=undef, yc=undef, ys=undef,
    z1=undef, z2=undef, zc=undef, zs=undef,
    r1=undef, r2=undef, r3=undef, r4=undef,
    r=0,
    n1=undef, n2=undef, n3=undef, n4=undef,
    n=undef
) {
    cubespan(
        span(x1, x2, xc, xs, "cubex (x)"),
        span(y1, y2, yc, ys, "cubex (y)"),
        span(z1, z2, zc, zs, "cubex (z)"),
        r1, r2, r3, r4, r,
        n1, n2, n3, n4, n
    );
}

module cylx(
    z1=undef, z2=undef, zc=undef, zs=undef,
    d=undef, d1=undef, d2=undef, r=undef, r1=undef, r2=undef, ext=0
) {
    zpt = span(z1, z2, zc, zs, "cylx (z)");
    tz(zpt[0]-ext)
    cylinder(zpt[1]-zpt[0] + 2*ext, r=r, r1=r1, r2=r2, d=d, d1=d1, d2=d2);
}

function lathe_poly_points(r, d) = let(
    scl = is_undef(r) ? .5 : 1,
    pts = is_undef(r) ? d : r,
    zbot = pts[0][0],
    ztop = at(pts, -1)[0],
) uniq([
    [0, zbot],
    each [for (pt = pts) [scl * pt.y, pt.x]],
    [0, ztop]
]);

module lathe_poly(r, d) {
    polyx(lathe_poly_points(r, d));
}

module lathe(r, d) {
    rotate_extrude(convexity=10) polyx(lathe_poly_points(r, d));
}

module lathe_ext_y(y1, y2, r, d) {
    pts = lathe_poly_points(r, d);
    render(10) {
        ty([y1, y2])
        rotate_extrude(convexity=10) polyx(pts);
        xzy() lxt(x1, x2) polyx(rfx(pts));
    }
}

module cubespan(x, y, z, r1, r2, r3, r4, r=0, n1=undef, n2=undef, n3=undef, n4=undef, n=undef) {
    r1 = coalesce(r1, r);
    r2 = coalesce(r2, r);
    r3 = coalesce(r3, r);
    r4 = coalesce(r4, r);

    xpt = sortpt(x);
    ypt = sortpt(y);
    zpt = sortpt(z);

    translate([xpt[0], ypt[0], zpt[0]])
    if (r1 == 0 && r2 == 0 && r3 == 0 && r4 == 0) {
        cube([
            xpt[1] - xpt[0],
            ypt[1] - ypt[0],
            zpt[1] - zpt[0],
        ]);
    } else {
        linear_extrude(zpt[1] - zpt[0]) {
            polygon(roundsquare_pts(xpt[1] - xpt[0], ypt[1] - ypt[0], r1, r2, r3, r4, n, n1, n2, n3, n4));
        }
    }
}

module squarex(
    x1=undef, x2=undef, xc=undef, xs=undef,
    y1=undef, y2=undef, yc=undef, ys=undef,
    r1=undef, r2=undef, r3=undef, r4=undef, r=0,
    n1=undef, n2=undef, n3=undef, n4=undef, n=undef

) {
    r1 = coalesce(r1, r);
    r2 = coalesce(r2, r);
    r3 = coalesce(r3, r);
    r4 = coalesce(r4, r);

    if (r1 == 0 && r2 == 0 && r3 == 0 && r4 == 0) {
        xpt = span(x1, x2, xc, xs, "squarex (x)");
        ypt = span(y1, y2, yc, ys, "squarex (y)");

        translate([xpt[0], ypt[0]])
        square([
            xpt[1] - xpt[0],
            ypt[1] - ypt[0]
        ]);
    } else {
        polygon(squarepts(
            x1, x2, xc, xs,
            y1, y2, yc, ys,
            r1, r2, r3, r4, r,
            n1, n2, n3, n4, n
        ));
    }
}

// ================================================================================
// Enhanced extrusions
// ================================================================================


// np = Number of points in extruded polygon (minimum 3)
// ns = Number of slices in the extrusion (minimum 2)

/*
|  Consider a pentagonal prism with (x, y) points [A, B, C, D, E] and height H.
|
|  In this case, np = 5 and ns = 2.
|
|  Let:
|  A0 = [A.x, A.y, 0], A1 = [A.x, A.y, H],
|  B0 = [B.x, B.y, 0], B1 = [B.x, B.y, H],
|  etc.
|
|  The points given to the polyhedron module are in the order:
|  [
|      A0, B0, C0, D0, E0,
|      A1, B1, C1, D1, E1
|  ]
|
|  So, the formula for the index of a given point on a given slice is:
|
|  (slice_index * ns) + point_index
|
|  Each side of the polygon is divided up like this:
|
|  A1 ---- B1 ---- C1 ---- D1 ---- E1 ---- A1
|  |      /|      /|      /|      /|      /|
|  |     / |     / |     / |     / |     / |
|  |    /  |    /  |    /  |    /  |    /  |
|  |   /   |   /   |   /   |   /   |   /   |
|  |  /    |  /    |  /    |  /    |  /    |
|  | /     | /     | /     | /     | /     |
|  A0 ---- B0 ---- C0 ---- D0 ---- E0 ---- A0
|
| The triangles are therefore:
|
|  [A0, B1, A1]
|  [A0, B0, B1]
|
|  [B0, C1, B1]
|  [B0, C0, C1]
|
|  [C0, D1, C1]
|  [C0, D0, D1]
|
|  [D0, E1, D1]
|  [D0, E0, E1]
|
|  [E0, A1, E1]
|  [E0, A0, A1]
|
*/

// base1 = base index of first slice
// base2 = base index of next slice
// i = index of point within polygon
// tri = which triangle of the quad ( 0 or 1 )

// A0 = base1 + i
// B0 = base1 + ((i + 1) % np)
// A1 = base2 + i
// B2 = base2 + ((i + 1) % np)
function _extrusion_side_face(np, base1, base2, i, tri) = (
    tri == 0 ?
    [
        /* A0 */ base1 + i,
        /* B1 */ base2 + ((i+1) % np),
        /* A1 */ base2 + i,
    ] :
    [
        /* A0 */ base1 + i,
        /* B0 */ base1 + ((i+1) % np),
        /* B1 */ base2 + ((i+1) % np),
    ]
);

function _extrusion_side_faces(np, ns) = [
    for (base = [0 : np : np * (ns - 2)], i = [0 : np-1], side = [0, 1])
    _extrusion_side_face(np, base, base + np, i, side)
];

function _extrusion_end_faces(np, tp) = [
    // bottom face - first polygon, reverse order
    [ for (i = [np - 1 : -1 : 0]) i ],

    // top face - last polygon, forward order
    [ for (i = [tp - np : 1 : tp - 1]) i ]
];

function _extrusion_end_faces_convex(np, tp) = [
    // bottom face - first polygon, reverse order
    for (i = [1 : np - 2]) [0, i + 1, i],

    // top face - last polygon, forward order
    let (base = tp - np)
    for (i = [1 : np - 2]) [base, base + i, base + i + 1],

];

function extrusion_faces(np, ns = 2, closed = false, convex = false) = [
    each (
        closed ?
        let(base1 = np * (ns - 1)) [
            for (i = [0 : np-1], side = [0, 1])
            _extrusion_side_face(np, base1, 0, i, side)
        ] :
        convex ? _extrusion_end_faces_convex(np, ns * np) :
        _extrusion_end_faces(np, ns * np)
    ),
    each _extrusion_side_faces(np, ns),
];

function slice_faces(lst, closed = false) = extrusion_faces(len(lst[0]), len(lst), closed);

// ======================================================================
// path_extrude

function _tri_inset(v, amt=.1) = let(
    mp = (v[0] + v[1] + v[2]) / 3,
) [
    v[0] + normto(v[0], mp) * amt,
    v[1] + normto(v[1], mp) * amt,
    v[2] + normto(v[2], mp) * amt,
];

module _debug_polyhedron(vertices, faces, points, slices, path, convexity=4, debug=0) {
    if (debug == 1 && !is_undef(path)) {
        for (i = indices(path)) let(v=path[i]) {
            color(["red", "green", "blue"][i % 3]) multmatrix(v[0]) xzy() lxt(0, 0.1) polygon(points);
        };
    } else if (debug == 2 && !is_undef(slices)) {
        for (i = indices(slices)) let(v=slices[i]) {
            color(["red", "green", "blue"][i % 3]) for (pt = v) translate(pt) cube(.1, center=true);
        }
    } else if (debug == 3) {
        for (i = indices(faces)) color(["red", "green", "blue", "gold"][i % 4]) let(f=faces[i], vtx=[for (j = f) vertices[j]]) {
            nvtx = _tri_inset(vtx);


            cpr = -cross(vtx[1] - vtx[0], vtx[2] - vtx[0]);
            n = cpr / norm(cpr);

            for (i = indices(nvtx)) cylpt(nvtx[i], nvtx[(i + 1) % len(nvtx)], d=.05);
        }
    } else {
        polyhedron(vertices, faces, convexity=convexity);
    }
}

module slice_extrude(slices, closed = false, convexity = 4, debug = 0) {
    vertices = flatten(slices);
    faces = slice_faces(slices, closed);

    if (debug == 0) {
        polyhedron(vertices, faces, convexity=convexity);
    } else {
        _debug_polyhedron(vertices, faces, slices, undef, convexity, debug);
    }
}

module path_extrude(points, path, closed = false, convexity = 4, debug=0) {
    points3 = xzpoints(poly_ccw(points));
    slices = [for (v = path) xform_pts(v[0], points3)];

    vertices = flatten(slices);
    faces = slice_faces(slices, closed);

    if (debug == 0) {
        polyhedron(vertices, faces, convexity=convexity);
    } else {
        _debug_polyhedron(vertices, faces, slices, path, convexity, debug);
    }
}

// ======================================================================


// ======================================================================
// helix_extrude

function _helix_angles(angle, start_angle, num_slices) = [for (seg = [0 : num_slices]) lerp(start_angle, angle, seg/num_slices)];
function helix_angles(angle, start_angle) = _helix_angles(angle, start_angle, ceil((angle - start_angle) * $fn/360));

function helix_points_3d(points, ang) = [
    for (pt = points) [pt.x * cos(ang), pt.x * sin(ang), pt.y]
];

module helix_extrude_sliced(points, angle=360, start_angle=0, convexity=4, convex = false) {
    num_slices = len(points) - 1;

    // angle of one segment
    slice_step = (angle - start_angle) / num_slices;

    // ensure that one segment doesn't go a full 360 degrees
    segment_size = max(1, floor(360 / slice_step - .01));

    slices = [
        for (seg = [0 : num_slices])
        helix_points_3d(points[seg], lerp(start_angle, angle, seg/num_slices))
    ];

    for (base = [0 : segment_size : num_slices - 1]) {
        end = min(base + segment_size, num_slices);
        //color((base / segment_size) % 2 == 0 ? "green" : "blue")
        polyhedron(flatten([for (i = [base : end]) slices[i]]), extrusion_faces(len(points[0]), end - base + 1, convex=convex), convexity=convexity);
    }
}

function dbg(v) = echo(debug=v) v;

function helix_translate(points, ang, ofs, scale, cofs) = let(
    scalevec = lerp([1, 1], scale, ang/360),
    xofs = ofs + (ang/360) * cofs
) [
    for (pt = points) [pt.x * scale.x + xofs.x, pt.y * scale.y + xofs.y]
];

function point_xform(f) = (function(points, ang) [for (pt = points) f(pt, ang)]);

module helix_extrude_ex(points, f, angle=360, start_angle=0, convexity=4, convex=false) {
    points = poly_ccw(points);

    helix_extrude_sliced([
        for (ang = helix_angles(angle, start_angle))
        f(points, ang)
    ], angle, start_angle, convexity, convex);
}

module helix_extrude(points, ofs=[0, 0], cofs=[0, 0], scale=[1, 1], angle=360, start_angle=0, convexity=4, convex=false) {
    helix_extrude_ex(points, function(points, ang) helix_translate(points, ang, ofs, scale, cofs), angle, start_angle, convexity, convex);
}

// ======================================================================

module lxpoly2(h, lpts, upts, lofs=[0, 0], uofs=[0, 0], convexity=4) {
    assert(len(lpts) == len(upts));

    allpts = [
        each [for (pt=lpts) [lofs.x + pt.x, lofs.y + pt.y, 0]],
        each [for (pt=upts) [uofs.x + pt.x, uofs.y + pt.y, h]]
    ];
    polyhedron(allpts, extrusion_faces(len(lpts)), convexity=convexity);
}

// ================================================================================
// Fillet / chamfer
// ================================================================================

// If r < 0, force npts to be 1. This will produce angles of [0, 90]
// and make the corner a chamfer instead of a fillet.
function _corner_npts(n, r) = r < 0 ? 1 : is_undef(n) ? max(1, ceil(5*r)) : n;

function corner_arc(r, xo=0, yo=0, ao=0, n=undef) = (
    r == 0 ? [[xo, yo]] :
    [for(i = [0 : 90 / _corner_npts(n, r) : 90]) [
        xo + abs(r) * cos(ao + i),
        yo + abs(r) * sin(ao + i)
    ]]
);

function corner_arc_1(r, xo, yo, n=undef) = corner_arc(r, xo+abs(r), yo+abs(r), 180, n);
function corner_arc_2(r, xo, yo, n=undef) = corner_arc(r, xo-abs(r), yo+abs(r), 270, n);
function corner_arc_3(r, xo, yo, n=undef) = corner_arc(r, xo+abs(r), yo-abs(r), 90, n);
function corner_arc_4(r, xo, yo, n=undef) = corner_arc(r, xo-abs(r), yo-abs(r), 0, n);

function squarepts(
    x1=undef, x2=undef, xc=undef, xs=undef,
    y1=undef, y2=undef, yc=undef, ys=undef,
    r1=undef, r2=undef, r3=undef, r4=undef,
    r=0,
    n1=undef, n2=undef, n3=undef, n4=undef, n=undef

) = let(
    xpt = span(x1, x2, xc, xs, "squarex (x)"),
    ypt = span(y1, y2, yc, ys, "squarex (y)"),
) [
    each corner_arc_1(coalesce(r1, r), xpt[0], ypt[0], coalesce(n1, n)),
    each corner_arc_2(coalesce(r2, r), xpt[1], ypt[0], coalesce(n2, n)),
    each corner_arc_4(coalesce(r4, r), xpt[1], ypt[1], coalesce(n4, n)),
    each corner_arc_3(coalesce(r3, r), xpt[0], ypt[1], coalesce(n3, n)),
];

function cubepts(
    x1=undef, x2=undef, xc=undef, xs=undef,
    y1=undef, y2=undef, yc=undef, ys=undef,
    z1=undef, z2=undef, zc=undef, zs=undef,
    r1=undef, r2=undef, r3=undef, r4=undef,
    r=0,
    n1=undef, n2=undef, n3=undef, n4=undef, n=undef

) = let(
    xpt = span(x1, x2, xc, xs, "cubex (x)"),
    ypt = span(y1, y2, yc, ys, "cubex (y)"),
    zpt = span(z1, z2, zc, zs, "cubex (z)"),
    f = squarepts(
        x1=xpt[0], x2=xpt[1],
        y1=ypt[0], y2 = ypt[1],
        r1=r1, r2=r2, r3=r3, r=r,
        n1=n1, n2=n2, n3=n3, n=n
    )
) [
    xypoints(f, zpt[0]),
    xypoints(f, zpt[1]),
];

function roundsquare_pts(width, depth, c1=0, c2=0, c3=0, c4=0, n=undef, n1=undef, n2=undef, n3=undef, n4=undef) =
squarepts(x2=width, y2=depth, r1=c1, r2=c2, r3=c3, r4=c4, n=n, n1=n1, n2=n2, n3=n3, n4=n4);

module regular_polygon(order = 4, r=1){
    angles=[ for (i = [0:order-1]) i*(360/order) ];
    coords=[ for (th=angles) [r*cos(th), r*sin(th)] ];
    polygon(coords);
}

module hexagon(d, xw=0, xl=0) {
    len2 = d / 2;
    side_length = d/(sqrt(3));
    points = [
        [-side_length / 2, -len2],
        [-side_length, 0],
        [-side_length, xl],
        [-side_length / 2, len2+xl],
        [xw + side_length / 2, len2+xl],
        [xw + side_length, xl],
        [xw + side_length, 0],
        [xw + side_length / 2, -len2],
    ];
    polygon(points);
}

module frustum_base(lz, lx1, ly1, lx2, ly2, uz, ux1, uy1, ux2, uy2) {
    points = [
        // lower
        [lx1, ly1, lz],
        [lx1, ly2, lz],
        [lx2, ly2, lz],
        [lx2, ly1, lz],
        // upper
        [ux1, uy1, uz],
        [ux1, uy2, uz],
        [ux2, uy2, uz],
        [ux2, uy1, uz],
    ];

    polyhedron(points, extrusion_faces(4));
}

module frustum(x1, y1, x2, y2, h=1, xe=0, ye=0, x1e=0, y1e=0, x2e=0, y2e=0) {
    frustum_base(
        0, x1, y1, x2, y2,
        h, x1 - xe - x1e, y1 - ye - y1e, x2 + xe + x2e, y2 + ye + y2e
    );
}

module arc(rad, start=0, end=180) {
    n = ceil($fn * (end - start) / 360);
    polygon([[0, 0], each arcpts(rad, start, end, n)]);
}

// ================================================================================
// Printing support
// ================================================================================

function _layer_height() = is_undef($layer_height) ? 0.2 : $layer_height;
function _first_layer_height() = is_undef($first_layer_height) ? 0.2 : $first_layer_height;

function layer_height(n) = n <= 0 ? 0 : n <= 1 ? _first_layer_height() : _layer_height();

// Layer number for z value. Interpolates results between layers, so you can use floor()
// or ceil() as appropriate
function layer_num_interp(z) = (
    let(lh = _layer_height(), flh=_first_layer_height())
    z <= 0 ? 0 :
    z <= flh ? 1 :
    1 + (z - flh) / lh
);

function layer_num(z) = floor(layer_num_interp(z));

function layer_z(n) = (
    let(lh = _layer_height(), flh=_first_layer_height())
    n <= 0 ? 0 :
    n <= 1 ? flh :
    (n - 1) * lh + flh
);

module bridge_support(d=5, h=0.2) {
    if ($preview) {
        color("yellow", 0.6)
        difference() {
            cylinder(h=h, d=d);
            for (r = [-45, 45])
            rz(r) cubex(xc=0, xs=d, yc=0, ys=.5, zc=0, zs=1);
        }
    } else {
        cylinder(h=h, d=d);
    }

}

module brim_tab_2d(l=3, w=3, d=6) {
    squarex(xc=0, xs=w, y1=0, y2=l + d/2);
    ty(l + d/2) circle(d=d);
}

module brim_tab(l=3, w=3, d=6, h=.4) {
    lxt(0, h) {
        brim_tab_2d(l, w, d);
    }
}

module brim_tab_corner(l=3, w=3, d=6, h=.4, ofs=10) {
    tx(-ofs) brim_tab(l, w, d, h);
    ty(-ofs) rz(-90) brim_tab(l, w, d, h);

}

module brim_tabs_2d(pts, l=3, w=3, d=6) {
    for (pt = pts)
    translate([pt.x, pt.y])
    rz(pt.z)
    brim_tab_2d(l, w, d);
}

module brim_tabs(pts, l=3, w=3, d=6, h=.4) {
    lxt(0, h) {
        brim_tabs_2d(pts, l, w, d);
    }
}

// ================================================================================
// Debugging
// ================================================================================

module pointlabel(i, font) {
    rz([45,-45])
    squarex(xs=.2, ys=2);

    tx(1.4)
    text(str(i), size=2, font=font, valign="center");
}

module show_points(points, scl=.01, color=[0, .7, 1], font="DejaVu Sans") {
    for (i = indices(points))
    let(pt = points[i])
    translate(pt)
    scale(scl) {
        color([0, 0, 0], .2) lxt(.1) difference() {
            offset(.5) pointlabel(i);
            pointlabel(i);
        }

        color(color, .5) lxt(.1) pointlabel(i, font);
    }
}

module face_camera() {
    rz($vpr.z)
    ry($vpr.y)
    rx($vpr.x)
    children();
}

module polyhedron_debug(points, faces, convexity=10, scl=.1) {
    colors = [
        [1, 0, 0],
        [0, 1, 0],
        [1, 1, 0],
        [0, 0, 1],
        [1, 0, 1],
        [0, 1, 1],
        [0, .7, 1],
        [.7, 1, 0],
        [1, 0, .7],
        [0, 1, .7],
        [1, .7, 0],
        [.7, 0, 1],
    ];



    for (i = indices(points))
    let(pt = points[i])
    translate(pt) scale(.05) {
        face_camera()
        lxt(zs=1) {
            squarex(xs=1, ys=1);
            tx(2) text(str(i), 2, font="DejaVu Sans", halign="left", valign="center");
        }
    }

    for (i = indices(faces))
    let(
        face = faces[i],
        fcolor = colors[i % len(colors)],
        pts = [for (j = face) points[j]],
        npts = len(pts),
        point_norms = [
            for (i = indices(pts))
            let(
                lp = pts[(i + npts - 1) % npts],
                cp = pts[i],
                np = pts[(i + 1) % npts],

                inorm = normalize(lerp(lp - cp, np - cp, .5)),
            ) inorm,
        ],
        offset_pts = [
            for (i = indices(pts))
            let(cp = pts[i], inorm = point_norms[i])
            cp + inorm * scl * 2
        ],
        vnorm = normalize(cross(pts[2] - pts[0], pts[1] - pts[0])),
    ) {

        color(fcolor, .25) {
            //hull() for(pt = pts) translate(pt) sphere(d=scl, $fn=8);
            for (p = [each index_pairs(pts), [len(pts) - 1, 0]])
            let(
                i1 = p[0], i2 = p[1],
                norm1 = point_norms[i1], norm2 = point_norms[i2],
                opt1 = pts[i1], opt2 = pts[i2],
                ipt1a = opt1 + norm1 * scl,
                ipt2a = opt2 + norm2 * scl,
                ipt1b = opt1 + norm1 * scl * .8,
                ipt2b = opt2 + norm2 * scl * .8,
                f1 = [opt1, opt2, ipt2a, ipt1a],
                f2 = [ipt1b, ipt2b, ipt2a, ipt1a],
                slices = [
                    f1,
                    vadd(vnorm * -scl, f2),
                ]
            )
            slice_extrude(slices);

            *linehull([each offset_points, offset_points[0]]) sphere(d=scl, $fn=8);
            *linehull([each pts, pts[0]]) sphere(d=scl, $fn=8);
        }

        color(fcolor * .75)
        for(pt = offset_pts) cylpt(pt, pt + vnorm * scl * 2, d=scl);
    }

}
