include <consts.scad>

// ================================================================================
// List functions
// ================================================================================

// reverse([1, 2, 3]) -> [3, 2, 1]
function reverse(seq) = [for(i = [len(seq) - 1 : -1 : 0]) seq[i]];

// flatten([[a, b], [c, d]]) -> [a, b, c, d]
function flatten(l) = [ for (a = l) for (b = a) b ];

/*
|  at(list, idx)
|
|  Performs list indexing, with negative indexing indicating offset from
|  the end of the list
|
|  Given: list = [10, 20, 30, 40]
|
|  at(list, 0) -> 10
|  at(list, 1) -> 20
|  at(list, -1) -> 40
|  at(list, -2) -> 30
|
*/
function at(list, idx) = list[idx < 0 ? idx + len(list) : idx];

/*
| sublst(list, s, e)
|
| Slices a list, the same way Python would.
|
| Given: list = [10, 20, 30, 40]
|
| sublst(list, 0) -> [10, 20, 30, 40]
| sublst(list, 1) -> [20, 30, 40]
| sublst(list, -1) -> [40]
| sublst(list, -2) -> [30, 40]
|
| sublst(list, 0, 3) -> [10, 20, 30]
| sublst(list, 1, 3) -> [20, 30]
| sublst(list, 1, -1) -> [20, 30]
| sublst(list, 2, 2) -> []
| sublst(list, 2, 1) -> []
*/

function indices(v) = len(v) == 0 ? [] : [for (i = [0 : len(v) - 1]) i];

function index_pairs(v) = len(v) < 2 ? [] : [for (i = [0 : len(v) - 2]) [i, i + 1]];

function pairs(v) = len(v) < 2 ? [] : [for (i = [0 : len(v) - 2]) [v[i], v[i + 1]]];

function enumerate(v) = len(v) == 0 ? [] : [for (i = [0 : len(v) - 1]) [i, v[i]]];

function sublst(list, s=0, e=undef) = (
    let(
        length = len(list),
        start = s < 0 ? length + s : s,
        end = is_undef(e) ? length : (e < 0 ? length + e : e),
        indices = end <= start ? [] : [start : end - 1],
    )
    [for (i = indices) list[i]]
);

function sum(list) = (
    let(length = len(list))
    length == 3 ? list[0] + list[1] + list[2] :
    length == 2 ? list[0] + list[1] :
    length == 1 ? list[0] :
    length == 0 ? undef :
    let(hl = floor(length / 2)) sum(sublst(list, 0, hl)) + sum(sublst(list, hl))
);

function reduce(f, list) = (
    let(length = len(list))
    length == 3 ? f(f(list[0], list[1]), list[2]) :
    length == 2 ? f(list[0], list[1]) :
    length == 1 ? list[0] :
    length == 0 ? undef :
    let(hl = floor(length / 2)) f(reduce(f, sublst(list, 0, hl)), reduce(f, sublst(list, hl)))
);

function min_idx(lst) = reduce(function(ai, bi) (lst[ai] < lst[bi] ? ai : bi), lst);
function max_idx(lst) = reduce(function(ai, bi) (lst[ai] > lst[bi] ? ai : bi), lst);


function index(lst, item) = search([item], lst)[0];

// Given a list of key-value pairs, look up the corresponding value
function getmap(lst, item) = lst[index(lst, item)][1];

// Removes adjacent duplicate elements from a list.
function uniq(lst) = (
    let(length = len(lst))
    length == 0 ? [] :
    [for (i = [0 : length - 1]) if (i == 0 || (lst[i] != lst[i - 1])) lst[i]]
);

// Takes a list and a list of indices, and returns a list of the values of the list at
// those indices.
// shuffle([81, 65, 42, 67, 99], [4, 2, 1, 3]) -> [99, 42, 65, 67]
function shuffle(lst, indices) = [for (i = indices) lst[i]];

function _raw_range(a, b, base, stepn=1, stepd=1) = (
    let(
        a1 = a - base,
        b1 = b - base,

        a2 = ceil(a1 * stepd / stepn),
        b2 = floor(b1 * stepd / stepn)
    )
    (b2 < a2 ? [] : [for (i = [a2 : b2]) base + (i * stepn) / stepd])
);

// Return all the values of the set {base + K * stepn / stepd : K is an integer}
// which are strictly greater than `a` and strictly less than `b`
function _open_range(a, b, base, stepn=1, stepd=1) = (
    [for (v = _raw_range(a, b, base, stepn, stepd)) if (v > a && v < b) v]
);

function open_range(a, b, base=undef, stepn=1, stepd=1) = (
    let(
        base = is_undef(base) ? a : base,
    )
    b < a ?
    reverse(_open_range(b, a, base, stepn, stepd)) :
    _open_range(a, b, base, stepn, stepd)
);

// Returns a list containing: a, open_range(a, b, ...), b
function closed_range(a, b, base=undef, stepn=1, stepd=1) = [
    a, each open_range(a, b, base, stepn, stepd), each (b == a) ? [] : [b]
];

// Returns a list containing: a, open_range(a, b, ...)
function half_open_range_a(a, b, base=undef, stepn=1, stepd=1) = [
    a,
    each open_range(a, b, base, stepn, stepd)
];

// Returns a list containing: open_range(a, b, ...), b
function half_open_range_b(a, b, base=undef, stepn=1, stepd=1) = [
    each open_range(a, b, base, stepn, stepd),
    b,
];

function polyangles(n, ofs=0) = vadd(ofs, lerpn(0, 360, n, ob=true));

// ================================================================================
// Vector functions
// ================================================================================

// vec2(pt): Convert a 3D point [x, y, z] to a 2D point at [x, y].
function vec2(pt) = [pt.x, pt.y];

// vec3(pt, z): Convert a 2D point [x, y] to a 3D point at the given Z value, defaulting
// to z = 0.
function vec3(pt, z=0) = [pt.x, pt.y, z];

// Square of the length of a vector. '*' operator on vectors performs dot product.
function veclen2(vec) = vec * vec;

function normalize(vec) = vec / norm(vec);

// Hypot: Returns the length of the hypotenuse of a right triangle with sides [x, y]
function hypot(x, y) = sqrt(x*x + y*y);

// Euclidean distance between two points.
function dist(a, b) = norm(b - a);

function normto(a, b) = normalize(b - a);

// Rotate 2D point by `ang`
function rotpt(pt, ang, ofs=[0, 0]) = (
    let(c = cos(ang), s = sin(ang), xpt = pt - ofs)
    ofs + [xpt.x * c - xpt.y * s, xpt.x * s + xpt.y * c]
);

function rotpts(pts, ang, ofs=[0, 0]) = (
    let(c = cos(ang), s = sin(ang))
    [for (pt = pts) let(xpt = pt - ofs) ofs + [xpt.x * c - xpt.y * s, xpt.x * s + xpt.y * c]]
);

// Apply a function to a single point or a list of points
function apply_vec(pts, f) = is_num(pts[0]) ? f(pts) : [for (pt = pts) f(pt)];

// Negate x, y, or z in a point or list of points.
function negx(pts) = apply_vec(pts, function(pt) [-pt.x, pt.y, if (!is_undef(pt.z)) pt.z]);
function negy(pts) = apply_vec(pts, function(pt) [pt.x, -pt.y, if (!is_undef(pt.z)) pt.z]);
function negz(pts) = apply_vec(pts, function(pt) [pt.x, pt.y, -pt.z]);

// Reflects a polygon around the X axis.
function rfx(pts) = uniq([
    each pts,
    each reverse(negx(pts)),
]);

// Reflects a polygon around the Y axis.
function rfy(pts) = uniq([
    each pts,
    each reverse(negy(pts)),
]);

// Points of an arc from `start` to `end`
function arcpts(rad, start=0, end=180, npts=30) = [for (i = [0 : npts]) rotpt([rad, 0], lerp(start, end, i/npts))];

function arcpts2(rad, start=0, end=180, base=undef, stepn=undef, stepd=undef) =
let(
    autostep = is_undef(stepn) || is_undef(stepd),
) [
    for (ang = closed_range(start, end, base, autostep ? 360 : stepn, autostep ? $fn : stepd))
    [rad * cos(ang), rad * sin(ang)]
];

function circlepts(rad, base=undef, stepn=undef, stepd=undef) =
let(
    autostep = is_undef(stepn) || is_undef(stepd),
) [
    for (ang = _raw_range(0, 360, is_undef(base) ? 0 : base, autostep ? 360 : stepn, autostep ? $fn : stepd))
    if (ang < 360)
    [rad * cos(ang), rad * sin(ang)]
];

// Given two lists a and b of the same size, multiplies their elements individually.
// listmul([a, b, c], [x, y, z]) -> [a * x, b * y, c * z]
function listmul(a, b) = [for (i = indices(a)) a[i] * b[i]];

// Add v to every value in `lst`
function vadd(v, lst) = [for (x = lst) v + x];

// Given two 2D points, `a` and `b`, return the point along the line connecting them whose
// x coordinate is `x`.
function intercept_y(a, b, x) = let(
    m = (b.y - a.y) / (b.x - a.x),
) [x, m * (x - a.x) + a.y];
function intercept_yv(p, v, x) = intercept_y(p, p + v, x);

function polarpt(r, t) = [r * cos(t), r * sin(t)];


// Given two 2D points, `a` and `b`, return the point along the line connecting them whose
// y coordinate is `y`.
function intercept_x(a, b, y) = let(
    m = (b.x - a.x) / (b.y - a.y),
) [m * (y - a.y) + a.x, y];

function intercept_xv(p, v, y) = intercept_x(p, p + v, y);

function intercept_cx(r, x, origin=[0, 0], sgn=1) = [x, origin.y + sgn * sqrt(r^2 - (x - origin.x)^2)];

function intercept_cy(r, y, origin=[0, 0], sgn=1) = [origin.x + sgn * sqrt(r^2 - (y - origin.y)^2), y];

// ================================================================================
// Lerp
// ================================================================================

function lerp(a, b, t) = (b - a) * t + a;
function lerpc(a, b, t) = (b - a) * max(0, min(1, t)) + a;
function lerp2(sv, ev, st, et, t) = lerp(sv, ev, (t - st) / (et - st));
function lerp2c(sv, ev, st, et, t) = t <= st ? sv : t >= et ? ev : sv + (ev - sv) * ((t - st) / (et - st));

function lerpn(a, b, n, oa=false, ob=false) = let(
    dt = b - a,
) [
    if (!oa) a,
    if (n > 1) for (i = [1 : n - 1]) a + (i / n) * dt,
    if (!ob) b
];

function coalesce(a, b, c, d, e) = (
    !is_undef(a) ? a :
    !is_undef(b) ? b :
    !is_undef(c) ? c :
    !is_undef(d) ? d :
    e
);

// ================================================================================
// Transformation matrix functions
// ================================================================================

// Convenience function for identity matrix
function ident_mat() = [
    [1,  0,  0,  0],
    [0,  1,  0,  0],
    [0,  0,  1,  0],
    [0,  0,  0,  1]
];

function _tmat(
    xx=1, yx=0, zx=0, tx=0,
    xy=0, yy=1, zy=0, ty=0,
    xz=0, yz=0, zz=1, tz=0
) = [
    [xx, yx, zx, tx],
    [xy, yy, zy, ty],
    [xz, yz, zz, tz],
    [ 0,  0,  0,  1]
];

// Base matrix transforms
function _rotate_mat_x(s, c) = _tmat(yy=c, zz=c, zy=-s, yz=s);

function _rotate_mat_y(s, c) = _tmat(xx=c, zz=c, zx=s, xz=-s);

function _rotate_mat_z(s, c) = _tmat(xx=c, yy=c, yx=-s, xy=s);

// tmat(point):
// tmat(x, y, z):
// tmat(x, y, z, xx, xy, xz, yx, yy, yz, zx, zy, zz):
//
// Create a transformation matrix.
//
// multmatrix(tmat(point)) { module(); }
// is equivalent to:
// translate(point) { module(); }
//
// tmat(1, 2, 3) == tmat([1, 2, 3])
// tmat(x=1) == tmat([1, 0, 0])
// tmat(y=2, z=3) == tmat([0, 2, 3])

function tmat(
    x=0, y=0, z=0,
    xx=1, xy=0, xz=0,
    yx=0, yy=1, yz=0,
    zx=0, zy=0, zz=1
) = (
    is_list(x)
    ? _tmat(xx, xy, xz, x.x, yx, yy, yz, x.y, zx, zy, zz, x.z)
    : _tmat(xx, xy, xz, x, yx, yy, yz, y, zx, zy, zz, z)
);

// sclmat(v):
// sclmat([x, y, z]):
// sclmat(x, y, z):
//
// Create a scale matrix.
//
// sclmat(2, 3, 4) == sclmat([2, 3, 4])
// sclmat(5) == sclmat([5, 5, 5])
// sclmat(x=1.5) == sclmat([1.5, 1, 1])
// sclmat(y=1.5, z=2.5) == sclmat([1, 1.5, 2.5])

function sclmat(v=undef, x=undef, y=undef, z=undef) = (
    is_list(v) ? _tmat(xx=v.x, yy=v.y, zz=v.z) :
    is_num(v) ? _tmat(xx=v, yy=v, zz=v) :
    _tmat(xx=is_undef(x) ? 1 : x, yy=is_undef(y) ? 1 : y, zz=is_undef(z) ? 1 : z)
);

// r[xyz]mat(a):
// r[xyz]mat(a, ofs):
//
// Create a rotation matrix around the X, Y, or Z axis.
//
// multmatrix(rxmat(ang)) { module(); }
// is equivalent to:
// rotate([ang, 0, 0]) { module(); }
//
// multmatrix(rxmat(ang, ofs)) { module(); }
// is equivalent to:
// translate(ofs) rotate([ang, 0, 0]) translate(-ofs) { module(); }

function _rmat(a, vec) = (
    let(
        vn = normalize(vec),
        c = cos(a),
        s = sin(a),
        ci = 1 - c,
    )
    _tmat(
        xx = c + ci * vn.x * vn.x,
        yy = c + ci * vn.y * vn.y,
        zz = c + ci * vn.z * vn.z,

        yx = ci * vn.y * vn.x - s * vn.z,
        zx = ci * vn.z * vn.x + s * vn.y,

        xy = ci * vn.x * vn.y + s * vn.z,
        zy = ci * vn.z * vn.y - s * vn.x,

        xz = ci * vn.x * vn.z - s * vn.y,
        yz = ci * vn.y * vn.z + s * vn.x,
    )
);

function rmat(a, vec, ofs=undef) = is_undef(ofs) ? _rmat(a, vec) : tmat(ofs) * _rmat(a, vec) * tmat(-ofs);
function rxmat(a, ofs=undef) = is_undef(ofs) ? _rotate_mat_x(sin(a), cos(a)) : tmat(ofs) * _rotate_mat_x(sin(a), cos(a)) * tmat(-ofs);
function rymat(a, ofs=undef) = is_undef(ofs) ? _rotate_mat_y(sin(a), cos(a)) : tmat(ofs) * _rotate_mat_y(sin(a), cos(a)) * tmat(-ofs);
function rzmat(a, ofs=undef) = is_undef(ofs) ? _rotate_mat_z(sin(a), cos(a)) : tmat(ofs) * _rotate_mat_z(sin(a), cos(a)) * tmat(-ofs);

// based on https://stackoverflow.com/questions/2624422/efficient-4x4-matrix-inverse-affine-transform

// invert_mat(mat):
//
// Inverts a 4x4 transformation matrix.
//
// For any given matrix `mat`, invert_mat(mat) * mat should be very close to `ident_mat()`,
// but might not be exact due to rounding error.
function invert_mat(mat) = (
    let(
        s0 = mat[0][0] * mat[1][1] - mat[1][0] * mat[0][1],
        s1 = mat[0][0] * mat[1][2] - mat[1][0] * mat[0][2],
        s2 = mat[0][0] * mat[1][3] - mat[1][0] * mat[0][3],
        s3 = mat[0][1] * mat[1][2] - mat[1][1] * mat[0][2],
        s4 = mat[0][1] * mat[1][3] - mat[1][1] * mat[0][3],
        s5 = mat[0][2] * mat[1][3] - mat[1][2] * mat[0][3],

        c5 = mat[2][2] * mat[3][3] - mat[3][2] * mat[2][3],
        c4 = mat[2][1] * mat[3][3] - mat[3][1] * mat[2][3],
        c3 = mat[2][1] * mat[3][2] - mat[3][1] * mat[2][2],
        c2 = mat[2][0] * mat[3][3] - mat[3][0] * mat[2][3],
        c1 = mat[2][0] * mat[3][2] - mat[3][0] * mat[2][2],
        c0 = mat[2][0] * mat[3][1] - mat[3][0] * mat[2][1],

        invdet = 1.0 / (s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0)
    )
    [
        [
            ( mat[1][1] * c5 - mat[1][2] * c4 + mat[1][3] * c3) * invdet,
            (-mat[0][1] * c5 + mat[0][2] * c4 - mat[0][3] * c3) * invdet,
            ( mat[3][1] * s5 - mat[3][2] * s4 + mat[3][3] * s3) * invdet,
            (-mat[2][1] * s5 + mat[2][2] * s4 - mat[2][3] * s3) * invdet,
        ],
        [
            (-mat[1][0] * c5 + mat[1][2] * c2 - mat[1][3] * c1) * invdet,
            ( mat[0][0] * c5 - mat[0][2] * c2 + mat[0][3] * c1) * invdet,
            (-mat[3][0] * s5 + mat[3][2] * s2 - mat[3][3] * s1) * invdet,
            ( mat[2][0] * s5 - mat[2][2] * s2 + mat[2][3] * s1) * invdet,
        ],
        [

            ( mat[1][0] * c4 - mat[1][1] * c2 + mat[1][3] * c0) * invdet,
            (-mat[0][0] * c4 + mat[0][1] * c2 - mat[0][3] * c0) * invdet,
            ( mat[3][0] * s4 - mat[3][1] * s2 + mat[3][3] * s0) * invdet,
            (-mat[2][0] * s4 + mat[2][1] * s2 - mat[2][3] * s0) * invdet,
        ],
        [
            (-mat[1][0] * c3 + mat[1][1] * c1 - mat[1][2] * c0) * invdet,
            ( mat[0][0] * c3 - mat[0][1] * c1 + mat[0][2] * c0) * invdet,
            (-mat[3][0] * s3 + mat[3][1] * s1 - mat[3][2] * s0) * invdet,
            ( mat[2][0] * s3 - mat[2][1] * s1 + mat[2][2] * s0) * invdet,
        ]
    ]
);


// Helper to convert vec4 back to vec3
function _v4_to_v3(vec) = [vec.x, vec.y, vec.z];
function _v4_to_v2(vec) = [vec.x, vec.y];

// xform_pt(mat, pt): Transforms absolute `pt` by the transformation matrix `mat`
function xform_pt(mat, pt) = _v4_to_v3(mat * [pt.x, pt.y, pt.z, 1]);

function xform_pt_2d(mat, pt) = _v4_to_v2(mat * [pt.x, pt.y, 0, 1]);

// xform_pts(mat, pts): Transforms each point in `pts` by `mat`
function xform_pts(mat, pts) = [for (pt = pts) xform_pt(mat, pt)];

function xform_pts_2d(mat, pts) = [for (pt = pts) xform_pt_2d(mat, pt)];

// xform_vec(mat, vec): Transforms relative vecetor `vec` by the transformation matrix `mat`
function xform_vec(mat, vec) = _v4_to_v3(mat * [vec.x, vec.y, vec.z, 0]);

function xform_vec_2d(mat, vec) = _v4_to_v2(mat * [vec.x, vec.y, 0, 0]);

// xform_pts(mat, vecs): Transforms each vector in `vecs` by `mat`
function xform_vecs(mat, vecs) = [for (vec = vecs) xform_vec(mat, vec)];

function xform_vecs_2d(mat, vecs) = [for (vec = vecs) xform_vec_2d(mat, vec)];

// xform_mats(mat, mats): Transforms each matrix in `mats` by `mat`
function xform_mats(mat, mats) = [for (m = mats) mat * m];

// 3D -> 2D point projection

function projectxy(points) =  [for (pt = points) [pt.x, pt.y]];
function projectxz(points) =  [for (pt = points) [pt.x, pt.z]];
function projectyz(points) =  [for (pt = points) [pt.y, pt.z]];

// 2D -> 3D point projection
function xypoints(points, z=0) = [for (pt = points) [pt.x, pt.y, z]];
function xzpoints(points, y=0) = [for (pt = points) [pt.x, y, pt.y]];
function yzpoints(points, x=0) = [for (pt = points) [x, pt.x, pt.y]];

// ================================================================================
// Projection matrix functions
// ================================================================================

function tetra_mat_affine(p0, p1, p2, p3) = let(
    rp1 = p1 - p0,
    rp2 = p2 - p0,
    rp3 = p3 - p0,
) tmat(p0) * tmat(
    xx=rp2.x, yx=rp2.y, zx=rp2.z,
    xy=rp1.x, yy=rp1.y, zy=rp1.z,
    xz=rp3.x, yz=rp3.y, zz=rp3.z
);

function _calc_screen_depth(p0, p1, p2, p3) = let(
    smat = tetra_mat_affine(p0, p1, p3, p0 + [0, 0, 1]),
    ismat = invert_mat(smat),

    xfsp = xform_pt(ismat, p2),
    ix = 1 / xfsp.x,
    iy = 1 / xfsp.y,
)
    [ix, iy] / (1 - (ix - 1) * (iy - 1));

// Given two adjacent faces of a rectangular prism defined as
//
//      B-----------C
//      |           |
//      |           |
//      |           |
//      |           |
//      |           |
//      A-----------D
//     /           /
//    F-----------E
//
// Where:
//   screenpts = screen projections of: [A, B, C, D, E, F]
//   wpts = world points for [A, B, D, F]
//
// Calculates a transformation matrix which will line up the faces with their screen
// projection assuming a camera at the origin, looking in the -Z direction with X to the
// right and Y up, and a 90-degree FOV.
//
// This can be achieved by setting:
// $vpf = 90;
// $vpd = 50;
// $vpr = [0, 0, 0];
// $vpt = [0, 0, 0];
//
// Then doing:
// tz($vpd) multmatrix(unproject_rectangle(...)) model();

function unproject_cube(screenpts, wpts) = let(
    spts = xypoints(screenpts, -1),

    pA = wpts[0],
    pB = wpts[1],
    pD = wpts[2],
    pE = wpts[3],

    sA = spts[0],
    sB = spts[1],
    sC = spts[2],
    sD = spts[3],

    sE = spts[4],
    sF = spts[5],

    wmat = tetra_mat_affine(pA, pB, pD, pE),
    iwmat = invert_mat(wmat),

    depth1 = _calc_screen_depth(sA, sB, sC, sD),
    depth2 = _calc_screen_depth(sA, sD, sE, sF),
    rB = depth1[0],
    rD = depth1[1],
    rE = depth2[1],
)
tetra_mat_affine(sA, sB * rB, sD * rD, sF * rE) * iwmat;

// Given a rectangle defined as
//
// D --------- C
// |           |
// A --------- B
//
// Where:
//  wpts = [A, B, C, D] in 3D world coordinates, and
//  screenpts = [A, B, C, D] projected into 2D screen coordinates in the range [-.5, -.5] to [.5, .5]
//
// It must be the case that D - A = C - B and C - D = B - A
//
// Calculates a transformation matrix which will line up the rectangle with its screen
// projection assuming a camera at the origin, looking in the +Z direction with X to the
// right and Y up, and a 90-degree FOV.
//
// This can be achieved by setting:
// $vpf = 90;
// $vpd = 50;
// $vpr = [0, 0, 0];
// $vpt = [0, 0, 0];
//
// Then doing:
// tz($vpd) sz(-1) multmatrix(unproject_rectangle(...)) model();
//
// zs is a scaling factor for the direction normal to the rectangle.

function unproject_rectangle(wpts, screenpts, zs=1) = let(
    spts = xypoints(screenpts, 1),
    wmat = tri_mat_affine(wpts[0], wpts[1], wpts[3], true),
    smat = tri_mat_affine(spts[0], spts[1], spts[3]),
    iwmat = invert_mat(wmat),
    ismat = invert_mat(smat),

    xfsp = xform_pt(ismat, spts[2]),
    ix = 1 / xfsp.x,
    iy = 1 / xfsp.y,

    scl = 1 / (1 - (ix - 1) * (iy - 1)),
    r1 = ix * scl,
    r2 = iy * scl,
)
tri_mat_affine(spts[0], spts[1] * r1, spts[3] * r2) * sclmat(z=zs) * iwmat;

// ================================================================================
// Path functions
// ================================================================================

// cross_sum(points): Given a polygon, returns the sum of the cross product of each angle.
// This can be used to determine the winding direction for 2D polygons: if positive,
// the winding is counter-clockwise, and if negative, the winding is clockwise.
function cross_sum(points) = (
    let(npts = len(points))
    sum([
        for (i = [0 : npts - 1])
        let(
            p1 = points[i],
            p2 = points[(i + 1) % npts],
            p3 = points[(i + 2) % npts],
        )
        cross(p3 - p2, p1 - p2)
    ])
);

// poly_cw(points): Makes sure the given polygon has clockwise winding.
// If the input polygon has counter-clockwise winding, reverses it.
function poly_cw(points) = cross_sum(points) > 0 ? reverse(points) : points;

// poly_ccw(points): Makes sure the given polygon has counter-clockwise winding.
function poly_ccw(points) = cross_sum(points) < 0 ? reverse(points) : points;

/*
| join_paths(paths)
| join_paths(a, b)
|
| A path is defined as a list of transformation matrices defining how to curve a set of
| points through space, combined with a distance along the path. The distance is not used
| by path_extrude() but can be used to post-process a path
|
| join_paths(a, b) will join two paths together, transforming each matrix in `b` by the
| last matrix in `a`, and adding the last distance in a to every distance in `b`.
|
| join_paths(paths) takes a list of paths and joins them all together.
| join_paths([p1, p2, p3]) is equivalent to join_paths(join_paths(p1, p2), p3), and
| join_paths([p1, p2, p3, p4]) is equivalent to join_paths(join_paths(join_paths(p1, p2), p3), p4)
*/


function _join_paths(a, b) = (
    let(
        last_a = at(a, -1),
        last_a_mat = last_a[0],
        last_a_dist = last_a[1]

    )
    is_string(b) ? [each sublst(a, 0, -1), [last_a_mat, last_a_dist, b]] :
    [
        each a,
        for (v = b) [last_a_mat * v[0], last_a_dist + v[1], if (!is_undef(v[2])) v[2]]
    ]
);

// P_0 = jp(a, b)
// P_1 = jp(P_0, c)
// P_2 = jp(P_1, d)

// jpl([a, b, c, d])
// -> jpl([jp(a, b), c, d])
// -> jpl([P_0, c, d, e])
// -> jpl([jp(P_0, c), d])
// -> jpl([P_1, d])
// -> jpl([jp(P_1, d)])
// -> jpl([P_2])
// -> P_2

function _join_path_list(paths) = (
    len(paths) > 1
    ? _join_path_list([_join_paths(paths[0], paths[1]), each sublst(paths, 2)])
    : paths[0]
);

function join_paths(a=undef, b=undef, paths=undef) = (
    // Case 1: caller specified `paths` by name (i.e. join_paths(paths=[p1, p2, p3, ...]))
    !is_undef(paths) ? _join_path_list(paths) :
    // Case 2: caller named `a` and `b` or two unnamed arguments (join_paths(p1, p2))
    !is_undef(b) ? _join_paths(a, b) :
    // Case 3: caller supplied one unnamed argument (join_paths([p1, p2, p3, ...]))
    _join_path_list(a)
);

function create_path(paths, start_xform=ident_mat()) = _join_path_list([path_start(start_xform), each paths]);

function path_length(path) = at(path, -1)[1];

function path_marks(path) = [
    for (i = indices(path))
    let(xform = path[i], mat = xform[0], dist = xform[1], mark = xform[2])
    if (!is_undef(mark)) [mark, [mat, dist, i]]
];

// Start a path off with the identity matrix
function path_start(start_xform=ident_mat()) = [[start_xform, 0]];

// Move a path straight forward
function path_straight(v, slices=1) = (
    slices == 1 ?
    [[tmat(y=v), v]] :
    [for (i = [1 : slices]) let(nv = v * i / slices) [tmat(y=nv), nv] ]
);

// Bend a path to the left or right with the given radius of curvature. Positive angles
// curve to the right and negative angles curve to the left.
function path_bend(ang, rad, stepn=2, stepd=1, xang = 0) = (
    let(
        sgn = ang > 0 ? -1 : 1,
        r_ang = abs(ang),
        r_rad = ang > 0 ? rad : -rad,
        rotvec = [-sin(xang), 0, cos(xang)],
        pivot = r_rad * [cos(xang), 0, sin(xang)]
    )
    [
        for (angv = half_open_range_b(0, r_ang, stepn=stepn, stepd=stepd))
        [
            /* mat  */ rmat(sgn * angv, rotvec, pivot),
            /* dist */ rad * angv * PI / 180
        ]
    ]
);

// ================================================================================
// Misc utility functions
// ================================================================================

// {              } -> not enough params
// {p1            } -> [p1, 0]
// {    p2        } -> [p2, 0]
// {p1, p2        } -> [p1, p2]
// {        pc    } -> not enough params
// {p1,     pc    } -> conflicting params
// {    p2, pc    } -> conflicting params
// {p1, p2, pc    } -> conflicting params
// {            ps} -> [-ps/2, ps/2]
// {p1,         ps} -> [p1, p1 + ps]
// {    p2,     ps} -> conflicting params
// {p1, p2,     ps} -> conflicting params
// {        pc, ps} -> [pc - ps/2, pc + ps/2]
// {p1,     pc, ps} -> conflicting params
// {    p2, pc, ps} -> conflicting params
// {p1, p2, pc, ps} -> conflicting params

function sortpt(a) = a[0] < a[1] ? a : [a[1], a[0]];
function span(p1, p2, pc, ps, msg="v") = sortpt(
    ((is_num(p2) && (is_num(ps) || is_num(pc))) || (is_num(pc) && is_num(p1))) ? assert(false, str("span: conflicting params given for ", msg)) :
    is_num(p2) ? [is_num(p1) ? p1 : 0, p2] :
    is_num(p1) ? [p1, is_num(ps) ? p1 + ps : 0] :
    is_num(ps) ? let(hs = ps/2, c = is_undef(pc) ? 0 : pc) [c - hs, c + hs] :
    assert(false, str("span: not enough params given for ", msg))
);

function _bsearch(values, t, a, b) =  (
    a >= b ? (a > 0 && t < values[a][0] ? a - 1 : a) :
    let(
        mp = floor((a + b) / 2),
        mpv = values[mp][0]
    )
    mpv == t ? mp :
    mpv > t ? _bsearch(values, t, a, mp - 1) :
    _bsearch(values, t, mp + 1, b)
);

// Version of lookup() which returns index instead of value
function lookup_index(t, values) = _bsearch(values, t, 0, len(values) - 1);

// Fixed version of lookup() which works with vectors
function vector_lookup_raw(t, values) = (
    let(
        length = len(values),
        idx = lookup_index(t, values),
        idx2 = min(idx + 1, length - 1),
        v1 = values[idx],
        v2 = values[idx2],

        v = lerp2c(v1[1], v2[1], v1[0], v2[0], t),

        sv = v1[1],
        ev = v2[1],
        st = v1[0],
        et = v2[0],

        t_out = t <= st ? 0 : t >= et ? 1 : ((t - st) / (et - st)),
    )
    [sv, ev, t_out, idx, idx2]
);

function vector_lookup(t, values) = (
    let(
        data = vector_lookup_raw(t, values),
    )
    lerp(data[0], data[1], data[2])
);

// Given two circles, one at [0, 0] with radius r1, and one at [d, 0] with radius r2,
// returns the intersection point [x, y] with positive y.
function tript(r1, r2, d) = let(x = (r1*r1 - r2*r2 + d*d) / (2*d)) [x, sqrt(r1*r1 - x*x)];

// Given a chord of a circle defined by the length of the chord (length), and the distance
// to the circle's edge (width), returns the distance of the chord to the circle's center.
// The radius can then be calculated by adding `width` to the returned value.
//
// +
// |\
// | \
// |  \
// |   \
// |    \
// |     \
// |     |
// |-----|
// |     |
// |     /
// |    /
// |   /
// |  /
// | /
// |/
// +

function chord_dist(width, length) = let(c = length / 2) (c*c / width - width) / 2;

// Given two circles, one at the origin with radius r1, and one at [d, 0] with radius r2,
// calculates and returns a list with the following data:
// tan_len: Length of the tangent line
// tan_ang: Angle of the tangent line
// tan_pt1, tan_pt2: Tangent points on each circle
// belt_len: Length of a belt that goes around both circles
function circle_tangent_info(r1, r2, d) = let(
    tan_len = sqrt(d^2 - (r2 - r1)^2),
    tan_vec = normalize([r1 - r2, tan_len]),
    tan_ang = atan2(r2 - r1, tan_len),

    tan_pt1 = tan_vec * r1,
    tan_pt2 = [d, 0] + tan_vec * r2,

    r1_l = abs(r1) * (90 + (r1 < 0 ? tan_ang : -tan_ang)) * PI / 180,
    r2_l = abs(r2) * (90 + (r2 < 0 ? -tan_ang : tan_ang)) * PI / 180,

    belt_len = 2 * (r1_l + r2_l + tan_len),
) [tan_len, tan_ang, tan_pt1, tan_pt2, belt_len];

// Define a screw to be used with screw_taper
function screw_def(pitch, dia, slope=1.0) = [pitch, dia + (slope * pitch), dia];

// Given a screw definition, offset the diameter by a fixed amount
function screw_offset(def, ofs) = [def[0], def[1] + ofs, def[2] + ofs];

// ================================================================================
// Bezier functions
// ================================================================================
function _pt2(cp) = is_undef(cp[2]) ? cp[1] : cp[2];
function bezierq(a, b, c, t) =
lerp(
    lerp(a, b, t),
    lerp(b, c, t),
    t
);

function bezierc(a, b, c, d, t) =
lerp(
    bezierq(a, b, c, t),
    bezierq(b, c, d, t),
    t
);

function bezierc_v(v1, v2, t) = bezierc(v1[0], v1[0] + _pt2(v1), v2[0] - v2[1], v2[0], t);

function bezier_lookup(cpts, t) = (
    let(
        data = vector_lookup_raw(t, cpts),
        v1 = data[0],
        v2 = data[1],
        t = data[2]
    ) bezierc_v(v1, v2, t)
);

function bezierpts(cpts, npts) = [
    for (i = [0 : 1 : len(cpts) - 2]) each [
        for (t = [0: 1 / npts : 1])
        bezierc_v(cpts[i], cpts[i + 1], t)
    ]
];

// ================================================================================
// Animation functions
// ================================================================================

_animate_funcs = [
    // 0: Constant
    // [st, 0, v]
    function(t, seg) seg[2],

    // 1: Open Lerp with t0 = st
    // [st, 1, t1, v0, v1]
    function(t, seg) lerp2(seg[3], seg[4], seg[0], seg[2], t),

    // 2: Open Lerp with variant t0
    // [st, 2, t0, t1, v0, v1]
    function(t, seg) lerp2(seg[4], seg[5], seg[2], seg[3], t),

    // 3: Clamped Lerp with t0 = st
    // [st, 3, t1, v0, v1]
    function(t, seg) lerp2c(seg[3], seg[4], seg[0], seg[2], t),

    // 4: Clamped Lerp with variant t0
    // [st, 4, t0, t1, v0, v1]
    function(t, seg) lerp2c(seg[4], seg[5], seg[2], seg[3], t),

    // 5: Open quadratic bezier
    // [st, 5, t0, t1, v0, cp, v1]
    function(t, seg) bezierq(seg[4], seg[5], seg[6], lerp2(0, 1, seg[2], seg[3], t)),

    // 5: Clamped quadratic bezier
    // [st, 6, t0, t1, v0, cp, v1]
    function(t, seg) bezierq(seg[4], seg[5], seg[6], lerp2c(0, 1, seg[2], seg[3], t)),

    // 7: Open cubic bezier
    // [st, 7, t0, t1, v0, cp0, cp1, v1]
    function(t, seg) bezierc(seg[4], seg[5], seg[6], seg[7], lerp2(0, 1, seg[2], seg[3], t)),

    // 8: Clamped cubic bezier
    // [st, 7, t0, t1, v0, cp0, cp1, v1]
    function(t, seg) bezierc(seg[4], seg[5], seg[6], seg[7], lerp2c(0, 1, seg[2], seg[3], t)),
];

function animate_seg(t, seg) = _animate_funcs[seg[1]](t, seg);

function animate_var(t, segs) = animate_seg(t, segs[lookup_index(t, segs)]);

// ================================================================================
// Multi-color printing support
// ================================================================================

module part_color(idx, color=undef, flag=undef, alpha=undef, pxf=undef, pxfm=undef) {
    part_selected = (
        is_num(idx) ? (is_undef($part_color) || $part_color == idx)
        : (is_undef($part_color_name) || $part_color_name == idx)
    );

    //echo(idx=idx, color=is_undef($part_color) ? "undefined" : $part_color);
    if ($preview) {
        if (is_undef(flag) || flag) {
            color(color, alpha) {
                if (!is_undef(pxfm)) {
                    for ($idx = [0 : $children - 1]) {
                        let(mat=pxfm[$idx])
                        multmatrix(!is_undef(mat) ? mat : !is_undef(pxf) ? pxf : ident_mat())
                        children($idx);
                    }
                } else if (!is_undef(pxf)) {
                    multmatrix(pxf)
                    children();
                } else {
                    children();
                }
            }
        }
    } else {
        if (part_selected) {
            color(color, alpha)
            children();
        }
    }
}


// Dummy function to exclude variable from customizer
function np(x) = x;

module tx(x) {
    if (is_list(x)) {
        for (xx = x) translate([xx, 0, 0]) children();
    } else {
        translate([x, 0, 0]) children();
    }
}

module ty(y) {
    if (is_list(y)) {
        for (yy = y) translate([0, yy, 0]) children();
    } else {
        translate([0, y, 0]) children();
    }
}

module tz(z) {
    if (is_list(z)) {
        for (zz = z) translate([0, 0, zz]) children();
    } else {
        translate([0, 0, z]) children();
    }
}

module tv(pts) {
    for (pt = pts) translate(pt) children();
}

module rx(x, ofs=undef) {
    if (is_list(x)) {
        for (xx = x) multmatrix(rxmat(xx, ofs)) children();
    } else {
        multmatrix(rxmat(x, ofs)) children();
    }
}

module ry(y, ofs=undef) {
    if (is_list(y)) {
        for (yy = y) multmatrix(rymat(yy, ofs)) children();
    } else {
        multmatrix(rymat(y, ofs)) children();
    }
}

module rz(z, ofs=undef) {
    if (is_list(z)) {
        for (zz = z) multmatrix(rzmat(zz, ofs)) children();
    } else {
        multmatrix(rzmat(z, ofs)) children();
    }
}

module sx(x) {
    scale([x, 1, 1]) children();
}

module sy(y) {
    scale([1, y, 1]) children();
}

module sz(z) {
    scale([1, 1, z]) children();
}

module corners(width, length) {
    w2 = width/2;
    l2 = length/2;

    children();
    translate([w2, l2, 0]) {
        mirror([1, 0, 0])
        translate([-w2, -l2, 0]) children();

        mirror([0, 1, 0])
        translate([-w2, -l2, 0]) children();

        mirror([1, 0, 0])
        mirror([0, 1, 0])
        translate([-w2, -l2, 0]) children();
    }

}

module corners2(x1, y1, x2, y2) {

    translate([x1, y1])
    mirror([1, 0, 0])
    mirror([0, 1, 0])
    children();

    translate([x1, y2])
    mirror([1, 0, 0])
    children();

    translate([x2, y1])
    mirror([0, 1, 0])
    children();

    translate([x2, y2])
    children();
}

module turn_screw(screwset, turns, adjust=0) {
    // allow passing a screwset, screw def, or pitch directly
    pitch = is_num(screwset) ? screwset : is_num(screwset[0]) ? screwset[0] : screwset[0][0];
    tz((turns + adjust) * pitch)
    rz(turns * 360)
    children();
}

// Modules such as cylinder and linear_extrude treat the Z axis differently, requiring
// rotation and mirroring. This transformation treats the polygon points as [x, z],
// extruding into the y axis.
module xzy() {
    multmatrix([
        [1, 0, 0, 0],
        [0, 0, 1, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 1]
    ])
    children();
}

// This module is like xzy(), but with polygon points as [y, z] extruding into the x axis.
module yzx() {
    multmatrix([
        [0, 0, 1, 0],
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 1]
    ])
    children();
}

// Colors parts of the first child based on the rest of the children
module color_part(colors) {
    color(colors[0])
    render(10)
    difference() {
        children(0);
        children([1:$children-1]);
    }
    for (i = [1 : len(colors) - 1]) {
        color(colors[i])
        render(10)
        intersection() {
            children(0);
            children(i);
        }
    }
}

//module slice(axis="x", max=500, x1=undef, x2=undef, xc=undef, xs=undef, enable=true) {
module slice(axis="x", v1=undef, v2=undef, vc=undef, vs=undef, enable=true, max=500) {
    if (enable) {
        vspan = sortpt(span(v1, v2, vc, vs, str("slice (", axis, ")")));

        xv = [1, 0, 0, 0];
        yv = [0, 1, 0, 0];
        zv = [0, 0, 1, 0];
        wv = [0, 0, 0, 1];
        intersection() {
            children();
            multmatrix(
                axis == "x" ? [zv, xv, yv, wv] :
                axis == "y" ? [xv, zv, yv, wv] :
                axis == "z" ? [xv, yv, zv, wv] :
                [xv, yv, zv, wv]
            )
            translate([-max/2, -max/2, vspan[0]])
            cube([max, max, vspan[1] - vspan[0]]);
        }
    } else {
        children();
    }
}

module thinslice(axis="x", v, enable=true, max=500) {
    if (enable) {


        xv = [1, 0, 0, 0];
        yv = [0, 1, 0, 0];
        zv = [0, 0, 1, 0];
        wv = [0, 0, 0, 1];
        linear_extrude(0.01)
        projection(true)
        tz(-v)
        multmatrix(
            axis == "x" ? [zv, xv, yv, wv] :
            axis == "y" ? [xv, zv, yv, wv] :
            axis == "z" ? [xv, yv, zv, wv] :
            [xv, yv, zv, wv]
        )
        children();

    } else {
        children();
    }
}

module reflect(v) {
    children();
    mirror(v) children();
}

module rfx(ofs=0) {
    if (ofs == 0) {
        rfx2(0, 0) children();
    } else {
        rfx2(ofs, ofs) tx(-ofs) children();
    }
}

module rfx2(x1, x2) {
    translate(is_list(x1) ? x1 : [x1, 0, 0]) children($rfx = 1);
    translate(is_list(x2) ? x1 : [x2, 0, 0]) sx(-1) children($rfx = -1);
}


module rfy(ofs=0) {
    if (ofs == 0) {
        rfy2(0, 0) children();
    } else {
        rfy2(ofs, ofs) ty(-ofs) children();
    }
}

module rfy2(y1, y2) {
    translate(is_list(y1) ? y1 : [0, y1, 0]) children($rfy = 1);
    translate(is_list(y2) ? y1 : [0, y2, 0]) sy(-1) children($rfy = -1);
}

module rfz(ofs=0) {
    if (ofs == 0) {
        rfz2(0, 0) children();
    } else {
        rfz2(ofs, ofs) tz(-ofs) children();
    }
}

module rfz2(z1, z2) {
    translate(is_list(z1) ? z1 : [0, 0, z1]) children($rfz = 1);
    translate(is_list(z2) ? z1 : [0, 0, z2]) sz(-1) children($rfz = -1);
}

module linehull(pts) {
    for (i = [0 : 1 : len(pts) - 2]) {
        pt1 = pts[i];
        pt2 = pts[i + 1];
        hull() {
            translate(pt1) children();
            translate(pt2) children();
        }
    }
}

module point_to(v1, v2) {
    dv = v2 - v1;
    lxy2 = dv.x * dv.x + dv.y * dv.y;

    if (lxy2 < 0.0001) {
        if (dv.z < 0) {
            translate(v2)
            children($h = -dv.z);
        } else {
            translate(v1)
            children($h = dv.z);
        }


    } else {
        translate(v1) {
            length = sqrt(lxy2 + dv.z * dv.z);
            rz = atan2(dv.y, dv.x);
            ry = atan2(sqrt(lxy2), dv.z);
            rz(rz) ry(ry) children($h=length);
        }
    }
}

module cylpt(v1, v2, r=undef, d=undef, r1=undef, r2=undef, d1=undef, d2=undef) {
    point_to(v1, v2) cylinder(h=$h, r=r, d=d, r1=r1, r2=r2, d1=d1, d2=d2);
}

module xcolor(color=undef, opacity=undef) {
    if ($preview_sliced_color || is_undef($preview_slice_axis) || $preview_slice_axis == "off") {
        color(color, opacity) children();
    } else {
        color(color, opacity) render(10) slice($preview_slice_axis, vc=$preview_slice_vc, vs=$preview_slice_vs - eps2, max=$preview_slice_max) children($preview_sliced_color=true);
    }
}

module polyx(points, txtrot=0, scl=.5) {
    if (is_polydbg()) {
        rotx = [for (pt = points) rotpt(pt, -txtrot).x];
        minx = min(rotx);
        maxx = max(rotx);
        midx = (minx + maxx) / 2;

        for (i = indices(points)) let(pt=points[i], rx=rotx[i]) translate(pt) scale(scl) {
            circle(d=1);
            rz(txtrot)
            if (rx < midx) {
                tx(-1) text(str(pt, " ", i), size=1, halign="right", valign="center");
            } else {
                tx(1) text(str(i, " ", pt), size=1, halign="left", valign="center");
            }
        }
        linehull([each points, points[0]]) circle(d=.2 * scl);
    } else {
        polygon(points);
    }

}

// Renders all children, except the first, if 'inner' is true and $preview is set. Designed
// so that you can take a 'difference' and add 'x' to it to debug the interior.
module xdifference(inner=true, color="green", alpha=.5) {
    if ($preview && inner) {
        children([1 : $children - 1]);
        color(color, alpha) render(10) children(0);
    } else {
        difference() {
            children(0);
            children([1 : $children - 1]);
        }
    }
}

/*
slice_axis = "off"; // [off, x, y, z]
slice_pos = 0; // [-50 : 1 : 50]
preview_slice(axis=slice_axis, vc=slice_pos)
*/
module preview_slice(axis="off", v1=undef, v2=undef, vc=undef, vs=1, max=500) {
    $preview_slice_max = max;
    $preview_slice_vc = vc;
    $preview_slice_vs = vs;
    $preview_slice_axis = axis;

    if (!$preview || axis == "off") {
        children();
    } else {
        slice(axis, v1, v2, vc, vs, max=max) children();

    }
}

// compatibility
module xslice(max=500, x1=undef, x2=undef, xc=undef, xs=undef, enable=true) {
    slice("x", x1, x2, xc, xs, enable, max) children();
}

module yslice(max=500, y1=undef, y2=undef, yc=undef, ys=undef, enable=true) {
    slice("y", y1, y2, yc, ys, enable, max) children();
}

module zslice(max=500, z1=undef, z2=undef, zc=undef, zs=undef, enable=true) {
    slice("z", z1, z2, zc, zs, enable, max) children();
}

module slice_at(z, max=10000) {
    slice("z", z1=-max, z2=z, max=max) children();
}

module pvh() {
    if (!$preview) children();
}

function is_polydbg() = $preview && !is_undef($polydbg) && $polydbg;

module lxt(z1 = undef, z2 = undef, zc = undef, zs = undef, convexity = 10, twist = undef, slices = undef, scale = undef, debug=false) {
    zpt = sortpt(span(z1, z2, zc, zs, "lxt (z)"));
    tz(zpt[0])
    linear_extrude(height=zpt[1]-zpt[0], center=false, convexity=convexity, twist=twist, slices=slices, scale=scale)
    children();

    if (debug) {
        %#tz(zpt[0] - 1)
        linear_extrude(height=zpt[1]-zpt[0] + 2, center=false, convexity=convexity, twist=twist, slices=slices, scale=scale)
        children($polydbg = true);

    }
}

module lxtd(z1 = undef, z2 = undef, zc = undef, zs = undef, convexity = 10, twist = undef, slices = undef, scale = undef) {
    lxt(z1, z2, zc, zs, convexity, twist, slices, scale, debug=true) children();
}

module lxpoly(height = undef, center = undef, convexity = undef, twist = undef, slices = undef, scale = undef, points=[], paths=undef) {
    linear_extrude(height=height, center=center, convexity=convexity, twist=twist, slices=slices, scale=scale)
    polygon(points, paths=paths, convexity=convexity);
}
