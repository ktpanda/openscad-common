include <consts.scad>
use <funcs.scad>
use <shapes.scad>
use <partbin.scad>
use <sevenseg.scad>
use <models.scad>

include <screw-defs.scad>

test = "all"; // [all, xform_matrix, path_extrude, path_extrude_twist, path_extrude_closed, path_extrude_twist, screws, polydbg, lathe, sevenseg]

do_test_cut = false;
preview_num = -1; // [-100 : 0.01 : 100]

$fn = 20;

module frames(size) {
    $size = size;
    spacing = size + [10, 10];

    for ($i = [0 : $children - 1]) {
        $point = test == "all" ? [($i % 5) * spacing.x, (floor($i / 5) * -spacing.y)] : [0, 0];
        translate($point) {
            lxt(0, 0.1) {
                difference() {
                    squarex(x1 = -2, x2 = size.x + 2, y1 = -2, y2 = size.y + 2);
                    squarex(x1 = -1, x2 = size.x + 1, y1 = -1, y2 = size.y + 1);
                }
            }

            children($i);
        }
    }
}

//*
module frame(title, centerx=false, centery=false) {
    if (test == "all" || test == title) {
        lxt(0, 0.1) {
            translate([2, $size.y + 3])
            text(title, size=2, valign="bottom");
        }

        translate([centerx ? $size.x/2 : 0, centery ? $size.y/2 : 0])
        children();
    }
}

// Verifies that meshes are closed by forcing a CSG operation
module test_cut(x) {
    if (do_test_cut) {
        render(10)
        difference() {
            children();
            cubex(xc=x, xs=1, yc=0, ys=100, zc=0, zs=100);
        }
    } else {
        children();
    }

}

extrude_poly = let(a1 = 120, a2 = 360 + 60) [each arcpts(5, a1, a2), each reverse(arcpts(4, a1, a2))];
/*
extrude_poly = [
[1, 1],
[-1, 1],
[-1, -1],
[1, -1]
];
*/

function open_path(slices=1) = create_path([
    path_straight(20, slices=slices),

    path_bend(60, 15),

    path_straight(10, slices=slices),

    path_bend(180, 6),

    path_straight(6, slices=slices),

    path_bend(-120, 6),

    path_straight(8, slices=slices),

    path_bend(-90, 6),

    path_straight(4, slices=slices),
]);


function closed_path(slices=1) = create_path([
    path_bend(60, 6),
    path_straight(8, slices=slices),

    path_bend(30, 6),
    path_straight(8, slices=slices),

    path_bend(30, 6),
    path_straight(8, slices=slices),

    path_bend(60, 6),
    path_straight(8, slices=slices),

    path_bend(60, 6),
    path_straight(8, slices=slices),

    path_bend(30, 6),
    path_straight(8, slices=slices),

    path_bend(30, 6),
    path_straight(8, slices=slices),

    path_bend(60, 6),

    path_straight(8, slices=slices),
]);

module axisptr() {
    z1 = 0.6;
    cylx(0, z1, d=.2);
    cylx(z1, 1, d1=.5, d2=0);
}

module axisind() {
    color("#0000ff") axisptr();
    yzx() color("#ff0000") axisptr();
    xzy() color("#00ff00") axisptr();

}

frames([45, 45]) {
    frame("xform_matrix") ty(15) {
        testmat = tmat(x = 4, z=.8) * rzmat($t * 360 + 15) * tmat(x=1);

        scale(6) {
            multmatrix(testmat)
            cube(1);

            let(p=xform_pts(testmat, [for (x = [0, 1], y = [0, 1], z = [0, 1]) [x, y, z]]))
            for (pt = p) translate(pt)
            cube(.1, center=true);
        }
        translate([6, 6]) scale(4) multmatrix(rmat($t * 360, [1, 1, 1])) axisind();
    }

    frame("path_extrude") {
        test_cut(20)
        translate([6, 0])
        path_extrude(extrude_poly, open_path());
    }

    frame("path_extrude_twist") {
        test_cut(20)
        translate([6, 0])
        let(
            npath = [for (v = open_path(8)) [v[0] * rymat(v[1]), v[1]]]
        )
        path_extrude(extrude_poly, npath);
    }

    frame("path_extrude_closed") {
        test_cut(20)
        translate([6, 25])
        path_extrude(extrude_poly, sublst(closed_path(), 0, -1), closed=true, debug=0);
    }

    frame("path_extrude_closed_twist") {
        test_cut(20)
        translate([6, 25])
        let(
            basepath = closed_path(8),
            length = path_length(basepath),
            npath = [for (v = basepath) [v[0] * rymat(v[1] * 360 / length), v[1]]]

        )
        path_extrude(projectxy(xform_pts(sclmat(x=.5, y=-.6), xypoints(extrude_poly))), sublst(npath, 0, -1), closed=true);
    }

    frame("screws", true, true) {
        defn = screw_inner("m8");
        rx(-90) {
            tx(-15) screw_thread(defn, 10, ltaper=-1, lext=1, utaper=-1, uext=1, zbase=$t * defn[0]);
            tx(-5) _screw_thread(screw_inner("m8"), 10);
            tx(5) _screw_thread_double(screw_inner("m8"), 10, 0, 5, 1.0, 1.2);
            tx(18) screw_thread(defn, 10, ltaper=-1, lext=1, utaper=1, uext=1, zbase=$t * defn[0], double=true, cone_s1 = 2);
        }
    }

    frame("polydbg", true, true) {
        lxtd(-1, 1) {
            polyx([
                [0, 0],
                [20, 0],
                [15, -10],
                [-8, -12],
                [-15, 12],
                [9, 6]
            ]);
        }
    }

    frame("lathe", false, false)
    translate([2, 2]) {
        rx(-90)
        lathe(r=[
            [0, 0],
            [4, 2],
            [5, 1],
            [6, .5],
            [7, 0]
        ]);
    }

    frame("sevenseg", false, false)
    color("#f404f4")
    lxt(0, 1)
    tx(10)
    scale(.25) {
        for (p = [1 : 10]) {
            ty(p * 15) {
                sevenseg_num(-(10 ^ p) + 0, sx=5, sy=5, spc=2, dec=1, pos_sign=true);
            }
        }

        tx(-numsize(preview_num, sx=5, pad=2, dec=2))
        sevenseg_num(preview_num, sx=5, sy=4, pad=2, dec=2, r=.4, t=2);
    }

    frame("bedmodel", true, true) scale(1/6) {

        print_bed_model_i3(center=true);
    }
}
/*
*/

function test_invert(mat) = invert_mat(mat) * mat;

echo(inv1=test_invert(tmat(x=2, y=4)));
echo(inv2=test_invert(rzmat(10) * tmat(x=2, y=4)));

echo(sum=sum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]));
echo(cross_sum = cross_sum((extrude_poly)));

lst = [
    [0, 5],
    [4, 8],
    [5, 3],
    [10, 3],
    [100, 4],
    [101, 5],
    [101.5, 6],
    [110, 0]
];

for (t = [-1, 0, 1, 4, 4.5, 6, 50, 99, 100, 100.1, 100.999, 101, 1000])
let(v=lookup_index(t, lst), q=lst[v])
echo(t=t, i=v, q=q);

echo(negx([1, 2]));
echo(negx([[1, 2], [3, 4]]));
echo(lerpn([1, 100], [2, 200], 4));


//*/
