#!/usr/bin/python3
import sys
import re
import time
import os
import argparse
import subprocess
import traceback
from os.path import dirname, basename, join, exists, splitext
from scadutils import classify_scad, SCADFragment

RX_DIRECTIVE = re.compile(r'^@(\w+)\s*(.*)', re.I)
RX_INDENT = re.compile(r'^(\s*)(.*?)(\s*)$', re.S)
BOM = '\ufeff'

ALLOWED_EXTENSIONS = ('.scad')

class Line:
    def __init__(self, indent_space, text, trailing, is_dos=False, balance=0, min_balance=0):
        self.indent_space = indent_space
        self.text = text
        self.trailing = trailing
        self.is_dos = is_dos

        self.indent_level = 0
        self.directive = ''
        self.balance = balance
        self.min_balance = min_balance

    def match_indent(self, line):
        '''If this is a comment line, and the original indentation of this line matches
        `line`, adjust our new indentation to match it.

        Returns True if the line is blank or original indentation matches. Returns False
        if this is not a comment line or indentation does not match.
        '''
        if not self.text:
            return True

        if self.text.startswith('//') and self.indent_space == line.indent_space:
            self.indent_level = line.indent_level
            return True

        return False

def main():
    p = argparse.ArgumentParser(description='Auto-indent a SCAD file')
    p.add_argument('files', nargs='*', help='SCAD files')
    p.add_argument('-s', '--stdin', action='store_true', help='Read from STDIN and write to STDOUT')
    p.add_argument('-f', '--force', action='store_true', help='force output even when extension doesn\'t match')
    p.add_argument('-d', '--dump', action='store_true', help='print to stdout instead of rewriting file')
    p.add_argument('-q', '--quiet', action='store_true', help='Don\'t print output about autoindented lines')
    args = p.parse_args()

    errout = sys.stderr

    if args.stdin:
        data = sys.stdin.buffer.read()
        try:
            if not args.quiet:
                print('Auto-indenting <stdin>', file=errout)
            text = data.decode('utf-8', 'replace')
            text, indentation_changed, trailing_cleared, blank_cleared = autoindent_text(text, errout, args.quiet)
            data = text.encode('utf-8')
            if not args.quiet:
                report_changes(indentation_changed, trailing_cleared, blank_cleared, file=errout)
        finally:
            sys.stdout.buffer.write(data)

    for fn in args.files:
        ext = splitext(fn)[1]
        if not args.force and ext not in ALLOWED_EXTENSIONS:
            print(f'Will not operate on SCAD file {fn} (use --force to do it anyway)', file=errout)
            continue

        if not args.quiet:
            print(f'Auto-indenting {fn}', file=errout)

        try:
            data, indentation_changed, trailing_cleared, blank_cleared = indent_file(fn, args.quiet, args.dump, args.stdin, errout)
            if not args.quiet:
                report_changes(indentation_changed, trailing_cleared, blank_cleared, file=errout)

        except OSError as e:
            print(f'Error auto-indenting {fn}: {e}', file=errout)

def indent_file(fn, quiet=False, dump=False, stdin=False, errout=sys.stderr):
    # Open in binary mode to preserve line endings.
    with open(fn, 'rb') as fp:
        data = fp.read()

    try:
        data = data.decode('utf-8')
    except UnicodeDecodeError as e:
        print(f'WARNING: {fn} is not valid utf-8: {e}', file=errout)
        print('Invalid characters will be replaced with U+FFFD', file=errout)
        data = data.decode('utf-8', 'replace')

    origdata = data

    data, indentation_changed, trailing_cleared, blank_cleared = autoindent_text(data, errout, quiet)

    any_changes = origdata != data

    if dump and not stdin:
        sys.stdout.write(data)
    else:
        if any_changes:
            with open(fn, 'wb') as fp:
                fp.write(data.encode('utf-8'))

    return any_changes, indentation_changed, trailing_cleared, blank_cleared

def autoindent_text(data, errout, quiet):
    # Check for a BOM and remove it before indenting
    has_bom = data.startswith(BOM)
    if has_bom:
        data = data[1:]

    uses_tabs = False

    fragments = classify_scad(data)

    current_line = []
    raw_lines = []
    min_balance = 0
    balance = 0

    for frag in fragments:
        if frag.type == 'code':
            for c in re.findall(r'[\(\)\[\]\{\}]', frag.text):
                if c in '([{':
                    balance += 1
                else:
                    balance -= 1
                    if balance < min_balance:
                        min_balance = balance

        if frag.type in {'code', 'comment'}:
            fraglines = frag.text.split('\n')
            current_line.append(fraglines[0])
            for t in fraglines[1:]:
                raw_lines.append((current_line, balance, min_balance))
                current_line = [t]
                min_balance = balance
        else:
            current_line.append(frag.text)

    raw_lines.append((current_line, balance, max(0, min_balance)))

    indent_balance = [0]

    lines = []
    for line, balance, min_balance in raw_lines:
        line = ''.join(line)
        is_dos = line.endswith('\r')
        if is_dos:
            line = line[:-1]

        m = RX_INDENT.match(line)
        indent_space, text, trailing = m.groups()
        line = Line(indent_space, text, trailing, is_dos, balance, min_balance)


        if min_balance > indent_balance[-1]:
            indent_balance.append(min_balance)

        while indent_balance and (min_balance < indent_balance[-1]):
            indent_balance.pop()

        if not indent_balance:
            indent_balance.append(0)

        line.indent_level = len(indent_balance) - 1

        if '\t' in line.indent_space:
            uses_tabs = True

        lines.append(line)

    if uses_tabs:
        indent = '\t'
    else:
        indent = '    '

    indentation_changed = 0
    trailing_cleared = 0
    blank_cleared = 0

    # Update indentation in all lines
    for line in lines:
        if line.text:
            new_indent = indent * max(0, line.indent_level)
            if new_indent != line.indent_space:
                indentation_changed += 1
                line.indent_space = new_indent

            if line.trailing:
                trailing_cleared += 1
        else:
            if line.indent_space:
                line.indent_space = ''
                blank_cleared += 1

    data = '\n'.join(line.indent_space + line.text + ('\r' if line.is_dos else '') for line in lines)

    # Restore the BOM if the original file had one
    if has_bom:
        data = BOM + data

    return data, indentation_changed, trailing_cleared, blank_cleared

def report_changes(indentation_changed, trailing_cleared, blank_cleared, file=sys.stderr):
    if not (indentation_changed or trailing_cleared or blank_cleared):
        print('    No changes.', file=file)

    if indentation_changed:
        print(f'   Lines with indentation changed: {indentation_changed}', file=file)

    if trailing_cleared:
        print(f'   Lines with trailing whitespace cleared: {trailing_cleared}', file=file)

    if blank_cleared:
        print(f'   Blank lines with whitespace cleared: {blank_cleared}', file=file)

if __name__ == '__main__':
    main()
