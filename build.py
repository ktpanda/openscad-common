#!/usr/bin/python3
import sys
import re
import time
import os
import argparse
import subprocess
import traceback
import hashlib
import ast
import threading

import tempfile
from io import BytesIO
from pathlib import Path
from contextlib import contextmanager
from typing import Optional, Union
from collections import deque
from dataclasses import dataclass, field

from stl import STL
from utils import save_file


CLEAR = '\x1b[0K'
CLEAR_REST = '\x1b[0K\n'
CLEARBELOW = '\x1b[0J'

WRAPON = '\x1b[?7h'
WRAPOFF = '\x1b[?7l'

CURSON = '\x1b[?25h'
CURSOFF = '\x1b[?25l'

RESULT_NONE, RESULT_ERROR, RESULT_BUILT, RESULT_CACHED = range(4)

RX_CSGLINE = re.compile(r'''
\s*
(?:
   \}  # closing brace
   |
   (.*?)        # module name
   \( (.*) \)   # arguments
   \s*
   ([;\{]))     # opening brace or semicolon
''', re.X)

def hash_file(fpath):
    h = hashlib.sha256()
    with fpath.open('rb') as fp:
        while True:
            data = fp.read(65536)
            if not data:
                break
            h.update(data)
    return h.hexdigest()

def find_openscad():
    openscad = os.getenv('OPENSCAD')
    if openscad is None or not Path(openscad).exists():
        return 'openscad'
    return openscad

def get_default_cache_path():
    base_path = os.getenv('XDG_CACHE_HOME')
    base_path = Path(base_path) if base_path else (Path.home() / '.cache')
    return base_path / 'openscad-stl'

class BuildError(Exception):
    pass

@dataclass
class CSGNode:
    mod:str
    args:str
    children:list = None

    def dump(self, lst, indent=''):
        if self.children:
            lst.append(f'{indent}{self.mod}({self.args}) {{\n')
            for c in self.children:
                c.dump(lst, indent + ' ')
            lst.append(f'{indent}}}\n')
        else:
            lst.append(f'{indent}{self.mod}({self.args});\n')

    def simplify(self):
        if self.children:
            nc = []
            for c in self.children:
                c = c.simplify()
                if self.mod == 'group' and c.mod == 'group':
                    nc.extend(c.children or ())
                else:
                    nc.append(c)

            if self.mod in {'group', 'union', 'difference', 'intersection'} and len(nc) == 1:
                return nc[0]
            self.children = nc
        return self

class SCADBuilder:
    def __init__(self, input, output, args, openscad_path=None, cache_path=None, rebuild=False, cache=True):
        self.input = input
        self.output = output
        self.args = args

        if openscad_path is None:
            openscad_path = find_openscad()

        if cache_path is None:
            cache_path = get_default_cache_path()

        self.openscad_path = openscad_path
        self.rebuild = rebuild
        self.cache_path = cache_path
        self.cache = cache

        self.csg_scad = None
        self.csg_hash = None

        self.render_time = None

        self.result = RESULT_NONE
        self.error_text = None

    def get_error_text(self):
        if self.error_text:
            yield(f'errors detected for {self.output}:')
            lines = self.error_text.split('\n')
            while lines and lines[-1] == '':
                lines.pop()

            for line in lines:
                yield(f'   | {line}')

    def report_errors(self):
        for line in self.get_error_text():
            print(line, file=sys.stderr)

    def run_prog(self, args, capt=False):
        proc = subprocess.run(
            args,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE if capt else subprocess.STDOUT,
            check=False,
            encoding='utf8'
        )

        if proc.returncode:
            self.error_text = proc.stderr if capt else proc.stdout
            self.result = RESULT_ERROR
            raise BuildError()

        return proc.stdout

    def get_csg(self):
        scadargs = [self.openscad_path, self.input, '--export-format=csg', '-o', '-', '-D$preview=false', '-Dhighres=true' ]
        scadargs.extend(self.args)
        csg_scad = self.run_prog(scadargs, True)

        root = CSGNode('group', '', [])
        node_stack = [root]
        for line in csg_scad.splitlines():
            if m := RX_CSGLINE.match(line):
                mod = m.group(1)
                if mod is None:
                    node_stack.pop()
                    continue
                args = m.group(2)
                leaf = m.group(3) == ';'

                if mod == 'import':
                    if m := re.match(r'(file = ")((?:\\.|[^\"])*)(",.*), timestamp = \d+', args):
                        path = ast.literal_eval(f'"{m.group(2)}"')
                        fpath = self.input.parent / path
                        try:
                            hash = hash_file(fpath)
                        except IOError as e:
                            print(f'WARNING: Could not read {fpath}: {e}')
                        else:
                            args = m.group(1) + hash + m.group(3)

                node = CSGNode(mod, args, None if leaf else [])
                node_stack[-1].children.append(node)
                if not leaf:
                    node_stack.append(node)

        root = root.simplify()
        text = []
        root.dump(text)
        self.csg_scad = ''.join(text)

        suffix = self.output.suffix
        h = hashlib.sha256()
        h.update(self.csg_scad.encode('utf8'))
        self.csg_hash = h.hexdigest()
        self.cache_file = self.cache_path / self.csg_hash[:3] / f'{self.csg_hash}{suffix}'

    def check_cache(self):
        if not self.rebuild:
            try:
                stl_cache = self.cache_file.read_bytes()
            except FileNotFoundError:
                pass
            else:
                save_file(self.output, stl_cache)
                self.result = RESULT_CACHED
                return True

        if self.cache:
            self.cache_file.parent.mkdir(exist_ok=True, parents=True)

        return False

    def render_output(self):
        suffix = self.output.suffix
        if suffix == '.stl':
            export_format = 'binstl'
        else:
            export_format = suffix[1:]

        with tempfile.NamedTemporaryFile(suffix=suffix) as tempf:
            tempf.close()

            sttime = time.time()

            scadargs = [self.openscad_path, self.input, f'--export-format={export_format}', '-o', tempf.name, '-D$preview=false', '-Dhighres=true' ]
            scadargs.extend(self.args)

            self.run_prog(scadargs)

            etime = time.time()

            self.render_time = etime - sttime

            if suffix == '.stl':
                stl = STL(tempf.name)
                stl.sort()
                out_data = stl.tobytes()
            else:
                out_data = Path(tempf.name).read_bytes()

        if self.cache:
            save_file(self.cache_file, out_data)

        save_file(self.output, out_data)
        self.result = RESULT_BUILT

    def build(self):
        try:
            self.get_csg()

            if self.check_cache():
                return True

            self.render_output()

            return True

        except BuildError:
            return False

class BuilderThread(threading.Thread):
    def __init__(self, mb):
        super().__init__(daemon=True)
        self.mb = mb
        self.status = 'waiting'
        self.done = False

    def set_status(self, text, msg=None):
        with self.mb.lock:
            self.status = text
            if msg is not None:
                self.mb.messages.append(msg)
            self.mb.status_cond.notify()

    def run(self):
        while (sb := self.mb._get_queue()) is not None:
            try:
                sb.status = 'csg'
                self.mb.message()
                sb.get_csg()

                if not sb.check_cache():
                    sb.status = 'rendering'
                    self.mb.message(f'Building {sb.output} (hash={sb.csg_hash})')
                    sb.render_output()

                    render_time = sb.render_time
                    if render_time < 60:
                        time_text = f'{render_time:.1f}s'
                    else:
                        render_time = int(render_time)
                        mins = render_time // 60
                        secs = render_time % 60
                        time_text = f'{mins}:{secs:02d}'

                    self.mb.message(f'Building {sb.output} complete in {time_text}')
                else:
                    self.mb.message(f'Using cached STL for {sb.output} (hash={sb.csg_hash})', notify=False)
                sb.status = 'done'
            except BuildError:
                sb.status = 'error'
                self.mb.message(list(sb.get_error_text()))
            except Exception as e:
                self.mb.message(traceback.format_exc())

        self.done = True
        self.mb.message()

class MultiBuilder:
    def __init__(self, openscad_path=None, cache_path=None, rebuild=False, cache=True, parallel=2):
        if openscad_path is None:
            openscad_path = find_openscad()

        if cache_path is None:
            cache_path = get_default_cache_path()

        self.openscad_path = openscad_path
        self.rebuild = rebuild
        self.cache_path = cache_path
        self.cache = cache
        self.parallel = parallel

        self.lock = threading.Lock()
        self.status_cond = threading.Condition(self.lock)

        self.all_builders = []
        self.queue = deque()
        self.threads = [BuilderThread(self) for j in range(parallel)]

        self.status_nlines = 0

        self.messages = []

    def add(self, input, output, args):
        sb = SCADBuilder(input, output, args, self.openscad_path, self.cache_path, self.rebuild, self.cache)
        self.all_builders.append(sb)
        self.queue.append(sb)
        sb.status = 'waiting'

    def _get_queue(self):
        with self.lock:
            try:
                sb = self.queue.popleft()
                return sb
            except IndexError:
                return None

    def _update_status(self):
        text = []

        for msg in self.messages:
            text.append(msg)
            text.append(CLEAR_REST)

        del self.messages[:]

        text.append(WRAPOFF)

        lines_written = 0

        for sb in self.all_builders:
            if sb.status in {'waiting', 'done', 'error'}:
                continue

            if lines_written:
                text.append('\n')
            text.append(f'{CLEAR}{sb.output}: {sb.status}')
            lines_written += 1

        text.append(f'\r')
        if lines_written > 1:
            text.append(f'\x1b[{lines_written - 1}A')
        text.append(WRAPON)

        #print(repr(''.join(text)))
        sys.stdout.write(''.join(text))
        sys.stdout.flush()

    def message(self, text=None, notify=True):
        with self.lock:
            if text:
                if hasattr(text, 'splitlines'):
                    self.messages.extend(text.splitlines())
                else:
                    self.messages.extend(text)

            if notify:
                self.status_cond.notify()

    def run(self):
        for thr in self.threads:
            thr.start()

        try:
            with self.lock:
                while True:
                    if all(thr.done for thr in self.threads):
                        break
                    self._update_status()
                    self.status_cond.wait(10)
                self._update_status()
        finally:
            sys.stdout.write(CLEARBELOW)
            sys.stdout.flush()

        for thr in self.threads:
            thr.join()

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('-i', '--input', type=Path, help='Input SCAD file')
    p.add_argument('-o', '--output', type=Path, help='Output STL file')
    p.add_argument('-O', '--csgoutput', type=Path, help='Output CSG (SCAD) file')
    p.add_argument('-c', '--cache-path', type=Path, help='Path to cache')
    p.add_argument('-n', '--nocache', action='store_true', help='Do not cache result')
    p.add_argument('-r', '--rebuild', action='store_true', help='Rebuild even if cache matches')
    p.add_argument('-p', '--prog', default=find_openscad(), help='Path to OpenSCAD')
    p.add_argument('osargs', nargs='*', help='Extra OpenSCAD arguments')
    args = p.parse_args()

    if not args.cache_path:
        args.cache_path = get_default_cache_path()

    if not args.input:
        p.print_help()
        return

    if not args.output and not args.csgoutput:
        args.output = args.input.with_suffix('.stl')
        if args.output == args.input:
            args.input = args.output.with_suffix('.scad')

    sb = SCADBuilder(args.input, args.output, args.osargs, args.prog, args.cache_path, args.rebuild, not args.nocache)

    try:
        sb.get_csg()

        if args.csgoutput:
            args.csgoutput.write_text(sb.csg_scad, encoding='utf8')

        if not args.output:
            print(f'{args.input}: hash={sb.csg_hash}')
            return

        if sb.check_cache():
            print(f'Using cached STL for {args.output} (hash={sb.csg_hash})')
            return 0

        print(f'Building {args.output} (hash={sb.csg_hash})')

        sb.render_output()

        render_time = sb.render_time
        if render_time < 60:
            time_text = f'{render_time:.1f}s'
        else:
            render_time = int(render_time)
            mins = render_time // 60
            secs = render_time % 60
            time_text = f'{mins}:{secs:02d}'

        print(f'Building {args.output} complete in {time_text}')

    except BuildError:
        sb.report_errors()
        return 1


if __name__ == '__main__':
    sys.exit(main())
