import os
import re
import json
import traceback
import tempfile
import argparse
import multiprocessing
from collections import namedtuple
from pathlib import Path

from utils import save_file

RX_INCLUDE = re.compile(r'(include|use)\s*<(.*?)>', re.S | re.X)
RX_MODULE = re.compile(r'module\s+(\w+)\s*\(', re.M)

RX_CLASSIFY = re.compile(r'/\*|"|\n|//', re.S | re.X)
RX_LINEEND = re.compile(r'.*?\n')
RX_COMMENT_END = re.compile(r'.*?\*/', re.S)
RX_STRING_END = re.compile(r'(\\.|[^"])*"')

SCADFragment = namedtuple('SCADFragment', ['type', 'text'])

def classify_scad(text):
    '''Parse through `text` and return a list of `SCADFragment` tuples.
    '''
    output = []
    pos = 0
    while True:
        m = RX_CLASSIFY.search(text, pos)
        if not m:
            ntext = text[pos:]
            if ntext:
                output.append(SCADFragment('code', ntext))
            return output

        found = m.group(0)

        endpos = m.start()
        curtype = None
        lookfor = None

        if found == '\n':
            endpos = m.end()
        elif found == '//':
            curtype = 'comment'
            lookfor = RX_LINEEND
        elif found == '/*':
            curtype = 'comment'
            lookfor = RX_COMMENT_END
        elif found == '"':
            curtype = 'string'
            lookfor = RX_STRING_END

        ntext = text[pos:endpos]
        if ntext:
            output.append(SCADFragment('code', ntext))
        pos = endpos

        if lookfor is not None:
            endpos = lookfor.match(text, m.end())
            if not endpos:
                output.append(SCADFragment(curtype, text[pos:]))
                return output

            endpos = endpos.end()
            output.append(SCADFragment(curtype, text[pos:endpos]))
            pos = endpos


class SCADFile:
    def __init__(self, path):
        self.path = path
        self.deps = [path]
        self.use_deps = []
        self.include_deps = []
        self.modules = []

    def _find_modules(self, modset):
        for c in self.include_deps:
            c._find_modules(modset)
        for m in self.modules:
            modset[m] = self

    def find_modules(self):
        modset = {}
        for c in self.use_deps:
            c._find_modules(modset)
        self._find_modules(modset)
        return modset

class DependencyFinder:
    def __init__(self):
        self.scads = {}

    def read_scad(self, path):
        if path in self.scads:
            return self.scads[path]

        scad = self.scads[path] = SCADFile(path)
        deps = scad.deps
        if path.name.startswith('gen-'):
            return scad

        try:
            text = path.read_text(encoding='utf8')
        except OSError:
            traceback.print_exc()
            return scad

        for frag in classify_scad(text):
            if frag.type == 'code':
                for m in RX_MODULE.finditer(frag.text):
                    scad.modules.append(m.group(1))

                if m := RX_INCLUDE.search(frag.text):
                    subscad = self.read_scad(path.parent / m.group(2))
                    (scad.use_deps if m.group(1) == 'use' else scad.include_deps).append(subscad)
                    for p in subscad.deps:
                        if p not in deps:
                            deps.append(p)
        return scad

def scad_encode_val(val):
    if val is True:
        return 'true'

    if val is False:
        return 'false'

    if func := getattr(val, 'toscad', None):
        return func()

    if isinstance(val, (int, float)):
        return repr(val)

    if isinstance(val, str):
        val = val.replace('\\', r'\\')
        val = val.replace('\'', r'\'')
        val = val.replace('\"', r'\"')
        val = val.replace('\n', r'\n')
        return f'"{val}"'

    if hasattr(val, '__iter__'):
        parts = [scad_encode_val(p) for p in val]
        return '[' + ','.join(parts) + ']'

class SourceFile:
    def __init__(self, path, name=None, automain=False, common_xform=None):
        self.path = Path(path)
        if name is None:
            name = path.stem
        self.name = name
        self._automain = automain
        self.parts = []
        self.part_by_name = {}
        self.data = []
        self.common_xform = common_xform

    def part(self, name, **defs):
        p = Part(name, defs)
        self.parts.append(p)
        self.part_by_name[p.name] = p
        return p

    def data(self, *files):
        for f in files:
            self.data.append(Path(f))

    def automain(self):
        self._automain = True

    def gen_automain(self, dp):
        data = self.path.read_bytes()
        text = data.decode('utf8')
        nlpos = text.find('\n')
        is_dos = False
        if nlpos > 0 and text[nlpos - 1] == '\r':
            is_dos = True
            text = text.replace('\r\n', '\n')

        start_marker = '\n// === automain start ===\n'
        end_marker = '\n// === automain end ===\n'
        start_pos = text.find(start_marker)
        end_pos = text.find(end_marker, start_pos)

        separate = False
        if isinstance(self._automain, str):
            separate = True
            outf = self.path.parent / f'{self._automain}.scad'
            module_name = self._automain.replace('-', '_')
        elif m := re.search(r'^\s*include\s*<\s*(automain[_/\-/\\])((?:\w|-|_)+?)\.scad\s*>', text, re.M):
            separate = True
            outf = self.path.parent / f'{m.group(1)}{m.group(2)}.scad'
            module_name = 'automain_' + m.group(2).replace('-', '_')

        if separate:
            automain_text = [
                f'// Generated by make.py, DO NOT EDIT\n\n'
                f'module {module_name}() {{\n'
            ]
            seen_modules = ()
            pre_txt = text
            post_txt = ''
        else:
            base_scad = dp.read_scad(self.path)

            seen_modules = base_scad.find_modules()


            if start_pos != -1 and end_pos != -1:
                pre_txt = text[:start_pos]
                post_txt = text[end_pos + len(end_marker):]
            else:
                pre_txt = text
                post_txt = ''

            automain_text = [start_marker]

            common_xform = self.common_xform
            if common_xform is None:
                common_xform = 'preview_slice(axis=slice_axis, vc=slice_pos)'

            automain_text.append(common_xform + ' {\n')

        all_modules = {}
        iftxt = 'if'
        for part in self.parts:
            all_modules[part.module] = []
            preview_module = 'preview_' + part.module
            have_preview = separate or preview_module in seen_modules
            if have_preview:
                all_modules[preview_module] = []

            automain_text.append(f'    {iftxt} (part == "{part.name}") {{\n')
            automain_text.append(f'        if ($preview) {{\n')
            if separate:
                modname = f'preview_xform_{part.module}'
                txt = []
                if part.preview_xform:
                    txt = [f'    {part.preview_xform}\n']
                txt.append('    children();\n')
                all_modules.setdefault(modname, txt)
                automain_text.append(f'            {modname}()\n')

            elif part.preview_xform:
                automain_text.append(f'            {part.preview_xform}\n')

            automain_text.append(f'            {part.module}({part.modargs});\n')

            if have_preview:
                automain_text.append(f'            preview_{part.module}({part.modargs});\n')

            automain_text.append(f'        }} else {{\n')


            if separate:
                modname = f'out_xform_{part.module}'
                txt = []
                if part.out_xform:
                    txt = [f'    {part.out_xform}\n']
                txt.append('    children();\n')
                all_modules.setdefault(modname, txt)
                automain_text.append(f'            {modname}()\n')

            elif part.out_xform:
                automain_text.append(f'            {part.out_xform}\n')

            automain_text.append(f'            {part.module}({part.modargs});\n')

            if part.out_extra:
                automain_text.append(f'            {part.out_extra}({part.modargs});\n')
                all_modules[part.out_extra] = []
            automain_text.append(f'        }}\n')
            automain_text.append(f'    }}\n')

            iftxt = 'else if'

        automain_text.append('    else {\n')
        automain_text.append('        assert(false, str("invalid part ", part));\n')
        automain_text.append('    }\n')
        automain_text.append('}\n')

        def repl_part(m):
            txt = f'{m.group(1)}{", ".join(self.part_by_name)}{m.group(3)}'
            return txt

        #^(part\s*=\s*\"[^\"]*\"\s*;\s*\[)(.*)(\].*)'
        pre_txt = re.sub(r'(?m)^(\s*part\s*=\s*\"[^\"]*\"\s*;\s*//\s*\[)(.*)(\]\s*)$', repl_part, pre_txt)

        out_text = [pre_txt]
        if not separate:
            for mod in all_modules:
                if mod not in seen_modules:
                    out_text.append(f'\nmodule {mod}() {{\n')
                    out_text.append(f'}}\n')

            out_text.extend(automain_text)
            out_text.append(end_marker)
            out_text.append(post_txt)

        out_text = ''.join(out_text)
        if out_text != text:
            print(f'Automain: regenerating {self.path}')
            if is_dos:
                out_text = out_text.replace('\n', '\r\n')
            self.path.write_bytes(out_text.encode('utf8'))

        if separate:
            for mod, content in all_modules.items():
                automain_text.append(f'\nmodule {mod}() {{\n')
                automain_text.extend(content)
                automain_text.append(f'}}\n')

            outf.parent.mkdir(exist_ok=True)
            outf.write_text(''.join(automain_text))

class Target:
    def __init__(self, parent=None, defs=None):
        self.parent = parent
        self._path = (self.parent._path if parent else ()) + (self,)

        self.stldata = None

        if defs is None:
            defs = {}

        self.ftype = defs.pop('ftype', None)
        self.desc = defs.pop('desc', None)
        desc_add = defs.pop('desc_add', None)

        if desc_add is not None:
            base = self.desc
            if base is None and parent is not None:
                base = parent.desc

            if base is None:
                self.desc = desc_add
            else:
                self.desc = f'{base} {desc_add}'

        self.defs = defs

        self.output_enable = True
        self.output_name_override = None

        # compat
        if 'stl' in defs:
            output = defs.pop('stl')
        else:
            output = defs.pop('output', True)

        if isinstance(output, str):
            p = Path(output)
            self.ftype = p.suffix.strip('.')
            self.output_name_override = p.stem
        elif output == False:
            self.output_enable = False

    def _get_name_part(self):
        return None

    def _get_ftype(self):
        if self.ftype is not None:
            return self.ftype

        if self.parent:
            return self.parent._get_ftype()

        return 'stl'

    def gen_defs(self):
        rv = {}
        for t in self._path:
            rv.update(t.defs)
        return rv

    def get_filename(self, prefix=''):
        name_parts = []
        if prefix:
            name_parts.append(prefix)

        for t in self._path:
            if t.output_name_override:
                name_parts.clear()
                name_parts.append(t.output_name_override)
                continue

            name = t._get_name_part()
            if name:
                name_parts.append(name)

        return '-'.join(name_parts) + '.' + self._get_ftype()

    def _add_args(self, outlst):
        pass

    def gen_args(self):
        defs = self.gen_defs()

        out = []

        for t in self._path:
            t._add_args(out)

        for name, val in defs.items():
            if val is not None:
                val = scad_encode_val(val)
                out.append(f"-D{name}={val}")
        return out

    def get_children(self):
        if self.output_enable:
            yield self

class Colorable(Target):
    def __init__(self, parent, defs):
        super().__init__(parent, defs)
        colors = self.defs.pop('colors', None)
        if colors is None:
            ncolors = self.defs.pop('ncolors', None)
            if ncolors is not None:
                colors = [f'c{i}' for i in range(ncolors)]

        if colors is None:
            self._color_def = None
            self._children = None
        else:
            composite = self.defs.pop('composite', False)
            self._color_def = colors, composite
            self._children = [ColorPart(self, i, name) for i, name in enumerate(colors)]


    def _get_color_def(self):
        nc = self._color_def
        if nc is None and self.parent is not None:
            nc = self.parent._get_color_def()

        if nc is None:
            return None, True

        return nc

    def get_children(self):
        if not self.output_enable:
            return

        colors, composite = self._get_color_def()
        if composite:
            yield self

        if colors is not None:
            yield from self._children

class Part(Colorable):
    def __init__(self, name, defs):
        super().__init__(None, defs)
        self.name = name

        self.variants = []

        self.preview_xform = defs.pop('preview_xform', None)
        self.out_xform = defs.pop('out_xform', None)
        self.module = (defs.pop('mod', None) or name).replace('-', '_')
        self.modargs = defs.pop('args', '')
        self.out_extra = defs.pop('out_extra', None)
        self.is_default = defs.pop('default', False)

        if not self.is_default:
            self.name_part = name

    def get_children(self):
        yield from super().get_children()
        for sp in self.variants:
            yield from sp.get_children()

    def _add_args(self, outlst):
        outlst.append(f"-Dpart={scad_encode_val(self.name)}")

    def _get_name_part(self):
        return None if self.is_default else self.name

    def variant(self, suffix, **defs):
        np = Variant(self, suffix, defs)
        self.variants.append(np)
        return np

    # for compatibility
    subpart = variant

class Variant(Colorable):
    def __init__(self, part, suffix, defs):
        super().__init__(part, defs)
        self.suffix = suffix

    def _get_name_part(self):
        return self.suffix

class MergePart:
    def __init__(self, out_stl, sources, desc=None):
        self.out_stl = out_stl
        self.sources = sources
        self.desc = None

class ColorPart(Target):
    def __init__(self, variant, index, name):
        super().__init__(variant)
        self.index = index
        self.name = name
        self.desc = variant.desc
        if name and self.desc is not None:
            self.desc += f' ({name})'
        self.defs['$part_color'] = self.index
        self.defs['$part_color_name'] = self.name

    def _get_name_part(self):
        return self.name

class MakeGen:
    def __init__(self):
        self.source_by_name = {}
        self.sources = []
        self.genscripts = []
        self.mergeparts = []

    def gen(self, base):
        self.genscripts.append(base)

    def source(self, name, path=None, automain=False, common_xform=None):
        try:
            return self.source_by_name[name]
        except KeyError:
            pass
        if path is None:
            path = name + '.scad'
        p = self.source_by_name[name] = SourceFile(Path(path), name, automain, common_xform)
        self.sources.append(p)
        return p

    def mergepart(self, out_stl, sources, desc=None):
        self.mergeparts.append(MergePart(out_stl, sources, desc))

    def all_targets(self):
        for source in self.sources:
            for part in source.parts:
                yield from part.get_children()

    def update_customizer(self):
        for source in self.sources:
            if not source.parts:
                continue

            json_path = source.path.with_suffix('.json')
            try:
                customizer_data = json.loads(json_path.read_text('utf8'))
            except (ValueError, FileNotFoundError):
                customizer_data = {
                    "fileFormatVersion": "1",
                    "parameterSets": {}
                }

            param_sets = customizer_data.setdefault('parameterSets', {})

            for part in source.parts:
                for variant in part.get_children():
                    if not variant.output_enable:
                        continue

                    filename = variant.get_filename(source.name)
                    if filename is None:
                        continue

                    defs = {'part': part.name}
                    defs.update(part.defs)
                    defs.update(variant.defs)
                    param_sets[filename] = defs

            save_file(json_path, json.dumps(customizer_data, indent=4))


    def generate_makefile(self, outpath='Makefile'):
        outlines = []
        all_parts = []
        for source in self.sources:
            if source.parts:
                for part in source.parts:
                    for variant in part.get_children():
                        if not variant.output_enable:
                            continue

                        filename = variant.get_filename(source.name)
                        if filename is not None:
                            all_parts.append((filename, source, variant))
            else:
                all_parts.append((f'{source.path.stem}.stl', source, None))

        dp = DependencyFinder()

        outlines.append(f'all = ' + " ".join(name for name, source, part in all_parts))
        outlines.append('')
        outlines.append('.PHONY: all')
        outlines.append('')
        outlines.append('all: $(all)')
        outlines.append('')
        for name, source, part in all_parts:
            scad = dp.read_scad(source.path)
            all_deps = scad.deps + source.data
            deps = ' '.join(str(path).replace('\\', '/') for path in all_deps)
            outlines.append(f'{name}: {deps}')

            scadargs = part.gen_args() if part else []

            outlines.append('\t./ktpanda/build.py -i $< -o $@ -- ' + ' '.join(makefile_escape(v) for v in scadargs))
            outlines.append('')

        for base in self.genscripts:
            outlines.append(f'{base}.scad: {base}.py ktpanda/genshape.py')
            outlines.append(f'\t./{base}.py')
            outlines.append('')

        Path(outpath).write_text('\n'.join(outlines), encoding='utf8')

def makefile_escape(v):
    v = v.replace('\\', r'\\')
    v = v.replace("'", r"'\''")
    v = v.replace('$', '$$')
    return f"'{v}'"
