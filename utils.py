import os
import tempfile
from typing import Optional, Union
from pathlib import Path
from contextlib import contextmanager

@contextmanager
def _save_file(path:Path, **kw):
    '''Save a file to path, creating a temporary file while writing, then atomically replacing'''

    tf = tempfile.NamedTemporaryFile(
        dir=str(path.parent), prefix=path.name,
        suffix='.write-temp', delete=False, **kw)

    ok = False
    try:
        with tf:
            yield tf

        os.replace(tf.name, path)
        ok = True
    finally:
        if not ok:
            try:
                os.unlink(tf.name)
            except OSError:
                pass

def save_file(path:Path, data:Optional[Union[str, bytes]]=None, **kw):
    '''Writes `data` to a path, first writing to a temporary file, then atomically
    replacing the destination. If `data` is a `bytes` object, the file is opened in binary
    mode. If `data` is a `str`, the file is opened as text, with the default encoding of
    `"utf8"`. If `data` is None, then a context manager is returned allowing the file to
    be incrementally written. Extra keyword arguments are passed to
    `tempfile.NamedTemporaryFile`.'''

    if isinstance(data, bytes):
        with _save_file(path, **kw) as fp:
            fp.write(data)
    elif isinstance(data, str):
        kw.setdefault('mode', 'w')
        kw.setdefault('encoding', 'utf8')
        with _save_file(path, **kw) as fp:
            fp.write(data)
    else:
        return _save_file(path, **kw)
