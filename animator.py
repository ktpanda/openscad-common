#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
import math
import tempfile
import ast
from pathlib import Path
from collections import namedtuple

from scadutils import scad_encode_val as toscad

KeyFrame = namedtuple('KeyFrame', ['time', 'value'])

class vector(list):
    '''
    Subclass of list which behaves more like an OpenSCAD vector
    '''

    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]

    @property
    def z(self):
        return self[2]

    def __add__(self, o):
        if isinstance(o, list):
            return vector((a + b) for a, b in zip(self, o))
        else:
            return vector(a + o for a in self)

    __iadd__ = __add__

    def __sub__(self, o):
        if isinstance(o, list):
            return vector((a - b) for a, b in zip(self, o))
        else:
            return vector(a - o for a in self)

    __isub__ = __sub__

    def __mul__(self, o):
        if isinstance(o, list):
            return vector((a * b) for a, b in zip(self, o))
        else:
            return vector(a * o for a in self)

    __imul__ = __mul__

    def __div__(self, o):
        if isinstance(o, list):
            return vector((a / b) for a, b in zip(self, o))
        else:
            return vector(a / o for a in self)

    __idiv__ = __div__

def tovec(value):
    '''Converts a value to a vector if it is iterable, or just returns the value if it is not.'''
    if isinstance(value, vector):
        return value
    elif hasattr(value, '__iter__'):
        return vector(tovec(v) for v in value)
    else:
        return value

def vlength(value):
    '''Returns the euclidean length of a vector. For scalars, returns the absolute value.'''
    if isinstance(value, list):
        cv = 0
        for v in value:
            cv += v * v
        return math.sqrt(cv)
    else:
        return abs(value)

def lerp_keyframe(k1, k2, t):
    if t <= k1.time:
        return k1.value
    elif t >= k2.time:
        return k2.value
    else:
        if isinstance(k1.value, bool) or isinstance(k2.value, bool):
            return k1.value
        return k1.value + (k2.value - k1.value) * ((t - k1.time) / (k2.time - k1.time))

class AnimVar:
    '''Represents an animated variable with associated keyframes'''

    def __init__(self, ival=0, speed=1):
        self.keyframes = [KeyFrame(0.0, tovec(ival))]
        self.speed = speed
        self.name = None
        self.timefunc = lambda: 0.0

    def truncate(self, t):
        '''Removes any keyframes after the given time and replace it with a keyframe at that time '''
        k2 = self.keyframes[-1]
        while t < self.keyframes[-1].time and len(self.keyframes) > 1:
            k2 = self.keyframes.pop()

        k1 = self.keyframes[-1]

        nkf = KeyFrame(t, lerp_keyframe(k1, k2, t))
        if nkf != k1:
            self.keyframes.append(nkf)

        return nkf

    def toscad(self):
        '''Returns the variable name (for use with scad_encode_val)'''
        return f'${self.name}'

    @property
    def value(self):
        '''Returns the value at the current global time'''
        return self.value_at(self.timefunc())

    def value_at(self, t):
        '''Calculates the value at the given time'''
        idx = len(self.keyframes) - 1
        while idx >= 0 and t < (k2 := self.keyframes[idx]).time:
            idx -= 1
        k1 = self.keyframes[max(0, idx - 1)]
        return lerp_keyframe(k1, k2, t)

    def reset(self, value=None, speed=None):
        '''Clears all keyframes and optionally changes the initial value.'''
        if value is None:
            value = self.keyframes[0].value

        if speed is not None:
            self.speed = speed

        self.keyframes = [KeyFrame(0.0, tovec(value))]

        return self

    def snap(self, value, t=None):
        '''Instantly change the value at the given time or the current time if not specified'''
        value = tovec(value)
        if t is None:
            t = self.timefunc()
        lkf = self.truncate(t)
        nkf = KeyFrame(t, value)
        if nkf != lkf:
            self.keyframes.append(nkf)
        return nkf

    def move(self, dt=None, et=None, dv=None, ev=None, speed=None, st=None):
        '''
        Start a linear move at the current time.

        The end position is determined by `dv` or `ev`. If `ev` is specified, then that is
        the final position, otherwise `dv` is added to the current position.

        The time of the move is determined by `dt`, `et`, or `speed`. If `et` is specified,
        the move will end at the given absolute time. If `dt` is specified, the move will
        end at current time + `dt`. If neither is specified, then `dt` is derived from
        the distance travelled divided by `speed`. If not specified, `speed` defaults
        to the value specified when the variable was defined.
        '''

        if st is None:
            st = self.timefunc()
        lkf = self.truncate(st)
        sv = lkf.value

        if ev is None:
            dv = tovec(dv)
            ev = sv + dv
        else:
            ev = tovec(ev)

        if et is None:
            if dt is None:
                if dv is None:
                    dv = ev - sv
                dt = vlength(dv) / (speed or self.speed)
            et = st + dt

        nkf = KeyFrame(et, ev)
        if nkf != lkf:
            self.keyframes.append(nkf)
        return nkf

class Animator:
    def __init__(self):
        self.vars = []

        self.scad_text = []

        self.modname = None

        self.globs = dict(
            __file__ = None,
            vector=vector,
            AnimVar=AnimVar,
            KeyFrame=KeyFrame,
            tovec=tovec,
            toscad=toscad,

            include_py = self.include_py,

            output_file = None,
            main_file = None,
            time = 0.0,
            a = self,
            _animator = self,

            imgsize = tovec((1920, 1080)),
            camera_pos = tovec((0, 0, 0)),
            camera_rot = tovec((45, 0, 45)),
            camera_dist = 100,
            camera_fov = None,
            projection = 'p',
            colorscheme = 'Metallic',
            wait=self.wait,
            defvar=self.addvar,
            use=self.scad_use,
            include=self.scad_include,
            scad=self.add_scad_text,
            generate=self.generate_scad,

            defines = {}
        )

    @property
    def time(self):
        return self.globs['time']

    @time.setter
    def time(self, v):
        self.globs['time'] = v

    def addvar(self, name, ival=0, speed=1):
        '''Defines an animated variable. `ival` is the initial value, and `speed` is the
        default speed for moves.'''

        var = AnimVar(ival, speed)
        var.name = name
        var.timefunc = lambda: self.globs['time']
        self.vars.append(var)
        self.globs[name] = var
        return var

    def wait(self, *values):
        '''Advance the global timer by a fixed amount, or to the final keyframe of an
        animated variable.'''

        for v in values:
            if isinstance(v, KeyFrame):
                nt = v.time
            elif isinstance(v, AnimVar):
                nt = v.keyframes[-1].time
            else:
                nt = self.time + v

            if nt > self.time:
                self.time = nt

    def add_scad_text(self, text):
        '''Add arbitrary OpenSCAD code to the output.'''
        self.scad_text.append(text)

    def scad_include(self, path):
        '''Add an `include` statement to the output.'''
        self.scad_text.append(f'include <{path}>\n')

    def scad_use(self, path):
        '''Add a `use` statement to the output.'''
        self.scad_text.append(f'use <{path}>\n')

    def define(self, **kwargs):
        '''Define variables to be passed to OpenSCAD when rendering'''
        self.globs['defines'].update(kwargs)

    def generate_scad(self, modname=None, xdefs=()):
        '''Ends the animation at the current time and generates keyframe tables. If
        `modname` is specified, then the variable definitions are placed into a module,
        otherwise they are just inserted into the outer level. `xdefs` is a list of lines
        of OpenSCAD code that are inserted right after the variable keyframe tables and can be used
        to derive extra variables from them.'''

        self.modname = modname

        scad_text = self.scad_text

        end_time = 0

        report_text = [
            f'end time = {self.time}',
            f'nframes(10fps) = {int(self.time * 10)}',
            f'nframes(15fps) = {int(self.time * 15)}',
            f'nframes(30fps) = {int(self.time * 30)}',
            f'nframes(60fps) = {int(self.time * 60)}',
        ]

        for line in report_text:
            print(line)
            scad_text.append(f'// {line}\n')
        print()

        scad_text.append(f'\n')

        var_prefix = ''
        indent = ''
        if modname is not None:
            indent = '    '
            var_prefix = f'{modname}_'

        scad_text.append(f'{var_prefix}anim_start = 0 + 0;\n')
        scad_text.append(f'{var_prefix}anim_end = 0 + {self.time};\n')

        scad_text.append(f'\n')

        if modname is not None:
            scad_text.append(f'module {modname}_var_default() {{\n')

        for var in self.vars:
            scad_text.append(f'{indent}${var.name} = {toscad(var.keyframes[0].value)};\n')


        if modname is not None:
            scad_text.append(f'\n')
            scad_text.append(f'    children();\n')
            scad_text.append(f'}}\n\n')
            scad_text.append(f'module {modname}() {{\n')

        scad_text.append(f'{indent}$ctime = {var_prefix}anim_start + $t * ({var_prefix}anim_end - {var_prefix}anim_start);\n\n')
        for var in self.vars:
            var.truncate(self.time)

            segs = []
            itr = iter(var.keyframes)

            ckf = next(itr, None)
            while (nkf := next(itr, None)) is not None:
                t0 = ckf.time
                t1 = nkf.time
                if t0 != t1:
                    v0 = ckf.value
                    v1 = nkf.value
                    if v0 == v1:
                        segs.append([t0, 0, v0])
                    else:
                        segs.append([t0, 3, t1, v0, v1])
                ckf = nkf

            # variable is constant?
            if len(segs) == 1 and segs[0][1] == 0:
                scad_text.append(f'{indent}${var.name} = {toscad(segs[0][2])};\n\n')
            else:
                scad_text.append(f'{indent}${var.name} = animate_var($ctime, [\n')

                for seg in segs:
                    scad_text.append(f'{indent}    {toscad(seg)},\n')

                scad_text.append(f'{indent}]);\n\n')

        for line in xdefs:
            scad_text.append(f'{indent}{line}\n')

        if modname is not None:
            scad_text.append(f'    children();\n')
            scad_text.append(f'}}\n')

    def include_py(self, path):
        current_file = self.globs['__file__']
        self.exec_py(Path(current_file).parent / path)

    def exec_py(self, path):
        current_file = self.globs['__file__']
        self.globs['__file__'] = str(path)

        # Read the source and parse it
        py_text = path.read_text('utf8').replace('\r\n', '\n').removeprefix('\ufeff')
        tree = ast.parse(py_text, str(path))
        newtree = ast.fix_missing_locations(SourceTransform().visit(tree))
        exec(compile(newtree, str(path), 'exec'), self.globs)

        self.globs['__file__'] = current_file


class SourceTransform(ast.NodeTransformer):
    '''AST transformer that converts list expressions into vectors.'''
    def visit_List(self, node):
        return ast.Call(
            func=ast.Name(id='vector', ctx=ast.Load()),
            args=[self.generic_visit(node)],
            keywords=[]
        )

    visit_ListComp = visit_List

class RenderJob:
    def __init__(self, jobnum, baseargs, renderpath, var_prefix, start_time, end_time, nframes):
        self.jobnum = jobnum
        self.baseargs = baseargs
        self.renderpath = renderpath
        self.var_prefix = var_prefix
        self.start_time = start_time
        self.end_time = end_time
        self.nframes = nframes
        self.proc = None

    def start(self):
        osargs = list(self.baseargs)

        osargs.append(f'--animate={self.nframes}')
        osargs.append(f'-D{self.var_prefix}anim_start={self.start_time}')
        osargs.append(f'-D{self.var_prefix}anim_end={self.end_time}')
        osargs.append(f'-o')
        osargs.append(self.renderpath / 'frame-.png')

        clean_frames(self.renderpath, self.nframes)

        self.proc = subprocess.Popen(osargs)

    def cancel(self):
        if self.proc is not None:
            self.proc.terminate()

    def wait(self, timeout=None):
        if self.proc is not None:
            self.proc.wait(timeout)
            self.proc = None

class FrameCalc:
    def __init__(self, start, fps):
        self.start = start
        self.fps = fps

    def frame_to_time(self, frame):
        return self.start + frame / self.fps

    def time_to_frame(self, time, roundup=False):
        frame = (time -  self.start) * self.fps
        if roundup:
            frame = math.ceil(frame)
        else:
            frame = math.floor(frame)
        return int(frame)

    def job_params(self, first_frame, end_frame, step=1):
        # frames equivalent to range(first_frame, end_frame, step)

        nframes = (end_frame - first_frame + (step - 1)) // step
        start = self.frame_to_time(first_frame)
        end = self.frame_to_time(first_frame + nframes * step)
        return start, end, nframes

def clean_frames(path, nframes):
    # A previous run may have generated frames past what we are rendering now,
    # so delete them
    for f in path.iterdir():
        if (m := re.match(r'^frame-(\d+).png', f.name)) and int(m.group(1)) >= nframes:
            f.unlink()

def main():
    p = argparse.ArgumentParser(description='OpenSCAD animation sequencer')
    p.add_argument('src', type=Path, help='Python script containing animation definition')
    p.add_argument('-s', '--scad', type=Path, help='OpenSCAD file to generate')
    p.add_argument('-r', '--render', type=Path, help='Render to video')
    p.add_argument('-P', '--render-path', type=Path, help='Render to directory')
    p.add_argument('-f', '--fps', type=float, default=30, help='Render FPS')
    p.add_argument('-p', '--projection', default='p', help='Camera projection')
    p.add_argument('-j', '--jobs', type=int, default=1, help='Number of OpenSCAD instances to run in parallel')
    p.add_argument('--size', type=int, metavar=('W', 'H'), nargs=2, help='Render image size')
    p.add_argument('--start', type=float, default=0, help='Render start time')
    p.add_argument('--end', type=float, help='Render end time')
    p.add_argument('ffargs', nargs='*', help='FFMpeg extra arguments')
    args = p.parse_intermixed_args()

    # Resolve all paths now since we do os.chdir to the directory that `src` is in
    if args.render_path is not None:
        args.render_path.mkdir(parents=True, exist_ok=True)
        args.render_path = args.render_path.resolve()

    if args.render:
        args.render = args.render.resolve()


    srcpath = args.src.resolve()

    os.chdir(srcpath.parent)

    anim = Animator()

    # Add the source directory to sys.path so the script can import modules from there
    sys.path.insert(0, str(srcpath.parent))

    print(f'Processing {args.src}')
    anim.exec_py(srcpath)

    if args.scad is None:
        output_file = anim.globs['output_file']
        if output_file is not None:
            args.scad = Path(output_file)
        else:
            args.scad = srcpath.with_suffix('.scad')

    main_file = anim.globs['main_file']
    if main_file is None:
        main_file = args.scad
    else:
        main_file = Path(main_file)

    scad_text = ''.join(anim.scad_text)
    args.scad.write_text(scad_text)

    tempdir = None
    if args.render or args.render_path:
        if args.size is None:
            args.size = tuple(anim.globs['imgsize'])

        if args.end is None:
            args.end = anim.time

        if args.render_path is None:
            tempdir = tempfile.TemporaryDirectory(prefix='scad-render-')
            args.render_path = Path(tempdir.name)

        try:
            fc = FrameCalc(args.start, args.fps)

            first_frame = 0
            last_frame = fc.time_to_frame(args.end, True)

            fov = anim.globs["camera_fov"] or 22.5

            if args.projection is None:
                args.projection = anim.globs['projection']

            osargs = [
                'openscad',
                main_file,
                f'--imgsize={args.size[0]},{args.size[1]}',
                f'--projection={args.projection}',
                f'--colorscheme={anim.globs["colorscheme"]}',
            ]

            osargs.append(f'-D$vpt={toscad(anim.globs["camera_pos"])}')
            osargs.append(f'-D$vpr={toscad(anim.globs["camera_rot"])}')
            osargs.append(f'-D$vpd={toscad(anim.globs["camera_dist"])}')
            osargs.append(f'-D$vpf={toscad(fov)}')

            var_prefix = f'{anim.modname}_' if anim.modname is not None else ''

            for k, v in anim.globs['defines'].items():
                osargs.append(f'-D{k}={toscad(v)}')

            jobs = []
            try:
                if args.jobs == 1:
                    start, end, nframes = fc.job_params(0, last_frame)
                    job = RenderJob(j, osargs, args.render_path, var_prefix, start, end, nframes)
                    jobs.append(job)
                    job.start()
                    job.wait()

                else:
                    for j in range(args.jobs):
                        start, end, nframes = fc.job_params(j, last_frame, args.jobs)
                        rpath = args.render_path / f'job-{j:02}'
                        rpath.mkdir(exist_ok=True)
                        job = RenderJob(j, osargs, rpath, var_prefix, start, end, nframes)
                        jobs.append(job)

                    for job in jobs:
                        job.start()

                    for job in jobs:
                        job.wait()

                    clean_frames(args.render_path, last_frame)

                    # Now combine all the rendered frames into one directory for ffmpeg
                    for frame in range(last_frame):
                        jobnum = frame % args.jobs
                        jobframe = frame // args.jobs
                        src = jobs[jobnum].renderpath / f'frame-{jobframe:05}.png'
                        dest = args.render_path / f'frame-{frame:05}.png'
                        os.replace(src, dest)

                    for job in jobs:
                        try:
                            job.renderpath.rmdir()
                        except IOError:
                            pass
            finally:
                for job in jobs:
                    job.cancel()

                for job in jobs:
                    try:
                        job.wait(.5)
                    except subprocess.TimeoutExpired:
                        pass

            if args.render:
                ffmpeg_args = [
                    'ffmpeg', '-r', str(args.fps), '-i', args.render_path / 'frame-%05d.png',
                ]
                ffmpeg_args.extend(args.ffargs)
                ffmpeg_args.extend(['-y', args.render])
                subprocess.check_call(ffmpeg_args)


        finally:
            if tempdir is not None:
                tempdir.cleanup()


if __name__ == '__main__':
    main()
