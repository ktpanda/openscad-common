#!/usr/bin/python3

import sys
import re
import tempfile
import argparse
from pathlib import Path
from decimal import Decimal as D, DecimalException

try:
    import pybgcode
except ImportError:
    pybgcode = None

def split_params(line):
    parts = line.partition(';')[0].split()
    rv = {}
    for p in parts:
        rv[p[0]] = p[1:]
    return rv

def find_current_params(lines, idx, limit=500):
    need = {'X', 'Y', 'Z', 'F'}
    rparams = {}
    begin = max(0, idx - limit)

    while idx >= begin and len(rparams) < 4:
        line = lines[idx]
        if re.match(r'^G[0-3] ', line):
            v = split_params(line[3:])
            for key in need:
                if key in v:
                    rparams.setdefault(key, v[key])
        idx -= 1

    return D(rparams.get('X')), D(rparams.get('Y')), D(rparams.get('Z')), D(rparams.get('F'))

def search_forward(lines, idx, func, limit=500):
    end = min(idx + limit, len(lines))
    while idx < end:
        line = lines[idx]
        res = func(idx, line)
        if res is not None:
            return idx, res
    return len(lines), None

def search_backward(lines, idx, func, limit=500):
    begin = max(0, idx - limit)
    while idx >= begin and len(rparams) < 4:
        line = lines[idx]
        res = func(idx, line)
        if res is not None:
            return idx, res
    return 0, None

def is_bgcode(path:Path):
    with path.open('rb') as fp:
        magic = fp.read(4)
        return magic == b'GCDE'

def read_text_lines(path:Path):
    rv = []
    with path.open(encoding='utf8') as fp:
        for line in fp:
            rv.append(line.rstrip())
    return rv

def read_gcode(path:Path):
    isbin = is_bgcode(path)
    if isbin:
        fpi = None
        fpo = None
        tempf = None
        try:
            tempf = tempfile.NamedTemporaryFile(suffix='.gcode', delete=False)
            tempf.close()
            fpi = pybgcode.open(str(path), "rb")
            fpo = pybgcode.open(tempf.name, "w")
            pybgcode.from_binary_to_ascii(fpi, fpo, False)
            pybgcode.close(fpo)
            fpo = None
            return True, read_text_lines(Path(tempf.name))
        finally:
            if tempf is not None:
                #print(f'input tempf = {tempf.name}')
                Path(tempf.name).unlink(missing_ok=True)
            if fpi is not None:
                pybgcode.close(fpi)
            if fpo is not None:
                pybgcode.close(fpo)
    else:
        return False, read_text_lines(path)

def write_gcode(path:Path, lines:list, binary=False):
    if binary:
        fpi = None
        fpo = None
        tempf = None
        try:
            tempf = tempfile.NamedTemporaryFile(suffix='.gcode', delete=False, mode='w', encoding='utf8')
            for line in lines:
                tempf.write(line + '\n')
            tempf.flush()
            tempf.close()

            fpi = pybgcode.open(tempf.name, "r")
            fpo = pybgcode.open(str(path), "wb")
            return pybgcode.from_ascii_to_binary(fpi, fpo, pybgcode.get_config())
        finally:
            if tempf is not None:
                #print(f'output tempf = {tempf.name}')
                Path(tempf.name).unlink(missing_ok=True)
            if fpi is not None:
                pybgcode.close(fpi)
            if fpo is not None:
                pybgcode.close(fpo)
    else:
        with path.open('w') as fp:
            for line in lines:
                fp.write(line + '\n')

def init_arguments(description=''):
    p = argparse.ArgumentParser(description=description)
    p.add_argument(
        '-g', '--gcode', type=Path,
        help='GCODE file to modify')

    p.add_argument(
        '-o', '--output', type=Path,
        help='Write to `output` instead of overwriting input')

    return p

def run(args, filterfunc):
    if args.gcode:
        isbin, lines = read_gcode(args.gcode)

        lines = filterfunc(args, lines)

        if args.output:
            rv = write_gcode(args.output, lines, args.output.suffix == '.bgcode')
        else:
            rv = write_gcode(args.gcode, lines, isbin)


        if not (rv is None or (pybgcode and rv == pybgcode.EResult.Success)):
            print(f'Error: {rv}', file=sys.stderr)
    else:
        lines = []
        for line in sys.stdin:
            lines.append(line.rstrip())

        lines = filterfunc(args, lines)

        for line in lines:
            print(line)

if __name__ == '__main__':
    p = init_arguments('Dummy filter')
    run(p.parse_args(), lambda args, lines: lines)
