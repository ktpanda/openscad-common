// Definitions for screws
// [pitch, major diameter, minor diameter]
// Data from http://www.apollointernational.in/iso-metric-thread-chart.php

screw_m2_ext_min = [0.4000, 1.8860, 1.4080];
screw_m2_ext_mid = [0.4000, 1.9335, 1.4780];
screw_m2_ext_max = [0.4000, 1.9810, 1.5480];
screw_m2_int_min = [0.4000, 2.0000, 1.5670];
screw_m2_int_mid = [0.4000, 2.0740, 1.6230];
screw_m2_int_max = [0.4000, 2.1480, 1.6790];

screw_m2_2_ext_min = [0.4500, 2.0800, 1.5400];
screw_m2_2_ext_mid = [0.4500, 2.1300, 1.6165];
screw_m2_2_ext_max = [0.4500, 2.1800, 1.6930];
screw_m2_2_int_min = [0.4500, 2.2000, 1.7130];
screw_m2_2_int_mid = [0.4500, 2.2800, 1.7755];
screw_m2_2_int_max = [0.4500, 2.3600, 1.8380];

screw_m2_3_ext_min = [0.4500, 2.1800, 1.6400];
screw_m2_3_ext_mid = [0.4500, 2.2300, 1.7165];
screw_m2_3_ext_max = [0.4500, 2.2800, 1.7930];
screw_m2_3_int_min = [0.4500, 2.3000, 1.8130];
screw_m2_3_int_mid = [0.4500, 2.3800, 1.8755];
screw_m2_3_int_max = [0.4500, 2.4600, 1.9380];

screw_m2_5_ext_min = [0.4500, 2.3800, 1.8400];
screw_m2_5_ext_mid = [0.4500, 2.4300, 1.9165];
screw_m2_5_ext_max = [0.4500, 2.4800, 1.9930];
screw_m2_5_int_min = [0.4500, 2.5000, 2.0130];
screw_m2_5_int_mid = [0.4500, 2.5800, 2.0755];
screw_m2_5_int_max = [0.4500, 2.6600, 2.1380];

screw_m2_6_ext_min = [0.4500, 2.4800, 1.9400];
screw_m2_6_ext_mid = [0.4500, 2.5300, 2.0165];
screw_m2_6_ext_max = [0.4500, 2.5800, 2.0930];
screw_m2_6_int_min = [0.4500, 2.6000, 2.1130];
screw_m2_6_int_mid = [0.4500, 2.6750, 2.1755];
screw_m2_6_int_max = [0.4500, 2.7500, 2.2380];

screw_m3_ext_min = [0.5000, 2.8740, 2.2720];
screw_m3_ext_mid = [0.5000, 2.9270, 2.3555];
screw_m3_ext_max = [0.5000, 2.9800, 2.4390];
screw_m3_int_min = [0.5000, 3.0000, 2.4590];
screw_m3_int_mid = [0.5000, 3.0860, 2.5290];
screw_m3_int_max = [0.5000, 3.1720, 2.5990];

screw_m3_5_ext_min = [0.6000, 3.3540, 2.6350];
screw_m3_5_ext_mid = [0.6000, 3.4165, 2.7320];
screw_m3_5_ext_max = [0.6000, 3.4790, 2.8290];
screw_m3_5_int_min = [0.6000, 3.5000, 2.8500];
screw_m3_5_int_mid = [0.6000, 3.5995, 2.9300];
screw_m3_5_int_max = [0.6000, 3.6990, 3.0100];

screw_m4_ext_min = [0.7000, 3.8380, 3.0020];
screw_m4_ext_mid = [0.7000, 3.9080, 3.1110];
screw_m4_ext_max = [0.7000, 3.9780, 3.2200];
screw_m4_int_min = [0.7000, 4.0000, 3.2420];
screw_m4_int_mid = [0.7000, 4.1095, 3.3320];
screw_m4_int_max = [0.7000, 4.2190, 3.4220];

screw_m4_5_ext_min = [0.7500, 4.3380, 3.4390];
screw_m4_5_ext_mid = [0.7500, 4.4080, 3.5525];
screw_m4_5_ext_max = [0.7500, 4.4780, 3.6660];
screw_m4_5_int_min = [0.7500, 4.5000, 3.6880];
screw_m4_5_int_mid = [0.7500, 4.6130, 3.7830];
screw_m4_5_int_max = [0.7500, 4.7260, 3.8780];

screw_m5_ext_min = [0.8000, 4.8260, 3.8690];
screw_m5_ext_mid = [0.8000, 4.9010, 3.9895];
screw_m5_ext_max = [0.8000, 4.9760, 4.1100];
screw_m5_int_min = [0.8000, 5.0000, 4.1340];
screw_m5_int_mid = [0.8000, 5.1200, 4.2340];
screw_m5_int_max = [0.8000, 5.2400, 4.3340];

screw_m5_5_ext_min = [0.5000, 5.3740, 4.7570];
screw_m5_5_ext_mid = [0.5000, 5.4270, 4.8480];
screw_m5_5_ext_max = [0.5000, 5.4800, 4.9390];
screw_m5_5_int_min = [0.5000, 5.5000, 4.9590];
screw_m5_5_int_mid = [0.5000, 5.5960, 5.0290];
screw_m5_5_int_max = [0.5000, 5.6920, 5.0990];

screw_m6_ext_min = [1.0000, 5.7940, 4.5960];
screw_m6_ext_mid = [1.0000, 5.8840, 4.7435];
screw_m6_ext_max = [1.0000, 5.9740, 4.8910];
screw_m6_int_min = [1.0000, 6.0000, 4.9170];
screw_m6_int_mid = [1.0000, 6.1470, 5.0350];
screw_m6_int_max = [1.0000, 6.2940, 5.1530];

screw_m7_ext_min = [1.0000, 6.7940, 5.5960];
screw_m7_ext_mid = [1.0000, 6.8840, 5.7435];
screw_m7_ext_max = [1.0000, 6.9740, 5.8910];
screw_m7_int_min = [1.0000, 7.0000, 5.9170];
screw_m7_int_mid = [1.0000, 7.1470, 6.0350];
screw_m7_int_max = [1.0000, 7.2940, 6.1530];

screw_m8_ext_min = [1.2500, 7.7600, 6.2720];
screw_m8_ext_mid = [1.2500, 7.8660, 6.4455];
screw_m8_ext_max = [1.2500, 7.9720, 6.6190];
screw_m8_int_min = [1.2500, 8.0000, 6.6470];
screw_m8_int_mid = [1.2500, 8.1700, 6.7795];
screw_m8_int_max = [1.2500, 8.3400, 6.9120];

screw_m9_ext_min = [1.2500, 8.7600, 7.2720];
screw_m9_ext_mid = [1.2500, 8.8660, 7.4455];
screw_m9_ext_max = [1.2500, 8.9720, 7.6190];
screw_m9_int_min = [1.2500, 9.0000, 7.6470];
screw_m9_int_mid = [1.2500, 9.1700, 7.7795];
screw_m9_int_max = [1.2500, 9.3400, 7.9120];

screw_m10_ext_min = [1.5000, 9.7320, 7.9380];
screw_m10_ext_mid = [1.5000, 9.8500, 8.1410];
screw_m10_ext_max = [1.5000, 9.9680, 8.3440];
screw_m10_int_min = [1.5000, 10.0000, 8.3760];
screw_m10_int_mid = [1.5000, 10.1980, 8.5260];
screw_m10_int_max = [1.5000, 10.3960, 8.6760];

screw_m11_ext_min = [1.5000, 10.7300, 8.9380];
screw_m11_ext_mid = [1.5000, 10.8500, 9.1410];
screw_m11_ext_max = [1.5000, 10.9700, 9.3440];
screw_m11_int_min = [1.5000, 11.0000, 9.3760];
screw_m11_int_mid = [1.5000, 11.1935, 9.5260];
screw_m11_int_max = [1.5000, 11.3870, 9.6760];

screw_m12_ext_min = [1.7500, 11.7000, 9.6010];
screw_m12_ext_mid = [1.7500, 11.8350, 9.8365];
screw_m12_ext_max = [1.7500, 11.9700, 10.0720];
screw_m12_int_min = [1.7500, 12.0000, 10.1060];
screw_m12_int_mid = [1.7500, 12.2265, 10.2735];
screw_m12_int_max = [1.7500, 12.4530, 10.4410];

screw_m14_ext_min = [2.0000, 13.6800, 11.2710];
screw_m14_ext_mid = [2.0000, 13.8200, 11.5340];
screw_m14_ext_max = [2.0000, 13.9600, 11.7970];
screw_m14_int_min = [2.0000, 14.0000, 11.8350];
screw_m14_int_mid = [2.0000, 14.2505, 12.0225];
screw_m14_int_max = [2.0000, 14.5010, 12.2100];

screw_m15_ext_min = [1.5000, 14.7300, 12.9300];
screw_m15_ext_mid = [1.5000, 14.8500, 13.1370];
screw_m15_ext_max = [1.5000, 14.9700, 13.3440];
screw_m15_int_min = [1.5000, 15.0000, 13.3760];
screw_m15_int_mid = [1.5000, 15.2035, 13.5260];
screw_m15_int_max = [1.5000, 15.4070, 13.6760];

screw_m16_ext_min = [2.0000, 15.6800, 13.2710];
screw_m16_ext_mid = [2.0000, 15.8200, 13.5340];
screw_m16_ext_max = [2.0000, 15.9600, 13.7970];
screw_m16_int_min = [2.0000, 16.0000, 13.8350];
screw_m16_int_mid = [2.0000, 16.2505, 14.0225];
screw_m16_int_max = [2.0000, 16.5010, 14.2100];

screw_m17_ext_min = [1.5000, 16.7300, 14.9300];
screw_m17_ext_mid = [1.5000, 16.8500, 15.1370];
screw_m17_ext_max = [1.5000, 16.9700, 15.3440];
screw_m17_int_min = [1.5000, 17.0000, 15.3760];
screw_m17_int_mid = [1.5000, 17.2035, 15.5260];
screw_m17_int_max = [1.5000, 17.4070, 15.6760];

screw_m18_ext_min = [2.5000, 17.6200, 14.6240];
screw_m18_ext_mid = [2.5000, 17.7900, 14.9380];
screw_m18_ext_max = [2.5000, 17.9600, 15.2520];
screw_m18_int_min = [2.5000, 18.0000, 15.2940];
screw_m18_int_mid = [2.5000, 18.2925, 15.5190];
screw_m18_int_max = [2.5000, 18.5850, 15.7440];

screw_m20_ext_min = [2.5000, 19.6200, 16.6240];
screw_m20_ext_mid = [2.5000, 19.7900, 16.9380];
screw_m20_ext_max = [2.5000, 19.9600, 17.2520];
screw_m20_int_min = [2.5000, 20.0000, 17.2940];
screw_m20_int_mid = [2.5000, 20.2925, 17.5190];
screw_m20_int_max = [2.5000, 20.5850, 17.7440];

screw_m22_ext_min = [3.0000, 21.5800, 17.9700];
screw_m22_ext_mid = [3.0000, 21.7650, 18.3370];
screw_m22_ext_max = [3.0000, 21.9500, 18.7040];
screw_m22_int_min = [3.0000, 22.0000, 18.7520];
screw_m22_int_mid = [3.0000, 22.3385, 19.0020];
screw_m22_int_max = [3.0000, 22.6770, 19.2520];

screw_m24_ext_min = [3.0000, 23.5800, 19.9550];
screw_m24_ext_mid = [3.0000, 23.7650, 20.3295];
screw_m24_ext_max = [3.0000, 23.9500, 20.7040];
screw_m24_int_min = [3.0000, 24.0000, 20.7520];
screw_m24_int_mid = [3.0000, 24.3490, 21.0020];
screw_m24_int_max = [3.0000, 24.6980, 21.2520];

screw_m25_ext_min = [2.0000, 24.6800, 22.2610];
screw_m25_ext_mid = [2.0000, 24.8200, 22.5290];
screw_m25_ext_max = [2.0000, 24.9600, 22.7970];
screw_m25_int_min = [2.0000, 25.0000, 22.8350];
screw_m25_int_mid = [2.0000, 25.2565, 23.0225];
screw_m25_int_max = [2.0000, 25.5130, 23.2100];

screw_m26_ext_min = [1.5000, 25.7300, 23.9200];
screw_m26_ext_mid = [1.5000, 25.8500, 24.1320];
screw_m26_ext_max = [1.5000, 25.9700, 24.3440];
screw_m26_int_min = [1.5000, 26.0000, 24.3760];
screw_m26_int_mid = [1.5000, 26.2085, 24.5260];
screw_m26_int_max = [1.5000, 26.4170, 24.6760];

screw_m27_ext_min = [3.0000, 26.5800, 22.9550];
screw_m27_ext_mid = [3.0000, 26.7650, 23.3295];
screw_m27_ext_max = [3.0000, 26.9500, 23.7040];
screw_m27_int_min = [3.0000, 27.0000, 23.7520];
screw_m27_int_mid = [3.0000, 27.3490, 24.0020];
screw_m27_int_max = [3.0000, 27.6980, 24.2520];

screw_m28_ext_min = [2.0000, 27.6800, 25.2610];
screw_m28_ext_mid = [2.0000, 27.8200, 25.5290];
screw_m28_ext_max = [2.0000, 27.9600, 25.7970];
screw_m28_int_min = [2.0000, 28.0000, 25.8350];
screw_m28_int_mid = [2.0000, 28.2565, 26.0225];
screw_m28_int_max = [2.0000, 28.5130, 26.2100];

screw_m30_ext_min = [3.5000, 29.5200, 25.3060];
screw_m30_ext_mid = [3.5000, 29.7350, 25.7320];
screw_m30_ext_max = [3.5000, 29.9500, 26.1580];
screw_m30_int_min = [3.5000, 30.0000, 26.2110];
screw_m30_int_mid = [3.5000, 30.3925, 26.4910];
screw_m30_int_max = [3.5000, 30.7850, 26.7710];

screw_m32_ext_min = [2.0000, 31.6800, 29.2610];
screw_m32_ext_mid = [2.0000, 31.8200, 29.5290];
screw_m32_ext_max = [2.0000, 31.9600, 29.7970];
screw_m32_int_min = [2.0000, 32.0000, 29.8350];
screw_m32_int_mid = [2.0000, 32.2565, 30.0225];
screw_m32_int_max = [2.0000, 32.5130, 30.2100];

screw_m33_ext_min = [3.5000, 32.5400, 28.3270];
screw_m33_ext_mid = [3.5000, 32.7550, 28.7530];
screw_m33_ext_max = [3.5000, 32.9700, 29.1790];
screw_m33_int_min = [3.5000, 33.0000, 29.2110];
screw_m33_int_mid = [3.5000, 33.3925, 29.4910];
screw_m33_int_max = [3.5000, 33.7850, 29.7710];

screw_m35_ext_min = [1.5000, 34.7300, 32.9200];
screw_m35_ext_mid = [1.5000, 34.8500, 33.1320];
screw_m35_ext_max = [1.5000, 34.9700, 33.3440];
screw_m35_int_min = [1.5000, 35.0000, 33.3760];
screw_m35_int_mid = [1.5000, 35.2080, 33.5260];
screw_m35_int_max = [1.5000, 35.4160, 33.6760];

screw_m36_ext_min = [4.0000, 35.4700, 30.6540];
screw_m36_ext_mid = [4.0000, 35.7050, 31.1320];
screw_m36_ext_max = [4.0000, 35.9400, 31.6100];
screw_m36_int_min = [4.0000, 36.0000, 31.6700];
screw_m36_int_mid = [4.0000, 36.4385, 31.9700];
screw_m36_int_max = [4.0000, 36.8770, 32.2700];

screw_m38_ext_min = [1.5000, 37.7300, 35.9200];
screw_m38_ext_mid = [1.5000, 37.8500, 36.1320];
screw_m38_ext_max = [1.5000, 37.9700, 36.3440];
screw_m38_int_min = [1.5000, 38.0000, 36.3760];
screw_m38_int_mid = [1.5000, 38.2085, 36.5260];
screw_m38_int_max = [1.5000, 38.4170, 36.6760];

screw_m39_ext_min = [4.0000, 38.4700, 33.6540];
screw_m39_ext_mid = [4.0000, 38.7050, 34.1320];
screw_m39_ext_max = [4.0000, 38.9400, 34.6100];
screw_m39_int_min = [4.0000, 39.0000, 34.6700];
screw_m39_int_mid = [4.0000, 39.4385, 34.9700];
screw_m39_int_max = [4.0000, 39.8770, 35.2700];

screw_m40_ext_min = [3.0000, 39.5800, 35.9550];
screw_m40_ext_mid = [3.0000, 39.7650, 36.3295];
screw_m40_ext_max = [3.0000, 39.9500, 36.7040];
screw_m40_int_min = [3.0000, 40.0000, 36.7520];
screw_m40_int_mid = [3.0000, 40.3490, 37.0020];
screw_m40_int_max = [3.0000, 40.6980, 37.2520];

screw_m42_ext_min = [4.5000, 41.4400, 36.0060];
screw_m42_ext_mid = [4.5000, 41.6900, 36.5360];
screw_m42_ext_max = [4.5000, 41.9400, 37.0660];
screw_m42_int_min = [4.5000, 42.0000, 37.1290];
screw_m42_int_mid = [4.5000, 42.4825, 37.4640];
screw_m42_int_max = [4.5000, 42.9650, 37.7990];

screw_m45_ext_min = [4.5000, 44.4400, 39.0060];
screw_m45_ext_mid = [4.5000, 44.6900, 39.5360];
screw_m45_ext_max = [4.5000, 44.9400, 40.0660];
screw_m45_int_min = [4.5000, 45.0000, 40.1290];
screw_m45_int_mid = [4.5000, 45.4825, 40.4640];
screw_m45_int_max = [4.5000, 45.9650, 40.7990];

screw_m48_ext_min = [5.0000, 47.4000, 41.3510];
screw_m48_ext_mid = [5.0000, 47.6650, 41.9335];
screw_m48_ext_max = [5.0000, 47.9300, 42.5160];
screw_m48_int_min = [5.0000, 48.0000, 42.5870];
screw_m48_int_mid = [5.0000, 48.5285, 42.9420];
screw_m48_int_max = [5.0000, 49.0570, 43.2970];

screw_m50_ext_min = [4.0000, 49.4700, 44.6420];
screw_m50_ext_mid = [4.0000, 49.7050, 45.1260];
screw_m50_ext_max = [4.0000, 49.9400, 45.6100];
screw_m50_int_min = [4.0000, 50.0000, 45.6700];
screw_m50_int_mid = [4.0000, 50.4460, 45.9700];
screw_m50_int_max = [4.0000, 50.8920, 46.2700];

screw_m52_ext_min = [5.0000, 51.4000, 45.3650];
screw_m52_ext_mid = [5.0000, 51.6650, 45.9405];
screw_m52_ext_max = [5.0000, 51.9300, 46.5160];
screw_m52_int_min = [5.0000, 52.0000, 46.5870];
screw_m52_int_mid = [5.0000, 52.5185, 46.9420];
screw_m52_int_max = [5.0000, 53.0370, 47.2970];

screw_m55_ext_min = [4.0000, 54.4700, 49.6420];
screw_m55_ext_mid = [4.0000, 54.7050, 50.1260];
screw_m55_ext_max = [4.0000, 54.9400, 50.6100];
screw_m55_int_min = [4.0000, 55.0000, 50.6700];
screw_m55_int_mid = [4.0000, 55.4460, 50.9700];
screw_m55_int_max = [4.0000, 55.8920, 51.2700];

screw_m56_ext_min = [5.5000, 55.3700, 48.7000];
screw_m56_ext_mid = [5.5000, 55.6500, 49.3355];
screw_m56_ext_max = [5.5000, 55.9300, 49.9710];
screw_m56_int_min = [5.5000, 56.0000, 50.0460];
screw_m56_int_mid = [5.5000, 56.5745, 50.4210];
screw_m56_int_max = [5.5000, 57.1490, 50.7960];

screw_m58_ext_min = [4.0000, 57.4700, 52.6420];
screw_m58_ext_mid = [4.0000, 57.7050, 53.1260];
screw_m58_ext_max = [4.0000, 57.9400, 53.6100];
screw_m58_int_min = [4.0000, 58.0000, 53.6700];
screw_m58_int_mid = [4.0000, 58.4460, 53.9700];
screw_m58_int_max = [4.0000, 58.8920, 54.2700];

screw_m60_ext_min = [5.5000, 59.3700, 52.7000];
screw_m60_ext_mid = [5.5000, 59.6500, 53.3355];
screw_m60_ext_max = [5.5000, 59.9300, 53.9710];
screw_m60_int_min = [5.5000, 60.0000, 54.0460];
screw_m60_int_mid = [5.5000, 60.5745, 54.4210];
screw_m60_int_max = [5.5000, 61.1490, 54.7960];

screw_m62_ext_min = [4.0000, 61.4700, 56.6420];
screw_m62_ext_mid = [4.0000, 61.7050, 57.1260];
screw_m62_ext_max = [4.0000, 61.9400, 57.6100];
screw_m62_int_min = [4.0000, 62.0000, 57.6700];
screw_m62_int_mid = [4.0000, 62.4460, 57.9700];
screw_m62_int_max = [4.0000, 62.8920, 58.2700];

screw_m63_ext_min = [1.5000, 62.7300, 60.9100];
screw_m63_ext_mid = [1.5000, 62.8500, 61.1270];
screw_m63_ext_max = [1.5000, 62.9700, 61.3440];
screw_m63_int_min = [1.5000, 63.0000, 61.3760];
screw_m63_int_mid = [1.5000, 63.2145, 61.5260];
screw_m63_int_max = [1.5000, 63.4290, 61.6760];

screw_m64_ext_min = [6.0000, 63.3200, 56.0470];
screw_m64_ext_mid = [6.0000, 63.6200, 56.7360];
screw_m64_ext_max = [6.0000, 63.9200, 57.4250];
screw_m64_int_min = [6.0000, 64.0000, 57.5050];
screw_m64_int_mid = [6.0000, 64.6205, 57.9050];
screw_m64_int_max = [6.0000, 65.2410, 58.3050];

screw_m65_ext_min = [4.0000, 64.4700, 59.6420];
screw_m65_ext_mid = [4.0000, 64.7050, 60.1260];
screw_m65_ext_max = [4.0000, 64.9400, 60.6100];
screw_m65_int_min = [4.0000, 65.0000, 60.6700];
screw_m65_int_mid = [4.0000, 65.4460, 60.9700];
screw_m65_int_max = [4.0000, 65.8920, 61.2700];

screw_m68_ext_min = [6.0000, 67.3200, 60.0470];
screw_m68_ext_mid = [6.0000, 67.6200, 60.7360];
screw_m68_ext_max = [6.0000, 67.9200, 61.4250];
screw_m68_int_min = [6.0000, 68.0000, 61.5050];
screw_m68_int_mid = [6.0000, 68.6205, 61.9050];
screw_m68_int_max = [6.0000, 69.2410, 62.3050];

screw_m70_ext_min = [6.0000, 69.3200, 62.0470];
screw_m70_ext_mid = [6.0000, 69.6200, 62.7360];
screw_m70_ext_max = [6.0000, 69.9200, 63.4250];
screw_m70_int_min = [6.0000, 70.0000, 63.5050];
screw_m70_int_mid = [6.0000, 70.6205, 63.9050];
screw_m70_int_max = [6.0000, 71.2410, 64.3050];

screw_m72_ext_min = [6.0000, 71.3200, 64.0470];
screw_m72_ext_mid = [6.0000, 71.6200, 64.7360];
screw_m72_ext_max = [6.0000, 71.9200, 65.4250];
screw_m72_int_min = [6.0000, 72.0000, 65.5050];
screw_m72_int_mid = [6.0000, 72.6205, 65.9050];
screw_m72_int_max = [6.0000, 73.2410, 66.3050];

screw_m75_ext_min = [6.0000, 74.3200, 67.0470];
screw_m75_ext_mid = [6.0000, 74.6200, 67.7360];
screw_m75_ext_max = [6.0000, 74.9200, 68.4250];
screw_m75_int_min = [6.0000, 75.0000, 68.5050];
screw_m75_int_mid = [6.0000, 75.6205, 68.9050];
screw_m75_int_max = [6.0000, 76.2410, 69.3050];

screw_m76_ext_min = [6.0000, 75.3200, 68.0470];
screw_m76_ext_mid = [6.0000, 75.6200, 68.7360];
screw_m76_ext_max = [6.0000, 75.9200, 69.4250];
screw_m76_int_min = [6.0000, 76.0000, 69.5050];
screw_m76_int_mid = [6.0000, 76.6205, 69.9050];
screw_m76_int_max = [6.0000, 77.2410, 70.3050];

screw_m78_ext_min = [2.0000, 77.6800, 75.2510];
screw_m78_ext_mid = [2.0000, 77.8200, 75.5240];
screw_m78_ext_max = [2.0000, 77.9600, 75.7970];
screw_m78_int_min = [2.0000, 78.0000, 75.8350];
screw_m78_int_mid = [2.0000, 78.2625, 76.0225];
screw_m78_int_max = [2.0000, 78.5250, 76.2100];

screw_m80_ext_min = [6.0000, 79.3200, 72.0470];
screw_m80_ext_mid = [6.0000, 79.6200, 72.7360];
screw_m80_ext_max = [6.0000, 79.9200, 73.4250];
screw_m80_int_min = [6.0000, 80.0000, 73.5050];
screw_m80_int_mid = [6.0000, 80.6205, 73.9050];
screw_m80_int_max = [6.0000, 81.2410, 74.3050];

screw_m82_ext_min = [2.0000, 81.6800, 79.2510];
screw_m82_ext_mid = [2.0000, 81.8200, 79.5240];
screw_m82_ext_max = [2.0000, 81.9600, 79.7970];
screw_m82_int_min = [2.0000, 82.0000, 79.8350];
screw_m82_int_mid = [2.0000, 82.2625, 80.0225];
screw_m82_int_max = [2.0000, 82.5250, 80.2100];

screw_m85_ext_min = [6.0000, 84.3200, 77.0470];
screw_m85_ext_mid = [6.0000, 84.6200, 77.7360];
screw_m85_ext_max = [6.0000, 84.9200, 78.4250];
screw_m85_int_min = [6.0000, 85.0000, 78.5050];
screw_m85_int_mid = [6.0000, 85.6205, 78.9050];
screw_m85_int_max = [6.0000, 86.2410, 79.3050];

screw_m90_ext_min = [6.0000, 89.3200, 82.0470];
screw_m90_ext_mid = [6.0000, 89.6200, 82.7360];
screw_m90_ext_max = [6.0000, 89.9200, 83.4250];
screw_m90_int_min = [6.0000, 90.0000, 83.5050];
screw_m90_int_mid = [6.0000, 90.6205, 83.9050];
screw_m90_int_max = [6.0000, 91.2410, 84.3050];

screw_m95_ext_min = [6.0000, 94.3200, 87.0270];
screw_m95_ext_mid = [6.0000, 94.6200, 87.7260];
screw_m95_ext_max = [6.0000, 94.9200, 88.4250];
screw_m95_int_min = [6.0000, 95.0000, 88.5050];
screw_m95_int_mid = [6.0000, 95.6330, 88.9050];
screw_m95_int_max = [6.0000, 96.2660, 89.3050];

screw_m100_ext_min = [6.0000, 99.3200, 92.0270];
screw_m100_ext_mid = [6.0000, 99.6200, 92.7260];
screw_m100_ext_max = [6.0000, 99.9200, 93.4250];
screw_m100_int_min = [6.0000, 100.0000, 93.5050];
screw_m100_int_mid = [6.0000, 100.6350, 93.9050];
screw_m100_int_max = [6.0000, 101.2700, 94.3050];

all_screws = [
    ["m2", [screw_m2_ext_min, screw_m2_ext_mid, screw_m2_ext_max, screw_m2_int_min, screw_m2_int_mid, screw_m2_int_max]],
    ["m2.2", [screw_m2_2_ext_min, screw_m2_2_ext_mid, screw_m2_2_ext_max, screw_m2_2_int_min, screw_m2_2_int_mid, screw_m2_2_int_max]],
    ["m2.3", [screw_m2_3_ext_min, screw_m2_3_ext_mid, screw_m2_3_ext_max, screw_m2_3_int_min, screw_m2_3_int_mid, screw_m2_3_int_max]],
    ["m2.5", [screw_m2_5_ext_min, screw_m2_5_ext_mid, screw_m2_5_ext_max, screw_m2_5_int_min, screw_m2_5_int_mid, screw_m2_5_int_max]],
    ["m2.6", [screw_m2_6_ext_min, screw_m2_6_ext_mid, screw_m2_6_ext_max, screw_m2_6_int_min, screw_m2_6_int_mid, screw_m2_6_int_max]],
    ["m3", [screw_m3_ext_min, screw_m3_ext_mid, screw_m3_ext_max, screw_m3_int_min, screw_m3_int_mid, screw_m3_int_max]],
    ["m3.5", [screw_m3_5_ext_min, screw_m3_5_ext_mid, screw_m3_5_ext_max, screw_m3_5_int_min, screw_m3_5_int_mid, screw_m3_5_int_max]],
    ["m4", [screw_m4_ext_min, screw_m4_ext_mid, screw_m4_ext_max, screw_m4_int_min, screw_m4_int_mid, screw_m4_int_max]],
    ["m4.5", [screw_m4_5_ext_min, screw_m4_5_ext_mid, screw_m4_5_ext_max, screw_m4_5_int_min, screw_m4_5_int_mid, screw_m4_5_int_max]],
    ["m5", [screw_m5_ext_min, screw_m5_ext_mid, screw_m5_ext_max, screw_m5_int_min, screw_m5_int_mid, screw_m5_int_max]],
    ["m5.5", [screw_m5_5_ext_min, screw_m5_5_ext_mid, screw_m5_5_ext_max, screw_m5_5_int_min, screw_m5_5_int_mid, screw_m5_5_int_max]],
    ["m6", [screw_m6_ext_min, screw_m6_ext_mid, screw_m6_ext_max, screw_m6_int_min, screw_m6_int_mid, screw_m6_int_max]],
    ["m7", [screw_m7_ext_min, screw_m7_ext_mid, screw_m7_ext_max, screw_m7_int_min, screw_m7_int_mid, screw_m7_int_max]],
    ["m8", [screw_m8_ext_min, screw_m8_ext_mid, screw_m8_ext_max, screw_m8_int_min, screw_m8_int_mid, screw_m8_int_max]],
    ["m9", [screw_m9_ext_min, screw_m9_ext_mid, screw_m9_ext_max, screw_m9_int_min, screw_m9_int_mid, screw_m9_int_max]],
    ["m10", [screw_m10_ext_min, screw_m10_ext_mid, screw_m10_ext_max, screw_m10_int_min, screw_m10_int_mid, screw_m10_int_max]],
    ["m11", [screw_m11_ext_min, screw_m11_ext_mid, screw_m11_ext_max, screw_m11_int_min, screw_m11_int_mid, screw_m11_int_max]],
    ["m12", [screw_m12_ext_min, screw_m12_ext_mid, screw_m12_ext_max, screw_m12_int_min, screw_m12_int_mid, screw_m12_int_max]],
    ["m14", [screw_m14_ext_min, screw_m14_ext_mid, screw_m14_ext_max, screw_m14_int_min, screw_m14_int_mid, screw_m14_int_max]],
    ["m15", [screw_m15_ext_min, screw_m15_ext_mid, screw_m15_ext_max, screw_m15_int_min, screw_m15_int_mid, screw_m15_int_max]],
    ["m16", [screw_m16_ext_min, screw_m16_ext_mid, screw_m16_ext_max, screw_m16_int_min, screw_m16_int_mid, screw_m16_int_max]],
    ["m17", [screw_m17_ext_min, screw_m17_ext_mid, screw_m17_ext_max, screw_m17_int_min, screw_m17_int_mid, screw_m17_int_max]],
    ["m18", [screw_m18_ext_min, screw_m18_ext_mid, screw_m18_ext_max, screw_m18_int_min, screw_m18_int_mid, screw_m18_int_max]],
    ["m20", [screw_m20_ext_min, screw_m20_ext_mid, screw_m20_ext_max, screw_m20_int_min, screw_m20_int_mid, screw_m20_int_max]],
    ["m22", [screw_m22_ext_min, screw_m22_ext_mid, screw_m22_ext_max, screw_m22_int_min, screw_m22_int_mid, screw_m22_int_max]],
    ["m24", [screw_m24_ext_min, screw_m24_ext_mid, screw_m24_ext_max, screw_m24_int_min, screw_m24_int_mid, screw_m24_int_max]],
    ["m25", [screw_m25_ext_min, screw_m25_ext_mid, screw_m25_ext_max, screw_m25_int_min, screw_m25_int_mid, screw_m25_int_max]],
    ["m26", [screw_m26_ext_min, screw_m26_ext_mid, screw_m26_ext_max, screw_m26_int_min, screw_m26_int_mid, screw_m26_int_max]],
    ["m27", [screw_m27_ext_min, screw_m27_ext_mid, screw_m27_ext_max, screw_m27_int_min, screw_m27_int_mid, screw_m27_int_max]],
    ["m28", [screw_m28_ext_min, screw_m28_ext_mid, screw_m28_ext_max, screw_m28_int_min, screw_m28_int_mid, screw_m28_int_max]],
    ["m30", [screw_m30_ext_min, screw_m30_ext_mid, screw_m30_ext_max, screw_m30_int_min, screw_m30_int_mid, screw_m30_int_max]],
    ["m32", [screw_m32_ext_min, screw_m32_ext_mid, screw_m32_ext_max, screw_m32_int_min, screw_m32_int_mid, screw_m32_int_max]],
    ["m33", [screw_m33_ext_min, screw_m33_ext_mid, screw_m33_ext_max, screw_m33_int_min, screw_m33_int_mid, screw_m33_int_max]],
    ["m35", [screw_m35_ext_min, screw_m35_ext_mid, screw_m35_ext_max, screw_m35_int_min, screw_m35_int_mid, screw_m35_int_max]],
    ["m36", [screw_m36_ext_min, screw_m36_ext_mid, screw_m36_ext_max, screw_m36_int_min, screw_m36_int_mid, screw_m36_int_max]],
    ["m38", [screw_m38_ext_min, screw_m38_ext_mid, screw_m38_ext_max, screw_m38_int_min, screw_m38_int_mid, screw_m38_int_max]],
    ["m39", [screw_m39_ext_min, screw_m39_ext_mid, screw_m39_ext_max, screw_m39_int_min, screw_m39_int_mid, screw_m39_int_max]],
    ["m40", [screw_m40_ext_min, screw_m40_ext_mid, screw_m40_ext_max, screw_m40_int_min, screw_m40_int_mid, screw_m40_int_max]],
    ["m42", [screw_m42_ext_min, screw_m42_ext_mid, screw_m42_ext_max, screw_m42_int_min, screw_m42_int_mid, screw_m42_int_max]],
    ["m45", [screw_m45_ext_min, screw_m45_ext_mid, screw_m45_ext_max, screw_m45_int_min, screw_m45_int_mid, screw_m45_int_max]],
    ["m48", [screw_m48_ext_min, screw_m48_ext_mid, screw_m48_ext_max, screw_m48_int_min, screw_m48_int_mid, screw_m48_int_max]],
    ["m50", [screw_m50_ext_min, screw_m50_ext_mid, screw_m50_ext_max, screw_m50_int_min, screw_m50_int_mid, screw_m50_int_max]],
    ["m52", [screw_m52_ext_min, screw_m52_ext_mid, screw_m52_ext_max, screw_m52_int_min, screw_m52_int_mid, screw_m52_int_max]],
    ["m55", [screw_m55_ext_min, screw_m55_ext_mid, screw_m55_ext_max, screw_m55_int_min, screw_m55_int_mid, screw_m55_int_max]],
    ["m56", [screw_m56_ext_min, screw_m56_ext_mid, screw_m56_ext_max, screw_m56_int_min, screw_m56_int_mid, screw_m56_int_max]],
    ["m58", [screw_m58_ext_min, screw_m58_ext_mid, screw_m58_ext_max, screw_m58_int_min, screw_m58_int_mid, screw_m58_int_max]],
    ["m60", [screw_m60_ext_min, screw_m60_ext_mid, screw_m60_ext_max, screw_m60_int_min, screw_m60_int_mid, screw_m60_int_max]],
    ["m62", [screw_m62_ext_min, screw_m62_ext_mid, screw_m62_ext_max, screw_m62_int_min, screw_m62_int_mid, screw_m62_int_max]],
    ["m63", [screw_m63_ext_min, screw_m63_ext_mid, screw_m63_ext_max, screw_m63_int_min, screw_m63_int_mid, screw_m63_int_max]],
    ["m64", [screw_m64_ext_min, screw_m64_ext_mid, screw_m64_ext_max, screw_m64_int_min, screw_m64_int_mid, screw_m64_int_max]],
    ["m65", [screw_m65_ext_min, screw_m65_ext_mid, screw_m65_ext_max, screw_m65_int_min, screw_m65_int_mid, screw_m65_int_max]],
    ["m68", [screw_m68_ext_min, screw_m68_ext_mid, screw_m68_ext_max, screw_m68_int_min, screw_m68_int_mid, screw_m68_int_max]],
    ["m70", [screw_m70_ext_min, screw_m70_ext_mid, screw_m70_ext_max, screw_m70_int_min, screw_m70_int_mid, screw_m70_int_max]],
    ["m72", [screw_m72_ext_min, screw_m72_ext_mid, screw_m72_ext_max, screw_m72_int_min, screw_m72_int_mid, screw_m72_int_max]],
    ["m75", [screw_m75_ext_min, screw_m75_ext_mid, screw_m75_ext_max, screw_m75_int_min, screw_m75_int_mid, screw_m75_int_max]],
    ["m76", [screw_m76_ext_min, screw_m76_ext_mid, screw_m76_ext_max, screw_m76_int_min, screw_m76_int_mid, screw_m76_int_max]],
    ["m78", [screw_m78_ext_min, screw_m78_ext_mid, screw_m78_ext_max, screw_m78_int_min, screw_m78_int_mid, screw_m78_int_max]],
    ["m80", [screw_m80_ext_min, screw_m80_ext_mid, screw_m80_ext_max, screw_m80_int_min, screw_m80_int_mid, screw_m80_int_max]],
    ["m82", [screw_m82_ext_min, screw_m82_ext_mid, screw_m82_ext_max, screw_m82_int_min, screw_m82_int_mid, screw_m82_int_max]],
    ["m85", [screw_m85_ext_min, screw_m85_ext_mid, screw_m85_ext_max, screw_m85_int_min, screw_m85_int_mid, screw_m85_int_max]],
    ["m90", [screw_m90_ext_min, screw_m90_ext_mid, screw_m90_ext_max, screw_m90_int_min, screw_m90_int_mid, screw_m90_int_max]],
    ["m95", [screw_m95_ext_min, screw_m95_ext_mid, screw_m95_ext_max, screw_m95_int_min, screw_m95_int_mid, screw_m95_int_max]],
    ["m100", [screw_m100_ext_min, screw_m100_ext_mid, screw_m100_ext_max, screw_m100_int_min, screw_m100_int_mid, screw_m100_int_max]],
];

//_screw_index = [for (i = [0 : len(all_screws)]) [all_screws[i][0], i]];
function get_screw(size) = all_screws[search([size], all_screws)[0]][1];

function screw_inner(size) = get_screw(size)[5];
function screw_outer(size) = get_screw(size)[0];
